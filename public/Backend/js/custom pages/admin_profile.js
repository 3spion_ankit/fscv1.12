$(document).ready(function () {
    /*
    all javascript logic of admin profile page.
    first describe logic parts and another part for validation and ajax calling.
     */


    /* Custome Function */


    const libs = {
        // realtime Typing function pass value (object,value)

        realTimeTyping: function (input, output) {
            const isSelected = $('#checkmeout0').is(':checked');
            if (isSelected) {
                output.val(input.val());
            }
        },
        setJurisdication: function (boolval) {
            if (boolval == "City") {
                $('.show_license_city').removeClass('d-none');
                $('#license_county_div').addClass('d-none');
            } else if (boolval == "County") {
                $('.show_license_city').addClass('d-none');
                $('#license_county_div').removeClass('d-none');
            }
        },
        isSamePhysicalAddress: function (boolval) {
            const isSelected = $(boolval).is(':checked');
            const reg_address1 = $('#reg_address_1').val();
            const reg_address2 = $('#reg_address_2').val();
            const reg_city = $('#reg_city').val();
            const reg_state = $('#comp_state').find(':selected').val();
            const reg_zipcode = $('#reg_zipcode').val();
            if (isSelected) {
                $('.hasCompanyReadonly').attr('readonly', 'readonly');
                $('#pa_address_1').val(reg_address1);
                $('#pa_address_2').val(reg_address2);
                $('#pa_city').val(reg_city);
                $('#pa_state').val(reg_state);
                $('#pa_state_input').val(reg_state);
                $('#pa_zip').val(reg_zipcode);
                $('#inputTag').removeClass('d-none');
                $('#pa_state_input').attr('name', 'pa_state');
                $('#pa_state').attr('name', 'pa_state__');
                $('#selectTag').addClass('d-none');
            } else {
                $('.hasCompanyReadonly').removeAttr('readonly');
                $('#pa_address_1').val('');
                $('#pa_address_2').val('');
                $('#pa_city').val('');
                $('#pa_state').val('');
                $('#pa_state_code').val('');
                $('#pa_zip').val('');
                $('#inputTag').addClass('d-none');
                $('#selectTag').removeClass('d-none');
                $('#pa_state').attr('name', 'pa_state');
                $('#pa_state_input').attr('name', 'pa_state__');
            }
        },
        isSameAsFax: function (boolval) {
            const isSelected = $(boolval).is(':checked');
            const fax_no = $('#fax_no').val();
            if (isSelected) {
                $('.hasReadonly').attr('readonly', 'readonly');
                $('#contact_fax_no').val(fax_no);
            } else {
                $('.hasReadonly').removeAttr('readonly');
                $('#contact_fax_no').val('');
            }
        },
        setQuestionValue: function (input, output) {
            const selectedVal = $(input).find(':selected').text();
            $(output).val(selectedVal);
        },
        responsess: function (result) {
            console.log(result);
        },
        ajaxCalling: function (url, data, method, callback) {
            $.ajax({
                url: url,
                method: method,
                data: data,
                success: callback,
                error: function (error) {
                    //returnval = error;
                }
            });
        },
        setBankName: function (val, component) {
            $(component).val(val);
        },
        telephoneType: function (input, output) {
            const val = $(input).find(':selected').val();
            if (val == "Office") {
                $(output).removeAttr('readonly');
            } else if (val == "Mobile") {
                $(output).val('');
                $(output).attr('readonly', 'readonly');
            }
        },
        dateConvertion: function (userDate) {
            var date = new Date(userDate),
                m = date.getMonth() + 1,
                yr = date.getFullYear(),
                month = m < 10 ? '0' + m : m,
                day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            return month + '/' + day + '/' + yr;
        }

    }

    $('#comp_state').change(function(){
        const isSelected = $('#checkmeout0').is(':checked');
        if(isSelected){
            const reg_state = $('#comp_state').find(':selected').val();
            $('#pa_state_input').val(reg_state);
        }
    });

    const jurisdiction_value = $('#jurisdiction').find(':selected').val();
    libs.setJurisdication(jurisdiction_value);
    $('#jurisdiction').change(function () {
        libs.setJurisdication($(this).val());
    });

    /* All Fields Masking */
    $('.js-select-2').select2();

    // $("#ext1").inputFilter(function(value) {
    //     return /^\d*$/.test(value);    // Allow digits only, using a RegExp
    // });

    /* In Company Info Profile */
    libs.isSamePhysicalAddress($('#checkmeout0'));
    $('#checkmeout0').click(function () {
        libs.isSamePhysicalAddress(this);
    });

    $('#reg_address_1').keyup(function () {
        var output = $('#pa_address_1');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_address_2').keyup(function () {
        var output = $('#pa_address_2');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_city').keyup(function () {
        var output = $('#pa_city');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_zipcode').keyup(function () {
        var output = $('#pa_zip');
        libs.realTimeTyping($(this), output);
    });
    $('#reg_state').keyup(function () {
        var output = $('#pa_state');
        libs.realTimeTyping($(this), output);
    });

    libs.telephoneType($('#contact_no_type'), $('#ext1'));
    $('#contact_no_type').change(function () {
        libs.telephoneType($('#contact_no_type'), $('#ext1'));
    });


    /* In Contact Info Profile */
    libs.isSameAsFax($('#checkmeout1'));
    $('#checkmeout1').click(function () {
        libs.isSameAsFax(this);
    });

    libs.telephoneType($('#contact_no_type_1'), $('#contact_no_extension_1'));
    libs.telephoneType($('#contact_no_type_2'), $('#contact_no_extension_2'));

    $('#contact_no_type_1').change(function () {
        libs.telephoneType($('#contact_no_type_1'), $('#contact_no_extension_1'));
    });
    $('#contact_no_type_2').change(function () {
        libs.telephoneType($('#contact_no_type_2'), $('#contact_no_extension_2'));
    });


    /* In Security Info Profile */
    $('#que_1').change(function () {
        libs.setQuestionValue($(this), $('#question1'));
    });
    $('#que_2').change(function () {
        libs.setQuestionValue($(this), $('#question2'));
    });
    $('#que_3').change(function () {
        libs.setQuestionValue($(this), $('#question3'));
    });


    /* In Profile Formation Tab */
    var getSelectedOfCorporation = $('#coporation').is(':checked');
    var getSelectedOfLLC = $('#llc').is(':checked');
    if (getSelectedOfCorporation) {
        $('#certificateText').text('Certification Of Corporation :');
        $('#certificateLabel').text('Certification Of Corporation');
        $('#articleText').text('Article Of Incorporation :');
        $('#articlelabel').text('Article Of Incorporation');
    }
    if (getSelectedOfLLC) {
        $('#certificateText').text('Certification Of LLC :');
        $('#certificateLabel').text('Certification Of LLC');
        $('#articleText').text('Article Of LLC :');
        $('#articlelabel').text('Article Of LLC');
    }
    $('#coporation').click(function () {
        $('#certificateText').text('Certification Of Corporation');
        $('#certificateLabel').text('Certification Of Corporation');
        $('#articleText').text('Article Of Incorporation');
        $('#articlelabel').text('Article Of Incorporation');
    });
    $('#llc').click(function () {
        $('#certificateText').text('Certification Of LLC');
        $('#certificateLabel').text('Certification Of LLC');
        $('#articleText').text('Article Of LLC');
        $('#articlelabel').text('Article Of LLC');
    });

    $('.certificateViewer').click(function () {
        const val = $(this).attr('data-src');
        const vs = $('input[name=certificate_type]:checked').val();
        console.log(vs);
        var title = "";
        if(vs == 1){
            title = "Certificate Of Corporation";
        }else if(vs == 2){
            title = "Certificate Of LLC";
        }
        $('.modal-body-documents').load(val,function(){

            $('.modal-fsc-title').html("Financial Service Center Inc.");
            $('.modal-sec-title').html("View "+title);
            $('#documentLoaders').modal({show:true});
        });
    });
    $('.articlesViewer').click(function () {
        const val = $(this).attr('data-src');
        const vs = $('input[name=certificate_type]:checked').val();
        console.log(vs);
        var title = "";
        if(vs == 1){
            title = "Article Of Incorporation";
        }else if(vs == 2){
            title = "Article Of LLC";
        }
        $('.modal-body-documents').load(val,function(){
            const vs = $('#coporation').find(':checked').val();
            console.log(vs);
            $('.modal-fsc-title').html("Financial Service Center Inc.");
            $('.modal-sec-title').html("View "+title);
            $('#documentLoaders').modal({show:true});
        });
    });

    $('#viewBusinessLicense').click(function () {
        const val = $(this).attr('data-src');
        $('.modal-body-documents').load(val,function(){
            $('.modal-fsc-title').html("Financial Service Center Inc.");
            $('.modal-sec-title').html("View License");
            $('#documentLoaders').modal({show:true});
        });
    });

    /*
    Banking Tab
     */

    libs.setBankName($('.bank_id').find(':selected').text());
    $('.bank_id').on('change', function () {
        const getComponentId = $(this).attr('id');
        var divString = getComponentId.split('_');
        let component = $('#banks_name');
        if (divString.length > 2) {
            const id = divString[2];
            component = $('#bank_name_' + id);
        }
        const val = $('#' + getComponentId + ' option:selected').text();
        console.log("selected val: " + getComponentId);
        libs.setBankName(val, component);
    });
    /*
        Form Validation Area
     */

    $("#storeProfile").validate({
        rules: {
            company_name: {required: true},
            business_name: {required: true},
            address_1: {required: true},
            city: {required: true},
            comp_state: {required: true},
            zip: {required: true},
            pa_city: {required: true},
            pa_state: {required: true},
            pa_zip: {required: true},
            pa_address_1: {requird: true},
            contact_no: {required: true},
            email:{email:true}
        },
        messages: {
            company_name: {required: "Company name must required !"},
            business_name: {required: "Business name must required !"},
            address_1: {required: "Address must required !"},
            city: {required: "City must required !"},
            comp_state: {required: "state must required !"},
            zip: {required: "Zip must required !"},
            pa_city: {required: "Physical City must required !"},
            pa_state: {required: "Physical State must required !"},
            pa_zip: {required: "Physical Zip must required !"},
            pa_address_1: {required: "Physical Address must required !"},
            contact_no: {required: "Contact must required !"},
            email:{email:"Email format is not correct"}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#storeContact").validate({
        rules: {
            fname: {required: true},
            lname: {required: true},
            city: {required: true},
            state: {required: true},
            zip: {required: true},
            contact_no_1: {required: true},
            email:{email:true}
        },
        messages: {
            fname: {required: "First name must be required !"},
            lname: {required: "Last Name must be required !"},
            city: {required: "City must required !"},
            state: {required: "state must required !"},
            zip: {required: "Zip must required !"},
            contact_no_1: {required: "Contact must required !"},
            email:{email:"Email format is not correct"}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();

        }
    });


    /*
    Ajax Calling Function For Formation
     */
    $('#certificateDelete').click(function () {
        const url = $(this).attr('data-src');
        const token = $(this).attr('data-token');
        const method = "POST";
        const data = {
            '_token': token,
            'certificate': 'certificate',
            'id': 1
        };
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
            location.reload();
        });

    });

    $('#addNewProfession').click(function () {
        const formsData = $('#addProfessionForm').serialize();
        const url = $(this).attr('data-src');
        const method = "POST";
        const data = formsData;
        libs.ajaxCalling(url, data, method, function (response) {
            //console.log(response);
            var jsonData = JSON.parse(response);
            $('#profession_name_table tbody').empty();
            $("#profession").empty();
            $("#profession").select2();
            var professionOption = "<option value=''>Select Profession</option>";
            for (var i = 0; i < jsonData.length; i++) {
                var no = i + 1;
                let professionData = "<tr>";
                professionData += "<td>" + no + "</td>";
                professionData += "<td>" + jsonData[i]['profession_name'] + "</td>";
                professionData += '<td>' +
                    '<a href="javascript:void(0);" id="'+jsonData[i]['id']+'" onclick="delProfession('+jsonData[i]['id']+');" class="delProfession"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                    '</td>';
                professionOption += "<option value='" + jsonData[i]['profession_name'] + "'>" + jsonData[i]['profession_name'] + " </option>";

                $('#profession_name_table tbody').append(professionData);
            }
            $("#profession").append(professionOption);
            $('#modal_Profession').modal('hide');
        });
    });


    $('.deleteProfession').click(function () {
        const id = $(this).attr('id');
        const url = '/admin/deleteProfession';
        const method = "GET";
        const data = {
            'id': id
        };
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
            var jsonData = JSON.parse(response);
            $('#profession_name_table tbody').empty();
            $("#profession").empty();
            $("#profession").select2();
            var professionOption = "<option value=''>Select Profession</option>";
            for (var i = 0; i < jsonData.length; i++) {
                var no = i + 1;
                let professionData = "<tr>";
                professionData += "<td>" + no + "</td>";
                professionData += "<td>" + jsonData[i]['profession_name'] + "</td>";
                professionData += '<td>' +
                    '<a id="'+jsonData[i]['id']+'" onclick="delProfession();" ><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                    '</td>';
                professionOption += "<option value='" + jsonData[i]['profession_name'] + "'>" + jsonData[i]['profession_name'] + " </option>";

                $('#profession_name_table tbody').append(professionData);
            }
            $("#profession").append(professionOption);

            $('#modal_Profession').modal('hide');
        });

    });


    $('#articlesDelete').click(function () {
        const url = $(this).attr('data-src');
        const token = $(this).attr('data-token');
        const method = "POST";
        const data = {
            '_token': token,
            'certificate': 'article',
            'id': 1
        };
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
            location.reload();
        });

    });

    $('.editSharHolder').click(function () {

        //const id = $(this).attr('data-id');
        const url = $(this).attr('data-src');
        const method = "GET";
        const data = {};
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
            var jsonData = JSON.parse(response);
            $('#shareholder_id').val(jsonData['id']);
            $('#shareHolder_fname').val(jsonData['fname']);
            $('#shareHolder_mname').val(jsonData['mname']);
            $('#shareHolder_lname').val(jsonData['lname']);
            $('#shareHolder_position').val(jsonData['position']);
            $('#shareHolder_percentage').val(jsonData['percentage']);
            $('#shareHolder_effective_date').val(libs.dateConvertion(jsonData['effective_date']));
            $('#shareHolder_SH_status').val(jsonData['status']);
            $('#modal-Share-holder-records').modal('show');
        })
    });

    $('.deleteShareholder').click(function () {
        Swal.fire({
            title: 'Are you sure?',
            text: "To remove From Share Holder",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const url = $(this).attr('data-src');
                const method = "GET";
                const data = {};
                libs.ajaxCalling(url, data, method, function (response) {
                    console.log(response);
                    window.location.reload();
                });
            }
        })
    });

    /*
        Dynamic Models Formation
     */
    $('.dynamic-models').click(function () {
        const dataURL = $(this).attr('data-href');
        $('.modal-body').load(dataURL, function () {
            $('#modal-Share-holder-records').modal({show: true});
        });
    });
    const selectedTypeOfLicense = $('#type_of_license').find(':selected').val();
    if(selectedTypeOfLicense == "Profession License"){
        $('#ifProfession').removeClass('d-none');
    }else if(selectedTypeOfLicense == "Firm License"){
        $('#ifProfession').addClass('d-none');
    }
    $('#type_of_license').change(function (){
        if($(this).val() == "Profession License"){
            $('#ifProfession').removeClass('d-none');
        }else if($(this).val() == "Firm License"){
            $('#ifProfession').addClass('d-none');
        }
    });

});

function responseofAjax(response) {
    var val = response;
    return val;
}

function delProfession(id) {
    const url = '/admin/deleteProfession';
    const method = "GET";
    const data = {
        'id': id
    };

    $.ajax({
        url: url,
        method: method,
        data: data,
        success: function(response){
            console.log(response);
            var jsonData = JSON.parse(response);
            $('#profession_name_table tbody').empty();
            $("#profession").empty();
            $("#profession").select2();
            var professionOption = "<option value=''>Select Profession</option>";
            for (var i = 0; i < jsonData.length; i++) {
                var no = i + 1;
                let professionData = "<tr>";
                professionData += "<td>" + no + "</td>";
                professionData += "<td>" + jsonData[i]['profession_name'] + "</td>";
                professionData += '<td>' +
                    '<a id="'+jsonData[i]['id']+'" class="deleteProfession"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                    '</td>';
                professionOption += "<option value='" + jsonData[i]['profession_name'] + "'>" + jsonData[i]['profession_name'] + " </option>";

                $('#profession_name_table tbody').append(professionData);
            }
            $("#profession").append(professionOption);

            $('#modal_Profession').modal('hide');
        },
        error: function (error) {

        }
    });
}

function dateConversation(selectedDate)
{
    var date = new Date(selectedDate),
        m = date.getMonth() + 1,
        yr = date.getFullYear(),
        month = m < 10 ? '0' + m : m,
        day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    return month + '/' + day + '/' + yr;
}

function editProfessionalLicense(id) {
    const url = '/admin/getProfessionalLicense';
    const method = "GET";
    const data = {
        'id': id
    };
    $.ajax({
        url: url,
        method: method,
        data: data,
        success: function(response){
            console.log($('#professional_license_state').select2('data'));
            var jsonData = JSON.parse(response);
            $('#profession').val(jsonData['profession']);
            $('#professional_id').val(jsonData['id']);
            $('#professional_license_state').select2('destroy');
            $('#professional_license_state').val(jsonData['professional_license_state']);
            $('#professional_license_state').select2();
            $('#type_of_license').val(jsonData['type_of_license']);
            $('#professional_license_period').val(jsonData['professional_license_period']);
            $('#professional_effective_date').val(dateConversation(jsonData['professional_effective_date']));
            $('#professional_license_no').val(jsonData['professional_license_no']);
            $('#professional_expire_date').val(dateConversation(jsonData['professional_expire_date']));
            $('#professional_license_note').val(jsonData['professional_license_note']);
            $('#is_ce_status').val();
        },
        error: function (error) {

        }
    });
}
