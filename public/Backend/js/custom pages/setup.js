$(document).ready(function () {

    //Common and custom function

    const libs = {
        readURL: function (input, imgControlName) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(imgControlName).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    };

    /*
    Business image preview before uploading.
     */
    $('#business_img').on('change', function () {
        console.log(this.file);
        libs.readURL($(this),$('#previewImag'));
    });
});
