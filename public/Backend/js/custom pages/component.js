$(document).ready(function(){

    /*
    Common Function
     */
    const libs = {
        componentReadonly:function(input,action){
            if(action=="show"){
                $(input).removeAttr('readonly');
            }else if(action=="hide"){
                $(input).attr('readonly','readonly');
            }
        },
        telephone:function(val){
            if(val=='Office'){
                this.componentReadonly($('.hasReadonly'),"show");
            }else if(val=='Mobile'){
                this.componentReadonly($('.hasReadonly'),"hide");
            }
        }
    }

    /*
   telephone Componet
    */
    libs.telephone($('.telephone_type').find(':selected').val());
    $('.telephone_type').change(function(e){
        const val = $(this).val();
        libs.telephone(val);
    });

});
