$(document).ready(function () {
    const libs={
        ajaxCalling: function (url, data, method, callback) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                method: method,
                data: data,
                success: callback,
                error: function (error) {
                    //returnval = error;
                }
            });
        },
        nextDate:function(days){
            const today = new Date();

            const priorDate = new Date().setDate(today.getDate() + days);
            const day = new Date(priorDate).getDate() <= 9 ? "0" + new Date(priorDate).getDate() : new Date(priorDate).getDate();
            const month = (new Date(priorDate).getMonth() + 1) <= 9 ? "0" + (new Date(priorDate).getMonth() + 1) : new Date(priorDate).getMonth()+1;
            const year = new Date(priorDate).getFullYear();
            return day + "/" + month + "/" + year;
        }
    };



    $('#reset_days').change(function () {
        const selectedDays = parseInt($(this).val());
        const nextDays = selectedDays > 0 ? libs.nextDate(selectedDays) :'';
        $('#reset_date').val(nextDays);
    });

    $('#next_review').on('blur',function(){
        const days = parseInt($(this).val());
        const nextDays = days > 0 ? libs.nextDate(days) : '';
        $('#next_review_date').val(nextDays);
    });

    $("#pay_form_submit").on('click',function(){
        var formdata = $('#pay_form').serialize();
        console.log(formdata);
        const url = $(this).attr('data-src');
        const method = "POST";
        const data = formdata;
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
        });
    });
    $("#review_submit").on('click',function(){
        var formdata = $('#review_form').serialize();
        console.log(formdata);
        const url = $(this).attr('data-src');
        const method = "POST";
        const data = formdata;
        libs.ajaxCalling(url, data, method, function (response) {
            console.log(response);
        });
    });
});
