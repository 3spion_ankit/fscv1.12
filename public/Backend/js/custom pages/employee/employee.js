$(document).ready(function () {

    const libs = {

        ajaxCalling: function (url, data, method, callback) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                method: method,
                data: data,
                success: callback,
                error: function (error) {
                    //returnval = error;
                }
            });
        },
        telephoneMasking: function (val, component) {
            const componetVal = $(component).val();
            if (val == "IND") {
                $(component).mask('99999 99999');
                $(component).val(componetVal);
            } else if (val == "USA") {
                $(component).mask('(999) 999-9999');
                $(component).val("9999999999");
            }
        },
        getId: function (val) {
            const divideString = val.split("_");
            const index = divideString.length - 1;
            return divideString[index];
        },
        setQuestionValue: function (input, output) {
            const selectedVal = $(input).find(':selected').text();
            $(output).val(selectedVal);
        },
        setStates: function (val, getId, url, state_val) {
            if (val != "") {
                const _token = $('meta[name="csrf-token"]').attr('content');
                const name = val;
                const method = "POST";
                const data = {
                    "_token": _token,
                    "contry_code": name,
                }
                libs.ajaxCalling(url, data, method, function (response) {
                    var jsonData = JSON.parse(response);
                    $('#states_' + getId).empty();
                    var output = "<option value=''> Select </option>";
                    for (var i = 0; i < jsonData.length; i++) {
                        if (jsonData[i]['code'] == state_val) {
                            output += "<option value='" + jsonData[i]['code'] + "' selected>" + jsonData[i]['code'] + " </option>";
                        } else {
                            output += "<option value='" + jsonData[i]['code'] + "'>" + jsonData[i]['code'] + " </option>";
                        }

                    }
                    $('#states_' + getId).append(output);
                });
                libs.telephoneMasking(name, $('#telephone_' + getId));
            }

        }
    }


    /* In Security Info Profile */
    $('#que_1').change(function () {
        libs.setQuestionValue($(this), $('#question1'));
    });
    $('#que_2').change(function () {
        libs.setQuestionValue($(this), $('#question2'));
    });
    $('#que_3').change(function () {
        libs.setQuestionValue($(this), $('#question3'));
    });

    $('#new-hiring-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
    $('#new-application-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
    $('#new-candidate-table').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}
        ]
    });
    // $('#represent_name').dataTable({
    //     "aoColumnDefs": [
    //         {"bSortable": false, "aTargets": [0, 1]}
    //     ]
    // });

    // Validation
    $("#create-employee").validate({
        rules: {
            employee_id: {required: true},
            user_id: {required: true},
            represent: {required: true},
            fname: {required: true},
            mname: {required: true},
            lname: {required: true},
            city: {required: true},
            state: {required: true},
            zip: {required: true},
            contact_no_1: {required: true}
        },
        messages: {
            employee_id: {required: "Please Enter Code"},
            user_id: {required: "Please Enter Code"},
            represent: {required: "Please Select Represent"},
            fname: {required: "First name must be required !"},
            mname: {required: "Middle name must be required !"},
            lname: {required: "Last Name must be required !"},
            city: {required: "City must required !"},
            state: {required: "state must required !"},
            zip: {required: "Zip must required !"},
            contact_no_1: {required: "Contact must required !"}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();

        }
    });

    /*
    Aajax calling
     */
    $('#create_represent_name').on('click', function () {
        const _token = $(this).attr('data-token');
        const name = $('#represent_name').val();
        const url = $(this).attr('data-src');
        const method = "POST";
        const data = {
            "_token": _token,
            "represent_name": name,
            "action": ""
        }
        libs.ajaxCalling(url, data, method, function (response) {
            var jsonData = JSON.parse(response);
            $('#represent_name_table tbody').empty();
            $("#represent").empty();
            var representOption = "<option value=''>Select Represent</option>";
            for (var i = 0; i < jsonData.length; i++) {
                var no = i + 1;
                let representData = "<tr>";
                representData += "<td>" + no + "</td>";
                representData += "<td>" + jsonData[i]['represent_name'] + "</td>";
                representData += '<td>' +
                    '<a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                    '</td>';
                var representOption = "<option value='" + jsonData[i]['id'] + "'>" + jsonData[i]['represent_name'] + " </option>";
                $("#represent").append(representOption);
                $('#represent_name_table tbody').append(representData);

            }
            $('#modal-represent-name').modal('hide');
        });

    });

    $('.edit_mode').on('show.bs.modal', function (event) {
        const id = libs.getId($(this).attr('id'));
        const url = $(this).attr('data-src');
        const val = $("#country_" + id).find(':selected').val();
        const state_val = $(this).attr('data-state');
        libs.setStates(val, id, url, state_val);
    });

    $('.country').on('change', function () {
        const id = libs.getId($(this).attr('id'));
        const url = $(this).attr('data-src');
        libs.setStates($(this).val(), id, url, "");
    });


});
