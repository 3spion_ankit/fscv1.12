$(document).ready(function () {

    /*
    Common function
     */

    const libs = {
        /*
        function return String of years base on user selection
         */
        getSelectedYearVal: function (year) {
            let years = "" ,cost = 0;
            let result = [];
            var thisYear = new Date().getFullYear();
            for (var i = 0; i < year; i++) {
                const yearCount = thisYear + i;
                cost = cost+50;
                if (i+1 == year) {
                    years += yearCount;
                } else {
                    years += yearCount + ",";
                }
            }
            result[0]=years;
            result[1]=cost;
            return result;
        },
        getRenewalYear: function (selectedval, output,output2) {
            const result = this.getSelectedYearVal(selectedval);
            $(output).val(result[0]);
            $(output2).val(result[1]);
        },
        ajaxCalling: function (url, data, method,callback) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                method: method,
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                success: callback,
                error: function (error) {
                    //returnval = error;
                }
            });
        }
    }

    /*
    work status Corporation License modal.
    Year
     */
    libs.getRenewalYear($('#renewal_for').find(':selected').val(),$('#renewal_year'),$('#renewal_amount'));
    $('#renewal_for').change(function () {
        const val = $(this).val();
        libs.getRenewalYear(val,$('#renewal_year'),$('#renewal_amount'));
    });

    /*
    Validation bY using validator js
     */

    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    });

    $('.corporationLicense').validate({
        ignore: [],
        rules:{
            paid_amount:{required:true},
            method_of_payment:{required:true},
            corporation_status:{ required: true},
            annually_receipt:{ required: true},
            officer:{require:true}
        },
        messages: {
            paid_amount:{ required: "First Name is required."},
            method_of_payment:{ required: "Last Name is required."},
            corporation_status:{ required: "Mobile no is required."},
            annually_receipt:{ required: "Email is required."},
            officer:{ required: "Select Specialization. It is required."}
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).closest('.form-tab').removeClass('has-error');
            $(e).remove();
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /*
    Ajax Calling OF Corporation License Renewal.
     */

    // $('body').on('click', 'edit_corporation', function (event) {
    //
    //     event.preventDefault();
    //     const id = $(this).attr('id');
    //     const url =$(this).attr('href');
    //     const method="POST";
    //     const data = {
    //         'corporation_id':id
    //     };
    //     libs.ajaxCalling(url,data,method,function(response){
    //         $('#modal-corporation-records').modal('show');
    //         var jsonData = JSON.parse(response);
    //         for(var i=0;i<jsonData.length;i++) {
    //             var no = i + 1;
    //             $('#renewal_for').val(jsonData[i]['renewal_for']);
    //             $('#renewal_year').val(jsonData[i]['renewal_year']);
    //             $('#renewal_amount').val(jsonData[i]['renewal_amount']);
    //             $('#paid_amount').val(jsonData[i]['paid_amount']);
    //             $('#method_of_payment').val(jsonData[i]['method_of_payment']);
    //             $('#corporation_status').val(jsonData[i]['corporation_status']);
    //             $('#annually_receipt').val(jsonData[i]['annually_receipt']);
    //             $('#officer').val(jsonData[i]['officer']);
    //             $('#website').val(jsonData[i]['website']);
    //         }
    //
    //     });
    // });

});
