<label class="col-md-3 col-form-label text-right" for="pa_city">City / State / Zip <span class="text-danger">*</span></label>
<div class="col-md-3">
    <input type="text" class="form-control" value="" id="{{$city_name}}" name="{{$city_name}}">
</div>
<div class="col-md-3" id="selectTag">
    <select name="{{$state_name}}" id="{{$state_name}}" class="form-control js-select-2">
        <option value="">Select</option>
        @foreach($getUSAStates as $key=>$state)
            <option value="{{$state->code}}" @if(isset($selectionData)) @if($selectionData == $state->code) selected @endif @endif >{{$state->code}}</option>
        @endforeach
    </select>
</div>
<div class="col-md-3">
    <input type="text" class="form-control input-usa-zip-mask" value="" id="{{$zip_name}}" name="{{$zip_name}}">
</div>
