<label class="col-form-label col-md-3 text-right">{{$label_name}} <span class="text-danger">*</span></label>
<div class="col-md-3">
    <input name="{{$contact_name}}" type="text" value="" id="{{$contact_name}}" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
</div>
<div class="col-md-3">
    <select name="{{$type_name}}" id="{{$type_name}}" class="form-control telephone_type">
        <option value="Office">Office</option>
        <option value="Mobile">Mobile</option>
    </select>
</div>
<div class="col-md-3">
    <input class="form-control input-extension-mask hasReadonly" id="{{$extnsion_name}}" name="{{$extnsion_name}}" value="" placeholder="Ext" type="text">
</div>
