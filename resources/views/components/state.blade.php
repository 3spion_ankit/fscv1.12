<select name="{{$ui_id ?? ''}}" id="{{$ui_id ?? ''}}" class="form-control js-select-2">
    <option value="">Select</option>
    @foreach($getUSAStates as $key=>$state)
        <option value="{{$state->code}}" @if(isset($selectionData)) @if($selectionData == $state->code) selected @endif @endif >{{$state->code}}</option>
    @endforeach
</select>
