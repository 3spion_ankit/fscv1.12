@if($status == "active" || $status == "Active" || $status==1)
    <span class="badge badge-status badge-outline-success badge-pill">Active</span>
@elseif ($status == "inactive" || $status == "Inactive")
    <span class="badge badge-status badge-outline-warning badge-pill">Inactive</span>
@elseif ($status=="hold" || $status=="Hold")
    <span class="badge badge-status badge-outline-danger badge-pill">Hold</span>
@elseif ($status=="wait" || $status=="Wait")
    <span class="badge badge-status badge-outline-primary badge-pill">Wait</span>
@elseif ($status=="close" || $status=="Close")
    <span class="badge badge-status badge-outline-danger badge-pill">Close</span>

@endif
