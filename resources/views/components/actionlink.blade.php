@if(isset($buttons))
        @if($buttons == 'edit' || $buttons == "Edit")
            <a href="{{$route}}" id="{{$id}}-model" data-toggle="modal" data-target="#{{$id}}" class="{{$class}}"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
            @endif
        @if($buttons == 'delete' || $buttons == "Delete")
            <a href="{{$route}}" id="{{$id}}-model" class="{{$class}}"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
        @endif
        @if($buttons == 'view' || $buttons == "View")
            <a href="{{$route}}" id="{{$id}}-model" date-toggle="modal" data-target="#{{$id}}"  class="{{$class}}"><i class="ri f-1-4x ri-eye-line text-primary"></i></a>
        @endif
        @if($buttons == 'action' || $buttons == "action")
        @endif
        @if($buttons == 'create' || $buttons == "Create")
            <a href="{{$route}}" id="{{$id}}-model" class="{{$class}}">{{$buttons}}</a>
        @endif
    @endif
