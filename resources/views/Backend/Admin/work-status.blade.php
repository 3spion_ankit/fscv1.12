@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">
    @php $dateUtility = new \App\Utility\DateUtility(); @endphp
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Administrator</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Work Status</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Admin Work Status {{session()->has('insideNo') ? session()->get('insideNo') : "" }}</h4>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link {{session()->has('worktabno') ? "" :" active "}} show mb-1" id="v-pills-corporation-tab" data-toggle="pill" href="#v-pills-corporation" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Corporation</a>
                                    <a class="nav-link {{session()->has('worktabno') ? "active" :" "}} mb-1" id="v-pills-license-tab" data-toggle="pill" href="#v-pills-license" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        License</a>
                                    <a class="nav-link mb-1" id="v-pills-taxation-tab" data-toggle="pill" href="#v-pills-taxation" role="tab" aria-controls="v-pills-messages"
                                       aria-selected="false">
                                        Taxation</a>
                                    <a class="nav-link mb-1" id="v-pills-banking-tab" data-toggle="pill" href="#v-pills-banking" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Banking</a>
                                    <a class="nav-link mb-1" id="v-pills-income-tab" data-toggle="pill" href="#v-pills-income" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Income</a>
                                    <a class="nav-link mb-1" id="v-pills-expense-tab" data-toggle="pill" href="#v-pills-expense" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        Expense</a>
                                    <a class="nav-link mb-1" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-other" role="tab" aria-controls="v-pills-settings"
                                       aria-selected="false">
                                        Others</a>
                                </div>
                            </div> <!-- end col-->


                            <div class="col-sm-10">
                                <div class="tab-content pt-0">
                                    <div class="tab-pane {{session()->has('worktabno') ? "" :" show active"}} " id="v-pills-corporation" role="tabpanel" aria-labelledby="v-pills-corporation-tab">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-2 col-form-label text-right" for="reg_address">State</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" value="GA" readonly>
                                                    </div>
                                                    <label class="col-md-3 col-form-label text-right" for="control">Control #</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="control" value="2227" redonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                            <li class="nav-item">
                                                                <a href="#corporation_renewal" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Corporation Renewal</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#share_ledger" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Share Ledger</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#amendment" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Amendment</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#minutes" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-cog"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Minutes</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#docs" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Docs</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active show" id="corporation_renewal">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-corporation-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Corporation Renewal Record
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-responsive table-bordered table-striped">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>Renewal Year</th>
                                                                                        <th>Renewal For</th>
                                                                                        <th>Paid Amount</th>
                                                                                        <th>Payment Method</th>
                                                                                        <th>Corporation Status</th>
                                                                                        <th>SOS Receipt</th>
                                                                                        <th>Annul Officer</th>
                                                                                        <th width="7%">Action</th>
                                                                                    </tr>
                                                                                    </thead>

                                                                                    <tbody>
                                                                                    @if(isset($corporationLicense))
                                                                                        @foreach($corporationLicense as $key=>$license)
                                                                                            <tr class="text-center">
                                                                                                <td>{{$license->renewal_year}}</td>
                                                                                                <td>{{$license->renewal_for}} Year</td>
                                                                                                <td>{{$license->paid_amount}}</td>
                                                                                                <td>{{$license->method_of_payment}}</td>
                                                                                                <td>{{$license->corporation_status}}</td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="" buttons="view"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="" buttons="view"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-actionlink
                                                                                                        id="modal-corporation-records-{{$license->id}}"
                                                                                                        class="waves-effect waves-light"
                                                                                                        buttons="edit"
                                                                                                        route="javascript:void(0);"
                                                                                                    />
                                                                                                    <x-actionlink route="" id="modal-corporation-records" class="waves-effect waves-light" buttons="delete"/>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <div class="modal fade" id="modal-corporation-records-{{$license->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                                <div class="modal-dialog modal-lg">
                                                                                                    <div class="modal-content">
                                                                                                        <div class="modal-header">
                                                                                                            <div class="text-center col-md-12">
                                                                                                                <h4 class="modal-title" id="myLargeModalLabel">Corporation Renew Record</h4>
                                                                                                            </div>
                                                                                                            {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="card">
                                                                                                                <div class="card-body">
                                                                                                                    <form class="control-form parsley-examples" action="{{route('admin.storeCorporationLicense')}}" method="post" enctype="multipart/form-data">
                                                                                                                        @csrf
                                                                                                                        <input type="hidden" name="admin_id" value="1"/>
                                                                                                                        <input type="hidden" name="corporation_id" id="{{$license->id}}"/>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="renewal_for"><span id="certificateText">Renew For </span></label>
                                                                                                                            <div class="col-md-3">
                                                                                                                                <select name="renewal_for" id="renewal_for" class="form-control">
                                                                                                                                    <option value="1" @if($license->renewal_for==1) selected @endif>1 Year</option>
                                                                                                                                    <option value="2" @if($license->renewal_for==2) selected @endif>2 Year</option>
                                                                                                                                    <option value="3" @if($license->renewal_for==3) selected @endif>3 Year</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="renewal_year"><span id="certificateText">Renewal Year</span></label>
                                                                                                                            <div class="col-md-3">
                                                                                                                                <input type="text" name="renewal_year" value="{{$license->renewal_year}}" class="form-control" id="renewal_year" readonly>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="renewal_amount"><span id="certificateText">Renew Amount</span></label>
                                                                                                                            <div class="col-md-3 input-group">
                                                                                                                                <div class="input-group-prepend">
                                                                                                                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                                                                                                                </div>
                                                                                                                                <input type="text" name="renewal_amount" class="form-control" value="{{$license->renewal_amount}}" id="renewal_amount" readonly>
                                                                                                                            </div>
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="paid_amount"><span id="certificateText">Paid Amount</span></label>
                                                                                                                            <div class="col-md-3 input-group">
                                                                                                                                <div class="input-group-prepend">
                                                                                                                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                                                                                                                </div>
                                                                                                                                <input type="text" name="paid_amount" value="{{$license->paid_amount}}" class="form-control" id="paid_amount" required>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="method_of_payment"><span id="certificateText">Method Of Payment</span></label>
                                                                                                                            <div class="col-md-9">
                                                                                                                                <input type="text" name="method_of_payment" value="{{$license->method_of_payment}}" class="form-control" id="method_of_payment">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="corporation_status"><span id="certificateText">Status</span></label>
                                                                                                                            <div class="col-md-9">
                                                                                                                                <select name="corporation_status" id="corporation_status" class="form-control" required>
                                                                                                                                    <option value="">Select</option>
                                                                                                                                    <option value="Active Owes Curr. Yr. AR" @if($license->corporation_status=="Active Owes Curr. Yr. AR") selected @endif>Active Owes Curr. Yr. AR</option>
                                                                                                                                    <option value="Active Compliance" @if($license->corporation_status=="Active Compliance") selected @endif>Active Compliance</option>
                                                                                                                                    <option value="Active Noncompliance" @if($license->corporation_status=="Active Noncompliance") selected @endif>Active Noncompliance</option>
                                                                                                                                    <option value="Admin. Dissolved" @if($license->corporation_status=="Admin. Dissolved") selected @endif>Admin. Dissolved</option>
                                                                                                                                    <option value="Dis-Cancel-Termin" @if($license->corporation_status=="Dis-Cancel-Termin") selected @endif>Dis-Cancel-Termin</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="annually_receipt"><span id="certificateText">Annually Receipt</span></label>
                                                                                                                            @if($license->annually_receipt)
                                                                                                                                <input type="hidden" name="annually_receipt" value="{{$license->annually_receipt}}"/>
                                                                                                                            @endif
                                                                                                                            <div class="col-md-9">
                                                                                                                                <input type="file" name="annually_receipt" id="annually_receipt" required>
                                                                                                                            </div>

                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="officer"><span id="certificateText">Officer</span></label>
                                                                                                                            @if($license->annually_receipt)
                                                                                                                                <input type="hidden" name="annually_receipt" value="{{$license->officer}}"/>
                                                                                                                            @endif
                                                                                                                            <div class="col-md-9">
                                                                                                                                <input type="file" name="officer"  id="officer" required>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-3 col-form-label text-right" for="website"><span id="certificateText">Website</span></label>
                                                                                                                            <div class="col-md-9">
                                                                                                                                <input type="text" name="website" class="form-control" value={{$license->website}} id="website">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3 text-center">
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="submit" value="submit" class="col-md-5 btn btn-primary text-center">
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </form>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><!-- /.modal-content -->
                                                                                                </div><!-- /.modal-dialog -->
                                                                                            </div><!-- /.modal -->
                                                                                        @endforeach
                                                                                    @endif
                                                                                    </tbody>
                                                                                </table>


                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane" id="share_ledger">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-share-Ledger-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Record
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-responsive table-bordered table-striped">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>ID</th>
                                                                                        <th>NAME OF CERTIFICATE HOLDER</th>
                                                                                        <th>CERTIFICATES ISSUED NO. SHARES*</th>
                                                                                        <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                                                                                        <th>AMOUNT PAID THEREON</th>
                                                                                        <th>DATE OF TRANSFER OF SHARES*</th>
                                                                                        <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                                                                                        <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                                                                                        <th width="7%">Action</th>
                                                                                    </tr>
                                                                                    </thead>

                                                                                    <tbody>

                                                                                    @if(isset($shareLedgerList))
                                                                                        @foreach($shareLedgerList as $key=>$shareLedger)
                                                                                            <tr class="text-center">
                                                                                                <td>{{$key+1}}</td>
                                                                                                <td>{{$shareLedger->holder_name}}</td>
                                                                                                <td>{{$shareLedger->issue_certificate}}</td>
                                                                                                <td>{{$shareLedger->origine_issue_form}}</td>
                                                                                                <td>{{$shareLedger->paid_amount}}</td>
                                                                                                <td>{{$dateUtility->retriveDate($shareLedger->transfer_date)}}</td>
                                                                                                <td>{{$shareLedger->surrender_certificate}}</td>
                                                                                                <td>{{$shareLedger->surrender_certificate_no}}</td>
                                                                                                <td>
                                                                                                    <x-actionlink
                                                                                                        id="modal-share-Ledger-records-{{$shareLedger->id}}"
                                                                                                        class="waves-effect waves-light"
                                                                                                        buttons="edit"
                                                                                                        route="javascript:void(0);"
                                                                                                    />
                                                                                                    <x-actionlink route="" id="" class="waves-effect waves-light" buttons="delete"/>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- Add Record of share Ledger -->
                                                                                            <!-- start -->
                                                                                            <div class="modal fade" id="modal-share-Ledger-records-{{$shareLedger->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                                <div class="modal-dialog modal-lg">
                                                                                                    <div class="modal-content">
                                                                                                        <div class="modal-header">
                                                                                                            <div class="text-center col-md-12">
                                                                                                                <h4 class="modal-title" id="myLargeModalLabel">Share Ledger Record</h4>
                                                                                                            </div>
                                                                                                            {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="card">
                                                                                                                <div class="card-body">
                                                                                                                    <form class="control-form" method="post" action="{{route('admin.storeShareLedger')}}">
                                                                                                                        @csrf
                                                                                                                        <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                                                                                                        <input type="hidden" name="shareledger_id" value="{{$shareLedger->id}}">
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="holder_name">
                                                                                                                                <span id="certificateText">Name of Certificate of Holder</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->holder_name}}" class="form-control" id="holder_name" name="holder_name" placeholder="Holder Name" required="">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="issue_certificate">
                                                                                                                                <span id="certificateText">Certificates issued No. Shares</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->issue_certificate}}" class="form-control" id="issue_certificate" name="issue_certificate" placeholder="Certificates Issue No of Shares" required="">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="origine_issue_from">
                                                                                                                                <span id="certificateText">From whom Transferred (If Original issue Enter as such)</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->origine_issue_form}}" class="form-control" id="origine_issue_form" name="origine_issue_form" placeholder="From Transfer">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="paid_amount">
                                                                                                                                <span id="certificateText">Amount paid thereon</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->paid_amount}}" class="form-control txtinput_1" id="paid_amount" name="paid_amount" placeholder="Paid Amount">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="transfer_date">
                                                                                                                                <span id="certificateText">Date of Transfer of Shares</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$dateUtility->retriveDateInVal($shareLedger->transfer_date)}}" class="form-control datepickers" id="transfer_date" name="transfer_date" placeholder="mm/dd/yyyy" readonly="" required="">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="surrender_certificate">
                                                                                                                                <span id="certificateText">Certificates Surrendered certif. Nos. </span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->surrender_certificate}}" class="form-control" id="surrender_certificate" name="surrender_certificate" placeholder="Surrender Certificate No">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="form-group row mb-3">
                                                                                                                            <label class="col-md-5 col-form-label text-right" for="surrender_certificate_no">
                                                                                                                                <span id="certificateText">Certificates Surrendered No. Shares</span>
                                                                                                                            </label>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <input type="text" value="{{$shareLedger->surrender_certificate_no}}" class="form-control" id="surrender_certificate_no" name="surrender_certificate_no" placeholder="Surrender Certificate No of Shares" required="">
                                                                                                                            </div>
                                                                                                                        </div>


                                                                                                                        <div class="form-group row mb-3 text-center">
                                                                                                                            <div class="col-md-6">
                                                                                                                                <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-6">
                                                                                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </form>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div><!-- /.modal-content -->
                                                                                                </div><!-- /.modal-dialog -->
                                                                                            </div><!-- /.modal -->
                                                                                            <!-- End -->
                                                                                        @endforeach
                                                                                    @endif
                                                                                    </tbody>
                                                                                </table>

                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="amendment">
                                                                <p>Amendment</p>
                                                            </div>
                                                            <div class="tab-pane" id="minutes">
                                                                <p>Minutes</p>
                                                            </div>
                                                            <div class="tab-pane" id="docs">
                                                                <form action="" class="control-form" method="post">
																	<div class="form-group row mb-3">
																		<div class="col-md-4">
																			<div class="form-group mb-3">
																				<label for="emailaddress">Document Name <span class="text-danger">*</span></label>
																				 <select name="work_status_docs[]" id="work_status_docs" class="form-control">
																					 <option value="Office">Office</option>
																					 <option value="Mobile">Mobile</option>
																				 </select>
																			</div>
																		</div>
																		<div class="col-md-1">
																			<br>
																			<div class="mt-1">
																				<a href="javascript:void(0);" class="" data-toggle="modal" data-target="#bs-example-modal-lg"><i class="ri ri-2x ri-add-box-line btn-warning"></i> </a>
																			</div>
																		</div>
																		<div class="col-md-4">
																			<div class="form-group mb-3">
																				<label for="emailaddress">Certificate of Corporation</label>
																				<input type="file" name="certificate" class="" id="certificate">
																			</div>
																		</div>
																		<div class="col-md-3">
																			<div class="form-group mb-3">
																				<br>
																				 <button type="submit" class="btn btn-primary mt-1	 text-center">Submit</button>
																			</div>
																		</div>

																	</div>	
																
                                                                </form>

                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <table id="corporation-renewal-table" class="table table-responsive table-bordered table-striped">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <th>Document Name</th>
                                                                                <th>view</th>
                                                                                <th width="7%">Action</th>
                                                                            </tr>
                                                                            </thead>

                                                                            <tbody>
                                                                            <tr>
                                                                                <td>Tiger Nixon</td>
                                                                                <td>System Architect</td>
                                                                                <td>Edinburgh</td>
                                                                                <td>61</td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div><!-- end col-->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end card -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade{{session()->has('worktabno') ? " active show" :" "}}  fade" id="v-pills-license" role="tabpanel" aria-labelledby="v-pills-license-tab">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                            <li class="nav-item">
                                                                <a href="#business_license" data-toggle="tab" aria-expanded="false" class="nav-link  {{session()->has('insideNo') ? session()->get('insideNo') == 1 ? "active" : "" :"active"}} ">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Business License</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#professional_license" data-toggle="tab" aria-expanded="true" class="nav-link {{session()->has('insideNo') ? session()->get('insideNo') == 2 ? "active" : "" :""}} ">
                                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                    <span class="d-none d-sm-inline-block">Professional License</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane  {{session()->has('insideNo') ? session()->get('insideNo') == 1 ? "active show" : "" :"active show"}}" id="business_license">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-Business-License-records">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Business License
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <table id="corporation-renewal-table" class="table table-responsive table-bordered table-hover table-centered">
                                                                                    <thead>
                                                                                    <tr class="text-center">
                                                                                        <th>Year</th>
                                                                                        <th>License Gross</th>
                                                                                        <th>License Fee</th>
                                                                                        <th>License #</th>
                                                                                        <th>License Issue Date</th>
                                                                                        <th>License Copy</th>
                                                                                        <th>Status</th>
                                                                                        <th width="7%">Action</th>
                                                                                        <th>Form</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @php $dateutility = new \App\Utility\DateUtility(); @endphp
                                                                                    @if(isset($businessLicense))
                                                                                        @foreach($businessLicense as $key=>$license)
                                                                                            <tr class="text-center">
                                                                                                <td>{{$license->year}}</td>
                                                                                                <td>{{$license->gross_revenue}}</td>
                                                                                                <td>{{$license->license_fee}}</td>
                                                                                                <td>{{$license->certificate_no}}</td>
                                                                                                <td>{{$dateutility->retriveDate($license->license_issue_date)}}</td>
                                                                                                <td class="text-center">
                                                                                                    <x-actionlink route="" id="" class="" buttons="view"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-status type="{{$license->license_status}}"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="" buttons="edit"/>
                                                                                                    <x-actionlink route="" id="" class="" buttons="delete"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <x-actionlink route="" id="" class="btn btn-primary" buttons="Create"/>
                                                                                                </td>

                                                                                            </tr>
                                                                                        @endforeach
                                                                                    @endif

                                                                                    </tbody>
                                                                                </table>

                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane  {{session()->has('insideNo') ? session()->get('insideNo') == 2 ? "active show" : "" :""}}" id="professional_license">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">

                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row mb-3">
																							<div class="col-sm-3">
																								<label class="col-form-label text-right">Profession :</label>
																								
																									<select type="text" class="form-control profession_option" id="profession" name="profession">
																										<option value="">Select</option>
																										@if(isset($professionList))
																											@foreach($professionList as $key=>$profession)
																												<option value="{{$profession->profession_name}}">{{$profession->profession_name}}</option>
																											@endforeach
																										@endif
																									</select>
																								
																							</div>
                                                                                            <div class="col-sm-3">
																								<label class="col-form-label text-right">State :</label>
                                                                                            
                                                                                                <x-state data="profession_state" message=""/>
                                                                                            
                                                                                            </div>
																							<div class="col-sm-3">
																								 <label class="col-form-label text-right" for="professional_license_state">Type Of License :</label>
                                                                                            
                                                                                                <select type="text" class="form-control profession_option" id="type_of_license" name="type_of_license">
                                                                                                    <option value="">Select License</option>
                                                                                                    <option value="Profession License">Profession License</option>
                                                                                                    <option value="Firm License">Firm License</option>
                                                                                                </select>
                                                                                            
																							</div>
																							<div class="col-sm-3">
																								<a href="{{route('admin.licenseHistroy')}}" class="btn mt-4 btn-primary text-center col-md-12">Search</a>
																							</div>
                                                                                        </div>
																						
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th>Profession</th>
                                                                                                <th>State</th>
                                                                                                <th>License Type</th>
                                                                                                <th>License Period</th>
                                                                                                <th>License #</th>
                                                                                                <th>Effective Date</th>
                                                                                                <th>Expire Date</th>
                                                                                                <th>CE Status</th>
                                                                                                <th>Action</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            @if(isset($professionalLicenseList))
                                                                                                @foreach($professionalLicenseList as $key=>$professionalLicense)
                                                                                                    <tr>
                                                                                                        <td>{{$professionalLicense->profession}}</td>
                                                                                                        <td class="text-center">{{$professionalLicense->professional_license_state}}</td>
                                                                                                        <td>{{$professionalLicense->type_of_license}}</td>
                                                                                                        <td class="text-center">{{$professionalLicense->professional_license_period}}</td>
                                                                                                        <td class="text-center">{{$professionalLicense->professional_license_no}}</td>
                                                                                                        <td class="text-center">{{$dateUtility->retriveDateInVal($professionalLicense->professional_effective_date)}}</td>
                                                                                                        <td class="text-center">{{$dateUtility->retriveDateInVal($professionalLicense->professional_expire_date)}}</td>
                                                                                                        <td class="text-center">{{$professionalLicense->is_ce_status ? 'Yes' : 'No'}}</td>
                                                                                                        <td>
                                                                                                            <a href="javascript:void(0)" onclick="editProfessionalLicense({{$professionalLicense->id}})"><i class="fas fa-edit"></i> </a>
                                                                                                            <a href="javascript:void(0)" onclick="deleteProfessionalLicense({{$professionalLicense->id}})"><i class="fas fa-trash"></i> </a>
                                                                                                            <a href="javascript:void(0)" class="text-success" onclick="deleteProfessionalLicense({{$professionalLicense->id}})"><i class="fas fa-history"></i> </a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                @endforeach
                                                                                            @endif

                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div> <!-- end card body-->
                                                                        </div> <!-- end card -->
                                                                    </div><!-- end col-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!-- end card -->
                                            </div>
                                        </div>
                                    </div>
									<div class="tab-pane fade{{session()->has('worktabno') ? " active show" :" "}}  fade" id="v-pills-taxation" role="tabpanel" aria-labelledby="v-pills-license-tab">
										<ul class="nav nav-pills navtab-bg nav-justified">
                                            <li class="nav-item">
                                                <a href="#Income" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                    <span class="d-none d-sm-inline-block">Income Tax</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#Payroll" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                    <span class="d-none d-sm-inline-block">Payroll Tax</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#Personal" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                                    <span class="d-none d-sm-inline-block">Personal Property Tax</span>
                                                </a>
                                            </li>
                                        </ul>
										<div class="tab-content">
                                            <div class="tab-pane show active" id="Income">
												<ul class="nav nav-pills navtab-bg nav-justified">
													<li class="nav-item">
														<a href="#Federal" data-toggle="tab" aria-expanded="false" class="nav-link active">
															<span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
															<span class="d-none d-sm-inline-block">Federal</span>
														</a>
													</li>
													<li class="nav-item">
														<a href="#State" data-toggle="tab" aria-expanded="true" class="nav-link ">
															<span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
															<span class="d-none d-sm-inline-block">State</span>
														</a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="tab-pane show active" id="Federal">
														<div class="alert alert-Fsc " role="alert">
															<h4><span class="col-sm-4" style="float:left;">&nbsp;Federal - (Form-1120S)</span> <span class="col-sm-8">Federal Income Tax Record</span></h4>
														</div>
														
														<div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Income_Tax_Filing">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Federal Income Tax Record
                                                                                        </a>
                                                                                    </div>
														<div class="accordion custom-accordion" id="custom-accordion-one">
															<div class="card mb-2">
																<div class="card-header" id="headingNine">
																	<h5 class="m-0 position-relative">
																		<a class="custom-accordion-title text-reset d-block collapsed" data-toggle="collapse" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
																			2020<i class="mdi mdi-chevron-down accordion-arrow"></i>
																		</a>
																	</h5>
																</div>
						
																<div id="collapseNine" class="collapse" aria-labelledby="headingFour" data-parent="#custom-accordion-one" style="">
																	<div class="card-body">
																		<table id="corporation-renewal-table" class="table table-bordered table-striped">
																			<thead>
																			<tr>
																				<th>Year</th>
																				<th>Tax Return</th>
																				<th>Form No.</th>
																				<th>Filing Date</th>
																				<th>Filing Methods</th>
																				<th>Staus</th>
																				<th>Action</th>
																			</tr>
																			</thead>
		
																			<tbody>
																																																								<tr>
																						<td>2020</td>
																						<td>Extension</td>
																						<td>Form-7004</td>
																						<td>Jul-24 2020		</td>
																						<td>E-File</td>
																						<td>Accepted</td>
																						<td>
		 
																							<a href="javascript:void(0);" id="modal-Bank-records-1-model" data-toggle="modal" data-target="#modal-Bank-records-1" class="waves-effect waves-light"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
																																			<a href="" id="-model" class="waves-effect waves-light"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
																						</td>
																					</tr>
																																																	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															
														</div>
														<div class="accordion custom-accordion" id="custom-accordion-two">
																
															<div class="card mb-2">
																<div class="card-header" id="headingSeven">
																	<h5 class="m-0 position-relative">
																		<a class="custom-accordion-title text-reset collapsed d-block" data-toggle="collapse" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
																			2019 <i class="mdi mdi-chevron-down accordion-arrow"></i>
																		</a>
																	</h5>
																</div>
																<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#custom-accordion-two">
																	<div class="card-body">
																		<table id="corporation-renewal-table" class="table table-bordered table-striped">
																			<thead>
																			<tr>
																				<th>Year</th>
																				<th>Tax Return</th>
																				<th>Form No.</th>
																				<th>Filing Date</th>
																				<th>Filing Methods</th>
																				<th>Staus</th>
																				<th>Action</th>
																			</tr>
																			</thead>
		
																			<tbody>
																																																								<tr>
																						<td>2020</td>
																						<td>Extension</td>
																						<td>Form-7004</td>
																						<td>Jul-24 2020		</td>
																						<td>E-File</td>
																						<td>Accepted</td>
																						<td>
		 
																							<a href="javascript:void(0);" id="modal-Bank-records-1-model" data-toggle="modal" data-target="#modal-Bank-records-1" class="waves-effect waves-light"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
																																			<a href="" id="-model" class="waves-effect waves-light"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
																						</td>
																					</tr>
																																																	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															
														</div>
														
														
														
													</div>
													<div class="tab-pane " id="State">
														<div class="alert alert-Fsc " role="alert">
															<h4><span class="col-sm-4" style="float:left;">&nbsp;State - (Form-1120S)</span> <span class="col-sm-8">State Income Tax Record</span></h4>
														</div>
														
														<div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Income_Tax_Filing">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add State Income Tax Record
                                                                                        </a>
                                                                                    </div>
														<div class="accordion custom-accordion" id="custom-accordion-three">
															<div class="card mb-2">
																<div class="card-header" id="headingNine">
																	<h5 class="m-0 position-relative">
																		<a class="custom-accordion-title text-reset d-block collapsed" data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapseNine">
																			2020<i class="mdi mdi-chevron-down accordion-arrow"></i>
																		</a>
																	</h5>
																</div>
						
																<div id="collapse3" class="collapse" aria-labelledby="headingFour" data-parent="#custom-accordion-three" style="">
																	<div class="card-body">
																		<table id="corporation-renewal-table" class="table table-bordered table-striped">
																			<thead>
																			<tr>
																				<th>Year</th>
																				<th>Tax Return</th>
																				<th>Form No.</th>
																				<th>Filing Date</th>
																				<th>Filing Methods</th>
																				<th>Staus</th>
																				<th>Action</th>
																			</tr>
																			</thead>
		
																			<tbody>
																																																								<tr>
																						<td>2020</td>
																						<td>Extension</td>
																						<td>Form-7004</td>
																						<td>Jul-24 2020		</td>
																						<td>E-File</td>
																						<td>Accepted</td>
																						<td>
		 
																							<a href="javascript:void(0);" id="modal-Bank-records-1-model" data-toggle="modal" data-target="#modal-Bank-records-1" class="waves-effect waves-light"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
																																			<a href="" id="-model" class="waves-effect waves-light"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
																						</td>
																					</tr>
																																																	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															
														</div>
														
													</div>
												</div>
                                            </div>
                                            <div class="tab-pane  " id="Payroll">
                                                <div class="alert alert-Fsc " role="alert">
															<h4><span class="col-sm-4" style="float:left;">&nbsp;Federal / State</span> <span class="col-sm-8">Payroll Tax Record</span></h4>
														</div>
														
														<div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Income_Tax_Filing">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Payroll Tax Record
                                                                                        </a>
                                                                                    </div>
																					<div class="accordion custom-accordion" id="custom-accordion-three">
															<div class="card mb-2">
																<div class="card-header" id="headingNine">
																	<h5 class="m-0 position-relative">
																		<a class="custom-accordion-title text-reset d-block collapsed" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapseNine">
																			2012<i class="mdi mdi-chevron-down accordion-arrow"></i>
																		</a>
																	</h5>
																</div>
						
																<div id="collapse4" class="collapse" aria-labelledby="headingFour" data-parent="#custom-accordion-three" style="">
																	<div class="card-body">
																		<table id="corporation-renewal-table" class="table table-bordered table-striped">
																			<thead>
																			<tr>
																				<th>Year</th>
																				<th>Tax Return</th>
																				<th>Form No.</th>
																				<th>Filing Date</th>
																				<th>Filing Methods</th>
																				<th>Staus</th>
																				<th>Action</th>
																			</tr>
																			</thead>
		
																			<tbody>
																																																								<tr>
																						<td>2020</td>
																						<td>Extension</td>
																						<td>Form-7004</td>
																						<td>Jul-24 2020		</td>
																						<td>E-File</td>
																						<td>Accepted</td>
																						<td>
		 
																							<a href="javascript:void(0);" id="modal-Bank-records-1-model" data-toggle="modal" data-target="#modal-Bank-records-1" class="waves-effect waves-light"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
																																			<a href="" id="-model" class="waves-effect waves-light"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
																						</td>
																					</tr>
																																																	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															
														</div>
														
                                            </div>
                                            <div class="tab-pane" id="Personal">
                                                <div class="alert alert-Fsc " role="alert">
															<h4><span class="col-sm-4" style="float:left;">&nbsp;County -</span> <span class="col-sm-8">Personal Property Tax Record</span></h4>
														</div>
														
														<div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Income_Tax_Filing">
                                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Personal Property Tax Record
                                                                                        </a>
                                                                                    </div>
																					<div class="accordion custom-accordion" id="custom-accordion-three">
															<div class="card mb-2">
																<div class="card-header" id="headingNine">
																	<h5 class="m-0 position-relative">
																		<a class="custom-accordion-title text-reset d-block collapsed" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapseNine">
																			2012<i class="mdi mdi-chevron-down accordion-arrow"></i>
																		</a>
																	</h5>
																</div>
						
																<div id="collapse4" class="collapse" aria-labelledby="headingFour" data-parent="#custom-accordion-three" style="">
																	<div class="card-body">
																		<table id="corporation-renewal-table" class="table table-bordered table-striped">
																			<thead>
																			<tr>
																				<th>Year</th>
																				<th>Tax Return</th>
																				<th>Form No.</th>
																				<th>Filing Date</th>
																				<th>Filing Methods</th>
																				<th>Staus</th>
																				<th>Action</th>
																			</tr>
																			</thead>
			
																			<tbody>
																																																								<tr>
																						<td>2020</td>
																						<td>Extension</td>
																						<td>Form-7004</td>
																						<td>Jul-24 2020		</td>
																						<td>E-File</td>
																						<td>Accepted</td>
																						<td>
		 
																							<a href="javascript:void(0);" id="modal-Bank-records-1-model" data-toggle="modal" data-target="#modal-Bank-records-1" class="waves-effect waves-light"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
																																			<a href="" id="-model" class="waves-effect waves-light"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
																						</td>
																					</tr>
																																																	</tbody>
																		</table>
																	</div>
																</div>
															</div>
															
														</div>
														
                                            </div>
                                        </div>
									</div>
                                </div>
                            </div> <!-- end col-->

                        </div> <!-- end row-->

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')
    <!--  Modal content for the Large example -->
    <div class="modal fade" id="bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title text-center" id="myLargeModalLabel">Document Name</h4>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="certificate"><span id="certificateText">Document name</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="document_name" class="form-control" id="document_names">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" id="create_document_name">Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xl-12 col-sm-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Document Name</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Docs</td>
                                    <td>
                                        <a href="javascript:void(0);"><i class="ri ri-2x ri-pencil-line text-success"></i></a>
                                        <a href="javascript:void(0);"><i class="ri ri-2x ri-delete-bin-2-line text-danger"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Add Corporation Renewal Records -->
    <!-- start -->
	 <div class="modal fade" id="modal_Income_Tax_Filing" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
			
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Income Tax Filing - (1120S) Add Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form parsley-examples" action="{{route('admin.storeCorporationLicense')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row mb-3">
                                    <div class="col-md-12 form-inline custom-checkbox">
										<div class="col-md-1">
										</div>
										<div class="col-md-10 form-inline">
											<div class="col-md-4">
											<input type="checkbox" class="custom-control-input checkmeout1" id="checkmeout1">
											<label class="custom-control-label" style="float:left;" for="checkmeout1">Both( Federal/State )</label>
										</div>
										<div class="col-md-4">
											<input type="checkbox" class="custom-control-input checkmeout2" id="checkmeout2">
											<label class="custom-control-label" style="float:left;" for="checkmeout2">Federal</label>                                        
										</div>
										<div class="col-md-4">
											<input type="checkbox" class="custom-control-input checkmeout3" id="checkmeout3">
											<label class="custom-control-label" style="float:left;" for="checkmeout3">State</label>
										</div>                                      
										</div>
										<div class="col-md-1">
										</div>
                                    </div>
                                </div>
								<div class="form_F" style="display:none;">
									<div class="form-group row mb-3" >
									<div class="col-md-12 form-inline custom-checkbox">
										<div class="col-md-3 lbl_title" style="text-align:left;">
											<label style="float:left;">Filling Year :</label>
										</div>
										<div class="col-md-4 div_D">
											<select class="form-control federalyear div_F" style="width:100%;" name="federalyear">
												<option value="">Select</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
												<option value="2023">2023</option>
											</select>                                    
										</div>
										<div class="col-md-4 ">
											<select class="form-control statesyear div_S" style="width:100%;" name="statesyear">
												<option value="">Select</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
												<option value="2023">2023</option>
											</select>                                  
										</div>
										<div class="col-md-1">
										</div>
                                    </div>
                                </div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Tax Return :</label>
											</div>
											<div class="col-md-4 div_D ">
												<select class="form-control taxation div_F" style="width:100%;" name="federaltax">
													<option value="">Select</option>
													<option value="Extension">Extension </option>
													<option value="Original">Original</option>
													<option value="Amendment">Amendment</option>
												</select>                                   
											</div>
											<div class="col-md-4 ">
												<select class="form-control taxation2 div_S" style="width:100%;" name="statestax">
													<option value="">Select</option>
														<option value="Extension">Extension </option>
														<option value="Original">Original</option>
														<option value="Amendment">Amendment</option>
												</select>                             
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Form No. :</label>
											</div>
											<div class="col-md-4 div_D ">
												<input type="text" readonly="" style="width:100%;" class="form-control div_F formno" name="federalformno">                             
											</div>
											<div class="col-md-4 ">
												<input type="text" class="form-control div_S formno2" style="width:100%;" name="statesformno" readonly="">                           
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Due Date :</label>
											</div>
											<div class="col-md-4 div_D ">
												<input type="text" class="form-control div_F duedate" style="width:100%;" name="federalduedate" placeholder="Mar-15-2020" readonly="">                                
											</div>
											<div class="col-md-4 ">
												<input type="text" class="form-control div_S duedate2" style="width:100%;" name="statesduedate" placeholder="Mar-15-2020" readonly="">                          
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Filing Method :</label>
											</div>
											<div class="col-md-4 div_D ">
												<select class="form-control div_F filingmethod" style="width:100%;" name="federalmethod">
                        <option 	value="">Select</option>
                        <option 	value="E-File">E-File</option>
                        <option 	value="Paperfile">Paperfile</option>
                    </select>   	                              
											</div>
											<div class="col-md-4 ">
												<select class="form-control div_S filingmethod2" style="width:100%;" name="statesmethod">
                        <option 	value="">Select</option>
                        <option 	value="E-File">E-File</option>
                        <option 	value="Paperfile">Paperfile</option>
                    </select>   	                         
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Filing Date :</label>
											</div>
											<div class="col-md-4 div_D ">
												<input type="date" class="form-control div_F" style="width:100%;" id="fillingdate" name="federaldate"> 
											</div>
											<div class="col-md-4 ">
												<input type="date" class="form-control div_S" style="width:100%;" id="fillingdate2" name="statesdate">
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Status of Filling :</label>
											</div>
											<div class="col-md-4 div_D ">
												<select class="form-control div_F" style="width:100%;" name="federalstatus">
                        <option 	value="">Select</option>
                        <option 	value="Accepted">Accepted</option>
                        <option 	value="Rejected">Rejected</option>
                        <option 	value="Pending">Pending</option>
                    </select>   	         
											</div>
											<div class="col-md-4 ">
												<select class="form-control div_S" style="width:100%;" name="statesstatus">
                        <option 	value="">Select</option>
                        <option 	value="Accepted">Accepted</option>
                        <option 	value="Rejected">Rejected</option>
                        <option 	value="Pending">Pending</option>
                    </select>   	      
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Add File :</label>
											</div>
											<div class="col-md-4 div_D ">
												<input type="file" class="div_F" style="width:100%;" name="federalfile">          
											</div>
											<div class="col-md-4 ">
												<input type="file" class="div_S" style="width:100%;" name="statesfile"> 
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3">
										<div class="col-md-12 form-inline custom-checkbox">
											<div class="col-md -3 lbl_title" style="text-align:left;">
												<label style="float:left;">Note :</label>
											</div>
											<div class="col-md-4 div_D ">
												<textarea class="form-control div_F" style="width:100%;" name="federalnote"></textarea>            
											</div>                             
											<div class="col-md-4 ">             
												<textarea class="form-control div_S" style="width:100%;" name="statesnote"></textarea>       
											</div>
											<div class="col-md-1">
											</div>
										</div>
									</div>
									<div class="form-group row mb-3 text-center">
                                    <div class="col-md-3">
									</div>
                                        
                                    <div class="col-md-9" style="text-align:left;">
										<span>&nbsp;</span>
                                        <input type="submit" value="submit" class="col-md-3 btn btn-primary text-center">
										<button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-3 btn btn-danger text-center">Cancel</button>
									</div>
                                </div>
								</div> 
						   </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	
    <div class="modal fade" id="modal-corporation-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Corporation Renew Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form parsley-examples" action="{{route('admin.storeCorporationLicense')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="admin_id" value="1"/>
                                <input type="hidden" name="corporation_id" id="corporation_id"/>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="renewal_for"><span id="certificateText">Renew For </span></label>
                                    <div class="col-md-3">
                                        <select name="renewal_for" id="renewal_for" class="form-control">
                                            <option value="1">1 Year</option>
                                            <option value="2">2 Year</option>
                                            <option value="3">3 Year</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="renewal_year"><span id="certificateText">Renewal Year</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="renewal_year" class="form-control" id="renewal_year" readonly>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="renewal_amount"><span id="certificateText">Renew Amount</span></label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="renewal_amount" class="form-control" id="renewal_amount" readonly>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="paid_amount"><span id="certificateText">Paid Amount</span></label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="paid_amount" class="form-control" id="paid_amount" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="method_of_payment"><span id="certificateText">Method Of Payment</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="method_of_payment" class="form-control" id="method_of_payment">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="corporation_status"><span id="certificateText">Status</span></label>
                                    <div class="col-md-9">
                                        <select name="corporation_status" id="corporation_status" class="form-control" required>
                                            <option value="">Select</option>
                                            <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                            <option value="Active Compliance">Active Compliance</option>
                                            <option value="Active Noncompliance">Active Noncompliance</option>
                                            <option value="Admin. Dissolved">Admin. Dissolved</option>
                                            <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="annually_receipt"><span id="certificateText">Annually Receipt</span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="annually_receipt" class="" id="annually_receipt" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="officer"><span id="certificateText">Officer</span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="officer" class="" id="officer" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="website"><span id="certificateText">Website</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="website" class="form-control" id="website">
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <input type="submit" value="submit" class="col-md-5 btn btn-primary text-center">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Record of share Ledger -->
    <!-- start -->
    <div class="modal fade" id="modal-share-Ledger-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Share Ledger Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" action="{{route('admin.storeShareLedger')}}">
                                @csrf
                                <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="holder_name">
                                        <span id="certificateText">Name of Certificate of Holder</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="holder_name" name="holder_name" placeholder="Holder Name" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="issue_certificate">
                                        <span id="certificateText">Certificates issued No. Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="issue_certificate" name="issue_certificate" placeholder="Certificates Issue No of Shares" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="origine_issue_from">
                                        <span id="certificateText">From whom Transferred (If Original issue Enter as such)</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="origine_issue_from" name="origine_issue_form" placeholder="From Transfer">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="paid_amount">
                                        <span id="certificateText">Amount paid thereon</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control txtinput_1" id="paid_amount" name="paid_amount" placeholder="Paid Amount">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="transfer_date">
                                        <span id="certificateText">Date of Transfer of Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepickers" id="transfer_date" name="transfer_date" placeholder="mm/dd/yyyy" readonly="" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="surrender_certificate">
                                        <span id="certificateText">Certificates Surrendered certif. Nos. </span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="surrender_certificate" name="surrender_certificate" placeholder="Surrender Certificate No">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-5 col-form-label text-right" for="surrender_certificate_no">
                                        <span id="certificateText">Certificates Surrendered No. Shares</span>
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="surrender_certificate_no" name="surrender_certificate_no" placeholder="Surrender Certificate No of Shares" required="">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Business License  -->
    <!-- start -->
    <div class="modal fade" id="modal-Business-License-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Business License Record</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" action="{{route('admin.storeBusinessLicense')}}" method="post" id="businessLicense" name="businessLicense" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="certificateText">Year </span></label>
                                    <div class="col-md-3">
                                        <select name="year" id="year" class="form-control" required>
                                            <option value="">Select</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="gross_revenue"><span id="certificateText">Gross Revenue</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="gross_revenue" class="form-control" id="gross_revenue" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="license_fee">License Fee </span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="license_fee" class="form-control" id="license_fee" required>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="license_status"><span id="certificateText">License Status</span></label>
                                    <div class="col-md-3">
                                        <select name="license_status" id="license_status" class="form-control">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                            <option value="Hold">Hold</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_copy"><span id="license_fee">License Copy </span></label>
                                    <div class="col-md-9">
                                        <input type="file" name="license_copy" class="" id="license_copy">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="year"><span id="license_issue_date">License Issue Date </span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="license_issue_date" class="form-control datepickers" id="license_issue_date" required>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="certificate_no"><span id="certificateText">Certificate No</span></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="certificate_no" id="certificate_no">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_website"><span id="license_fee">Website</span></label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="http:// or https:// required" name="license_website" class="form-control" id="license_website">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="license_note"><span id="license_fee">Note </span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="license_note" class="form-control" id="license_note">
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/workstatus.js')}}"></script>
	<script>
		$('#checkmeout1').bind('change', function () {
		
			if ($(this).is(':checked'))
			{
				$(".form_F").css("display","block");
				$(".div_F").css("display","block");
				$(".div_D").css("display","block");
				$(".div_S").css("display","block");
				$('.checkmeout2 input:checkbox'). prop('checked', false);
				$('.checkmeout3 input:checkbox'). prop('checked', false);
			}
			else
			{
				$(".form_F").css("display","none");
				$(".div_D").css("display","none");
				$(".div_F").css("display","none");
				$(".div_S").css("display","none");
			}
		});
		 $('#checkmeout2').bind('change', function () {
		
			 if ($(this).is(':checked'))
			 {
				 $('.checkmeout1 input:checkbox'). prop('checked', false);
				 $('.checkmeout3 input:checkbox'). prop('checked', false);
				$(".form_F").css("display","block");
				$(".div_D").css("display","block");
				$(".div_F").css("display","block");
				$(".div_S").css("display","none");
			 }
			 else
			 {
		      	$(".form_F").css("display","none");
				$(".div_F").css("display","none");
				$(".div_D").css("display","none");
				$(".div_S").css("display","none");
			 }
		 });
		 $('#checkmeout3').bind('change', function () {
		
			 if ($(this).is(':checked'))
			 {
				 $('.checkmeout1 input:checkbox'). prop('checked', false);
				 $('.checkmeout2 input:checkbox'). prop('checked', false);
				$(".form_F").css("display","block");
				$(".div_F").css("display","none");
				$(".div_D").css("display","none");
				$(".div_S").css("display","block");
			 }
			 else
			 {	
				$(".form_F").css("display","none");
				$(".div_F").css("display","none");
				$(".div_D").css("display","none");
				$(".div_S").css("display","none");
			 }		
		 });
	</script>
@endsection
