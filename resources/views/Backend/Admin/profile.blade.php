@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <style>

    </style>
    <!-- Start Content-->
    <div class="container-fluid">
    @php $dateFormat = new \App\Utility\DateUtility(); @endphp
    <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Administrator</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">


                        <div class="alert alert-Fsc Header text-center" role="alert">
                            <h4> Admin Profile </h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist">
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 1 ? "active" : '' : 'active' }}  show mb-1" id="v-pills-company-tab" data-toggle="pill" href="#v-pills-company"
                                       aria-selected="true" disabled>
                                        Company Info</a>
                                    <a class="nav-link  {{session()->has('tab_no') ? session()->get('tab_no')== 2 ? "active" : '' : '' }}  mb-1" id="v-pills-contact-tab" data-toggle="pill" href="#v-pills-contact"
                                       aria-selected="false" disabled>
                                        Contact Info</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 3 ? "active" : '' : '' }}  mb-1" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
                                       aria-selected="false">
                                        Security Info</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 4 ? "active" : '' : '' }}  mb-1" id="v-pills-formation-tab" data-toggle="pill" href="#v-pills-formation"
                                       aria-selected="false">
                                        Formation</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? "active" : '' : '' }}  mb-1" id="v-pills-license-tab" data-toggle="pill" href="#v-pills-license"
                                       aria-selected="true">
                                        License</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 6 ? "active" : '' : '' }}  mb-1" id="v-pills-taxation-tab" data-toggle="pill" href="#v-pills-taxation"
                                       aria-selected="false">
                                        Taxation</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 7 ? "active" : '' : '' }}  mb-1" id="v-pills-banking-tab" data-toggle="pill" href="#v-pills-banking"
                                       aria-selected="false">
                                        Banking</a>
                                    <a class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 8 ? "active" : '' : '' }}  mb-1" id="v-pills-other-tab" data-toggle="pill" href="#v-pills-other"
                                       aria-selected="false">
                                        Others</a>
                                </div>
                            </div> <!-- end col-->


                            <div class="col-sm-10">
                                <div class="tab-content pt-0">

                                    @if (session()->has('ProfileSuccess') )<br><br>
                                    <div class="alert alert-success alert-dismissable systemGenMessage col-md-12 text-center ">
                                        <h4>{{ session()->get('ProfileSuccess') }} </h4>
                                    </div>
                                    @endif
                                    @if (session()->has('ProfileError'))<br><br>
                                    <div class="alert alert-danger systemGenMessage col-md-12 text-center">
                                        <h4>{{ session()->get('ProfileError') }}</h4>
                                    </div>
                                    @endif


                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 1 ? "active show" : '' : 'active show' }} fade" id="v-pills-company" role="tabpanel" aria-labelledby="v-pills-company-tab">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="{{route('admin.storeProfile')}}" method="post" name="storeProfile" id="storeProfile">
                                                    @csrf
                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            @foreach ($errors->all() as $error)
                                                                <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                    <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                                    <input type="hidden" @if(isset($isProfileExsist)) @if($isProfileExsist > 0) name=profile_id @endif @endif value="1">
                                                    <div class="alert alert-Fsc text-center" role="alert">
                                                        <h4>Company Inforamtion</h4>
                                                    </div>
                                                    <hr/>
                                                    <div class="form-group row mb-3">

                                                        <div class="col-md-3">
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <label class="col-form-label text-right" for="company_name_lbl">Company Name : <span class="text-danger">*</span></label>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <cite title="Source Title">(Legal Name)<span>&nbsp;&nbsp;</span></cite>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" id="company_name_lbl" value="@if($isProfileExsist > 0) {{$companyInfo->company_name}} @else {{old('company_name')}} @endif" name="company_name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <div class="col-md-3">
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <label class="col-form-label text-right" for="business_name"> Business Name : <span class="text-danger">*</span></label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <cite title="Source Title">(DBA)<span>&nbsp;&nbsp;</span></cite>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-9">
                                                            <input type="text" id="business_name" value="@if($isProfileExsist > 0) {{$companyInfo->business_name}} @else {{old('business_name')}} @endif" name="business_name" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="reg_address_1">Address 1 : <span class="text-danger">*</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->address_1}} @else {{old('address_1')}} @endif" id="reg_address_1" name="address_1">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="reg_address_2">Address 2 : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->address_2}} @else {{old('address_2')}} @endif" id="reg_address_2" name="address_2">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="reg_city">City / State / Zip : <span class="text-danger">*</span></label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->city}} @else {{old('city')}} @endif" id="reg_city" name="city">
                                                        </div>
                                                        <div class="col-md-3">
                                                            @if($isProfileExsist > 0)
                                                                <x-state data="comp_state" message="{{$companyInfo->comp_state}}"/>
                                                            @else
                                                                <x-state data="comp_state" message=""/>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-usa-zip-mask" value="@if($isProfileExsist > 0){{$companyInfo->zip}}@else{{old('zip')}}@endif" id="reg_zipcode" name="zip">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <label class="col-form-label col-md-3 text-right">Telephone # : <span class="text-danger">*</span></label>
                                                        <div class="col-md-3">
                                                            <input name="contact_no" type="text" value="@if($isProfileExsist > 0){{$companyInfo->contact_no}}@else{{old('contact_no')}}@endif" id="contact_no" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select name="contact_no_type" id="contact_no_type" class="form-control">
                                                                <option value="Office" {{$isProfileExsist > 0 ? $companyInfo->contact_no_type == "Office" ? "Selected" : "" :""}}>Office</option>
                                                                <option value="Mobile" {{$isProfileExsist > 0 ? $companyInfo->contact_no_type == "Mobile" ? "Selected" : "" :""}}>Mobile</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input class="form-control input-extension-mask" id="ext1" maxlength="5" name="extension" value="@if($isProfileExsist > 0){{$companyInfo->extension}}@else{{old('extension')}}@endif" placeholder="Ext" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="fax_no">Fax # : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control input-fax-no-mask" value="@if($isProfileExsist > 0){{$companyInfo->fax_no}}@else{{old('fax_no')}}@endif" id="fax_no" name="fax_no">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="email">Email : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0){{$companyInfo->email}}@else{{old('email')}}@endif" id="email" name="email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="website">Website : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0){{$companyInfo->website}}@else{{old('website')}}@endif" id="website" name="website">
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="alert alert-Fsc text-center" role="alert">
                                                        <h4>Physical Address</h4>
                                                    </div>
                                                    <hr/>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="pa_address_1"></label>
                                                        <div class="custom-checkbox col-md-9">
                                                            <span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input type="checkbox" id="checkmeout0" name="same_as_company_info" value="@if($isProfileExsist > 0) 1 @else 0 @endif" @if($isProfileExsist > 0) checked @endif class="form-control custom-control-input">
                                                            <label class="custom-control-label" for="checkmeout0">Same as Company Address</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="pa_address_1">Physical Address1 : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control hasCompanyReadonly" id="pa_address_1" value="@if($isProfileExsist > 0) {{$companyInfo->pa_address_1}} @else {{old('pa_address_1')}} @endif" name="pa_address_1">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="pa_address_2">Physical Address2 : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control hasCompanyReadonly" id="pa_address_2" value="@if($isProfileExsist > 0) {{$companyInfo->pa_address_2}} @else {{old('pa_address_2')}} @endif" name="pa_address_2">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="pa_city">City / State / Zip : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control hasCompanyReadonly" value="@if($isProfileExsist > 0) {{$companyInfo->pa_city}} @else {{old('pa_city')}} @endif" id="pa_city" name="pa_city">
                                                        </div>
                                                        <div class="col-md-3 d-none" id="inputTag">
                                                            <input type="text" class="form-control hasCompanyReadonly" value="@if($isProfileExsist > 0) {{$companyInfo->pa_state}} @else {{old('pa_state')}} @endif" id="pa_state_input" name="pa_state">
                                                        </div>
                                                        <div class="col-md-3" id="selectTag">
                                                            @if($isProfileExsist > 0)
                                                                <x-state data="pa_state" message="{{$companyInfo->pa_state}}"/>
                                                            @else
                                                                <x-state data="pa_state" message=""/>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control hasCompanyReadonly input-usa-zip-mask" value="@if($isProfileExsist > 0) {{$companyInfo->pa_zip}} @else {{old('pa_zip')}} @endif" id="pa_zip" name="pa_zip">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label text-right" for="county">County / Code : <span class="text-danger">&nbsp;</span></label>
                                                        <div class="col-md-6">
                                                            <select id="select_county" class="form-control fsc-input">
                                                                <option value="">Select County</option>
                                                                @if(isset($taxCountyList))
                                                                    @foreach($taxCountyList as $key=>$taxCounty)
                                                                        <option value="{{$taxCounty->county_code}}"
                                                                            {{$companyInfo->county_code == $taxCounty->county_code ? "selected" : ''}}>{{$taxCounty->county_name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <input type="hidden" id="county" name="county" value="{{$companyInfo->county_code}}"/>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control" value="@if($isProfileExsist > 0) {{$companyInfo->county_code}} @else {{old('county_code')}} @endif" id="county_code" name="county_code" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <div class="col-md-12 text-center">
                                                            @if(isset($isProfileExsist))
                                                                @if($isProfileExsist > 0)
                                                                    <button type="submit" class="col-md-2 btn btn-primary">Update</button>
                                                                @else
                                                                    <button type="submit" class="col-md-2 btn btn-primary">Submit</button>
                                                                @endif
                                                            @endif

                                                            <button type="reset" class=" col-md-2 btn btn-danger">Cancel</button>
                                                        </div>
                                                        {{--                                                <label class="col-md-3 col-form-label text-right" for="county"></label>--}}


                                                    </div>
                                                </form>


                                            </div>
                                        </div>


                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 2 ? "active show" : '' : '' }} fade" id="v-pills-contact" role="tabpanel" aria-labelledby="v-pills-contact-tab">
                                        <form action="{{route('admin.storeContact')}}" name="storeContact" id="storeContact" method="post">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif

                                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                            <input type="hidden" @if(isset($isContactExist)) @if($isContactExist > 0) name=contact_id value="{{$contactInfo->id}} @endif @endif ">
                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4>Contact Information</h4>
                                            </div>
                                            <hr/>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="name">Name :<span class="text-danger">&nbsp;&nbsp;</span></label>
                                                <div class="col-md-2">
                                                    <select name="prefix_name" id="prefix_name" class="form-control">
                                                        <option value="Mr" {{$isContactExist > 0 ? $contactInfo->prefix_name == "Mr" ? "Selected" : "" :""}}>Mr.</option>
                                                        <option value="Mrs" {{$isContactExist > 0 ? $contactInfo->prefix_name == "Mrs" ? "Selected" : "" :""}}>Mrs.</option>
                                                        <option value="Miss" {{$isContactExist > 0 ? $contactInfo->prefix_name == "Miss" ? "Selected" : "" :""}}>Miss.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="fname" value="@if($isContactExist > 0) {{$contactInfo->fname}} @else {{old('fname')}} @endif">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="name" name="mname" value="@if($isContactExist > 0) {{$contactInfo->mname}} @else {{old('mname')}} @endif">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="lname" value="@if($isContactExist > 0) {{$contactInfo->lname}} @else {{old('lname')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address 1 :<span class="text-danger">&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_1" value="@if($isContactExist > 0) {{$contactInfo->address_1}} @else {{old('address_1')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address 2 :<span class="text-danger">&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_2" value="@if($isContactExist > 0) {{$contactInfo->address_2}} @else {{old('address_2')}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / State / Zip :<span class="text-danger">&nbsp;&nbsp;</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="city" value="@if($isContactExist > 0) {{$contactInfo->state}} @else {{old('city')}} @endif">
                                                </div>
                                                <div class="col-md-3">
                                                    @if($isContactExist > 0)
                                                        <x-state data="state" message="{{$contactInfo->state}}"/>
                                                    @else
                                                        <x-state data="state" message="GA"/>
                                                    @endif
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-usa-zip-mask" id="reg_city" name="zip" value="@if($isContactExist > 0){{$contactInfo->zip}}@else{{old('zip')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone # 1 : <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_1" value="@if($isContactExist > 0){{$contactInfo->contact_no_1}}@else{{old('contact_no_1')}}@endif" type="tel" id="contact_no_1" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_1" id="contact_no_type_1" class="form-control">
                                                        <option value="Office" {{$isContactExist > 0 ? $contactInfo->contact_no_type_1 == "Office" ? "Selected" : "" :""}}>Office</option>
                                                        <option value="Mobile" {{$isContactExist > 0 ? $contactInfo->contact_no_type_1 == "Mobile" ? "Selected" : "" :""}}>Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control input-extension-mask" id="contact_no_extension_1" maxlength="5" name="contact_no_extension_1" value="@if($isContactExist > 0){{$contactInfo->contact_no_extension_1}}@else {{old('contact_no_extension_1')}} @endif" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone # 2 : <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input name="contact_no_2" value="@if($isContactExist > 0){{$contactInfo->contact_no_2}}@else{{old('contact_no_2')}}@endif" type="tel" id="contact_no_2" class="form-control input-tel-mask" placeholder="(999) 999-9999"/>
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="contact_no_type_2" id="contact_no_type_2" class="form-control">
                                                        <option value="Office" {{$isContactExist > 0 ? $contactInfo->contact_no_type_2 == "Office" ? "Selected" : "" :""}}>Office</option>
                                                        <option value="Mobile" {{$isContactExist > 0 ? $contactInfo->contact_no_type_2 == "Mobile" ? "Selected" : "" :""}}>Mobile</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="form-control input-extension-mask" id="contact_no_extension_2" maxlength="5" name="contact_no_extension_2" value="@if($isContactExist > 0){{$contactInfo->contact_no_extension_2}}@else {{old('contact_no_extension_2')}} @endif" placeholder="Ext" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no"></label>
                                                <div class="col-md-9 custom-checkbox">
                                                    <span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <input type="checkbox" name="same_as_company_fax_no" value="1" class="custom-control-input" id="checkmeout1">
                                                    <label class="custom-control-label" for="checkmeout1">Same as Company Fax #</label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax # :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-fax-no-mask hasReadonly" id="contact_fax_no" name="fax_no" value="@if($isContactExist > 0){{$contactInfo->fax_no}}@else{{old('fax_no')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="email" name="email" value="@if($isContactExist > 0){{$contactInfo->email}}@else{{old('email')}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <label class="col-md-3 col-form-label text-right" for="county"></label>
                                                <div class="col-md-12 text-center">
                                                    @if(isset($isContactExist))
                                                        @if($isContactExist > 0)
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                    @endif
                                                    <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 3 ? "active show" : '' : '' }}  fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="{{route('admin.storeSecurity')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                            <input type="hidden" @if($isSecurityExist>0) name="security_id" value="{{$securityInfo->id}}" @endif>
                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4>Security Information</h4>
                                            </div>

                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Username :</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="{{Auth::user()->email}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Security Q.1 :</label>
                                                <div class="col-md-9">
                                                    <select name="que_1" id="que_1" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_1 == 1 ? 'selected' : ''}} @endif>What was your favorite place to visit as a child?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_1 == 2 ? 'selected' : ''}} @endif>Who is your favorite actor, musician, or artist?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_1 == 3 ? 'selected' : ''}} @endif>What is the name of your favorite pet?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_1 == 4 ? 'selected' : ''}} @endif>In what city were you born?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_1 == 5 ? 'selected' : ''}} @endif>What is the name of your first school?</option>
                                                    </select>
                                                    <input type="hidden" id="question1" name="question1" value="@if($isSecurityExist>0){{$securityInfo->question1}}@endif">
                                                </div>

                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer1">Answer 1 :</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer1" name="answer1" required value="@if($isSecurityExist>0){{$securityInfo->answer1}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="que_2">Security Q.2 :</label>
                                                <div class="col-md-9">
                                                    <select name="que_2" id="que_2" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_2 == 1 ? 'selected' : ''}} @endif>What is your favorite movie?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_2 == 2 ? 'selected' : ''}} @endif>What was the make of your first car?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_2 == 3 ? 'selected' : ''}} @endif>What is your favorite color?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_2 == 4 ? 'selected' : ''}} @endif>What is your father's middle name?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_2 == 5 ? 'selected' : ''}} @endif>What is the name of your first grade teacher?</option>
                                                    </select>
                                                    <input type="hidden" id="question2" name="question2" value="@if($isSecurityExist>0){{$securityInfo->question2}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer2">Answer 2 :</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer2" name="answer2" value="@if($isSecurityExist>0){{$securityInfo->answer2}}@endif" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="que_3">Security Q.3 :</label>
                                                <div class="col-md-9">
                                                    <select name="que_3" id="que_3" class="form-control">
                                                        <option value="">---Select---</option>
                                                        <option value="1" @if($isSecurityExist>0) {{$securityInfo->que_3 == 1 ? 'selected' : ''}} @endif>What was your high school mascot?</option>
                                                        <option value="2" @if($isSecurityExist>0) {{$securityInfo->que_3 == 2 ? 'selected' : ''}} @endif>Which is your favorite web browser?</option>
                                                        <option value="3" @if($isSecurityExist>0) {{$securityInfo->que_3 == 3 ? 'selected' : ''}} @endif>In what year was your father born?</option>
                                                        <option value="4" @if($isSecurityExist>0) {{$securityInfo->que_3 == 4 ? 'selected' : ''}} @endif>What is the name of your favorite childhood friend?</option>
                                                        <option value="5" @if($isSecurityExist>0) {{$securityInfo->que_3 == 5 ? 'selected' : ''}} @endif>What was your favorite food as a child?</option>
                                                    </select>
                                                    <input type="hidden" id="question3" name="question3" value="@if($isSecurityExist>0){{$securityInfo->question3}}@endif">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="answer3">Answer 3 :</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="answer3" name="answer3" value="@if($isSecurityExist>0){{$securityInfo->answer3}}@endif" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-12 text-center">
                                                    @if(isset($isSecurityExist))
                                                        @if($isSecurityExist > 0)
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                    @endif

                                                    <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 4 ? "active show" : '' : '' }}  fade" id="v-pills-formation" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="{{route('admin.storeFormation')}}" method="POST" name="storeFormation" enctype="multipart/form-data">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                            <input type="hidden" @if($isFormationExist > 0) name="formation_id" value="1" @endif>
                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4>Company Formation Information</h4>
                                            </div>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">State of Formation :</label>
                                                <div class="col-md-3">
                                                    @if($isFormationExist > 0)
                                                        <x-state data="state_of_formation" message="{{$formationInfo->state_of_formation}}"/>
                                                    @else
                                                        <x-state data="state_of_formation" message="GA"/>
                                                    @endif
                                                </div>
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Date of Incorporation :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control datepickers" id="date_of_incorporation" name="date_of_incorporation" value="@if($isFormationExist > 0){{date('m/d/Y',strtotime($formationInfo->date_of_incorporation))}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="legal_name">Legal Name :</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="legal_name" name="legal_name" value="@if($isProfileExsist > 0){{$companyInfo->company_name}}@endif" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Control Number :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="control_number" name="control_number" value="@if($isFormationExist > 0){{$formationInfo->control_number}}@endif">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" value="@if($isCorporationLicenseExist>0){{$corporationLicense->corporation_status}}@endif" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right"></label>
                                                <div class="col-sm-9" style="display:flex;">
                                                    <div class="custom-control custom-radio col-md-3 col-span-4">
                                                        @if($isFormationExist > 0)
                                                            @if($formationInfo->certificate != null)
                                                                <input type="radio" id="coporation" name="certificate_type" disabled value="1" class="custom-control-input" @if($isFormationExist > 0)@if($formationInfo->certificate_type == 1) checked @endif @endif">
                                                        @else
                                                            <input type="radio" id="coporation" name="certificate_type" value="1" class="custom-control-input" checked>
                                                        @endif
                                                        @else
                                                            <input type="radio" id="coporation" name="certificate_type" value="1" class="custom-control-input" checked>
                                                        @endif
                                                        <label class="custom-control-label" for="coporation">Corporation</label>
                                                    </div>
                                                    <div class="custom-control custom-radio  col-md-3">
                                                        @if($isFormationExist > 0)
                                                            @if($formationInfo->articles != null)
                                                                <input type="radio" id="llc" name="certificate_type" disabled value="2" class="custom-control-input" @if($isFormationExist > 0)@if($formationInfo->certificate_type == 2) checked @endif @endif">
                                                        @else
                                                            <input type="radio" id="llc" name="certificate_type" value="2" class="custom-control-input">
                                                        @endif
                                                        @else
                                                            <input type="radio" id="llc" name="certificate_type" value="2" class="custom-control-input">
                                                        @endif
                                                        <label class="custom-control-label" for="llc">LLC</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="certificate"><span id="certificateText">Certificate Of Corporation :</span></label>
                                                <div class="col-md-9">
                                                    <label class="col-md-5 btn btn-primary certificateViewer" id="certificateLabel" data-src="{{route('admin.viewDocument',1)}}">Certificate Of Corporation</label>
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->certificate != null)
                                                            <input type="hidden" name="certificate" class="form-control d-none" value="@if($isFormationExist > 0){{$formationInfo->certificate}}@endif">
                                                        @else
                                                            <input type="file" name="certificate" class="col-md-6" id="certificate">
                                                        @endif
                                                    @else
                                                        <input type="file" name="certificate" class="col-md-6" id="certificate">
                                                    @endif
                                                    {{--                                                    <label class="col-md-3 btn btn-warning @if($isFormationExist > 0) @if($formationInfo->certificate != null) d-none @endif @endif" for="certificate">Browser...</label>--}}
                                                    <label data-src="{{route('admin.removeCertificate')}}" data-token="{{csrf_token()}}" class="col-md-1 btn  @if($isFormationExist > 0) @if($formationInfo->certificate == null) d-none @endif @endif" id="certificateDelete"><i class="ri f-1-5x ri-delete-bin-2-line text-danger" style="font-size:35px;"></i></label>
                                                    <label data-src="{{route('admin.viewDocument',1)}}" class="col-md-1 btn certificateViewer @if($isFormationExist > 0) @if($formationInfo->certificate == null) d-none @endif @endif" id="certificateView"><i class="ri-file-pdf-line text-success" style="font-size:35px;"></i></label>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="article"><span id="articleText">Article Of Incorporation :</span></label>
                                                <div class="col-md-9">
                                                    <label class="col-md-5 btn btn-primary articlesViewer" id="articlelabel" data-src="{{route('admin.viewDocument',2)}}">Article Of Incorporation</label>
                                                    @if($isFormationExist > 0)
                                                        @if($formationInfo->articles != null)
                                                            <input type="hidden" name="articles" class="form-control d-none" value="@if($isFormationExist > 0){{$formationInfo->articles}}@endif">
                                                        @else
                                                            <input type="file" name="articles" class="col-md-6" id="articles">
                                                        @endif
                                                    @else
                                                        <input type="file" name="articles" class="col-md-6" id="articles">

                                                @endif
                                                <!--<label class="col-md-3 btn btn-warning @if($isFormationExist > 0) @if($formationInfo->articles != null) d-none @endif @endif" for="articles">Browser...</label>-->
                                                    <label data-src="{{route('admin.removeCertificate')}}" data-token="{{csrf_token()}}" class="col-md-1 btn  @if($isFormationExist > 0) @if($formationInfo->articles == null) d-none @endif @endif" id="articlesDelete"><i class="ri f-1-5x ri-delete-bin-2-line text-danger" style="font-size:35px;"></i></label>
                                                    <label data-src="{{route('admin.viewDocument',2)}}" class="col-md-1 btn articlesViewer @if($isFormationExist > 0) @if($formationInfo->articles == null) d-none @endif @endif" id="articlesViewer"><i class="ri-file-pdf-line text-success" style="font-size:35px;"></i></label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="agent_names">Agent Name :</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_fname" value="@if($isFormationExist > 0){{$formationInfo->agent_fname}}@endif">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_mname" value="@if($isFormationExist > 0){{$formationInfo->agent_mname}}@endif">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="agent_names" name="agent_lname" value="@if($isFormationExist > 0){{$formationInfo->agent_lname}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="renewal_date">Renewal Date :</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="renewal_date" name="renewal_date" value="@if($isCorporationLicenseExist > 0){{$renewal_date}}@endif">
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="{{route('admin.work-status')}}" class="btn btn-warning text-center col-md-12">Renew Record</a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="https://ecorp.sos.ga.gov/Account" target="_blank" class="btn btn-warning text-center col-md-12">Renew Here</a>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-12 text-center">
                                                    @if(isset($isFormationExist))
                                                        @if($isFormationExist > 0)
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                                        @else
                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                        @endif
                                                    @else
                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                    @endif
                                                    <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-Fsc text-center" role="alert">
                                                    <h4>Shareholder / Officer Information</h4>
                                                </div>
                                                <hr/>

                                                <div class="row">
                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                        <a href="javascript:void(0);" data-href="{{route('models.share-holder')}}" class="btn btn-warning waves-effect waves-light " data-toggle="modal" data-target="#modal-Share-holder-records">
                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Share Holder
                                                        </a>
                                                        @if(isset($shareHolders))
                                                            @if(count($shareHolders) > 0)
                                                                @if(isset($totalPercentageOfShareholder))
                                                                    @if($totalPercentageOfShareholder!=100)

                                                                    @else
                                                                        {{--                                                                        <button href="javascript:void(0);" id="alert_shareholder" class="btn btn-warning waves-effect waves-light">--}}
                                                                        {{--                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Share Holder--}}
                                                                        {{--                                                                        </button>--}}
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif

                                                    </div>
                                                    <div class="col-md-6 col-xl-6 col-sm-6">
                                                        <h5 class="text-danger" id="CheckShareHolderPercentage"> Total Percentage should be 100.00% </h5>
                                                    </div>
                                                </div>
                                                <table id="Share-Holder-table" class="table table-bordered table-hover table-centered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>M</th>
                                                        <th>Last Name</th>
                                                        <th>Position</th>
                                                        <th>Percentage</th>
                                                        <th>Effective Date</th>
                                                        <th>Status</th>
                                                        <th width="7%">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($shareHolders))
                                                        @foreach($shareHolders as $key=>$value)
                                                            <tr>
                                                                <td>{{$key+1}}</td>
                                                                <td>{{$value->fname}}</td>
                                                                <td>{{$value->mname}}</td>
                                                                <td>{{$value->lname}}</td>
                                                                <td>{{$value->position}}</td>
                                                                <td class="text-right">{{$value->percentage}}.00%</td>
                                                                <td>{{$dateFormat->retriveDateInVal($value->effective_date)}}</td>
                                                                <td>
                                                                    @if($value->status == "active")
                                                                        <span class="badge badge-status badge-outline-success badge-pill">Active</span></td>
                                                                @elseif($value->status=="inactive")
                                                                    <span class="badge badge-status badge-outline-danger badge-pill">In-Active</span></td>
                                                                @endif
                                                                <td>
                                                                    {{--                                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-Share-holder-records-{{$value->id}}"--}}
                                                                    {{--                                                                       class="waves-effect waves-light">--}}
                                                                    {{--                                                                        <i class="ri f-1-4x ri-edit-box-line text-primary"></i>--}}
                                                                    {{--                                                                    </a>--}}
                                                                    <a href="javascript:void(0);" data-src="{{route('admin.getShareHolder',$value->id)}}" class="waves-effect waves-light editSharHolder"><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
                                                                    <a href="javascript:void(0);" data-src="{{route('admin.deleteShareHolder',$value->id)}}" class="waves-effect waves-light deleteShareholder"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                                </td>
                                                            </tr>

                                                        @endforeach
                                                        <tr>
                                                            <td colspan="5" class="text-right">Total</td>
                                                            <td class="text-right"><span id="formation_sharholder_percentage">@if(isset($totalPercentageOfShareholder)) {{$totalPercentageOfShareholder}}.00% @endif</span></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? "active show" : '' : '' }}  fade" id="v-pills-license" role="tabpanel" aria-labelledby="v-pills-settings-tab">

                                        <div class="card">
                                            <div class="card-body">

                                                <ul class="nav nav-pills navtab-bg nav-justified">
                                                    <li class="nav-item">
                                                        <a href="#home1" data-toggle="tab" aria-expanded="false"
                                                           class="nav-link {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? session()->has('tab_no1') ? session()->get('tab_no1')== 1 ? "active" : '' : 'active' : 'active' : 'active' }}">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                            <span class="d-none d-sm-inline-block">Business License</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#profile1" data-toggle="tab" aria-expanded="true" class="nav-link  {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? session()->has('tab_no1') ? session()->get('tab_no1')== 2 ? "active" : '' : '' : '' : '' }}">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                            <span class="d-none d-sm-inline-block">Professional License</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? session()->has('tab_no1') ? session()->get('tab_no1')== 1 ? "active show" : '' : '' : 'active show' : 'active show' }}" id="home1">
                                                        <form action="{{route('admin.storeAdminBusinessLicense')}}" method="POST">
                                                            @csrf
                                                            @if($isBusinessLicenseExist > 0)
                                                                <input type="hidden" name="businessLicense_id" value="{{$businessLicenseInfo->id}}">
                                                            @endif
                                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                                            <div class="alert alert-Fsc text-center" role="alert">
                                                                <h4>Business License</h4>
                                                            </div>
                                                            <hr/>

                                                            @if(isset($licenseData))

                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label text-right" for="jurisdiction">Jurisdiction :</label>
                                                                    <div class="col-md-3">
                                                                        <select name="jurisdiction" id="jurisdiction" class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="City" {{$isBusinessLicenseExist > 0 ? $businessLicenseInfo->jurisdiction == "City" ? 'Selected' :'' : ''}}>City</option>
                                                                            <option value="County" {{$isBusinessLicenseExist > 0 ? $businessLicenseInfo->jurisdiction == "County" ? 'Selected' :'' : ''}}>County</option>
                                                                        </select>
                                                                    </div>
                                                                    <label class="col-md-3 col-form-label show_license_city d-none text-right" for="license_city">City :</label>
                                                                    <div class="col-md-3 show_license_city d-none">
                                                                        <input type="text" class="form-control" id="license_city" name="license_city" value="{{$isBusinessLicenseExist > 0 ? $businessLicenseInfo->license_city : ''}}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row mb-3 d-none" id="license_county_div">
                                                                    <label class="col-md-3 col-form-label text-right" for="license_county">County :</label>
                                                                    <div class="col-md-3">
                                                                        <select id="select_license_county" class="form-control fsc-input" disabled>
                                                                            <option value="">Select County</option>
                                                                            @if(isset($taxCountyList))
                                                                                @foreach($taxCountyList as $key=>$taxCounty)
                                                                                    <option value="{{$taxCounty->county_code}}"
                                                                                        {{$companyInfo->county_code == $taxCounty->county_code ? "selected" : ''}}>{{$taxCounty->county_name}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                        <input type="hidden" id="license_county" name="license_county" value="{{$companyInfo->license_county}}"/>
                                                                    </div>
                                                                    <label class="col-md-3 col-form-label text-right" for="license_county_code">County Code :</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" readonly class="form-control" id="license_county_code" name="license_county_code" value="{{$companyInfo->county_code}}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label text-right" for="license_no">License :</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" readonly class="form-control" id="license_no" name="license_no" value="{{$licenseData->certificate_no}}">
                                                                    </div>
                                                                    <label class="col-md-3 col-form-label text-right" for="expire_date">Expire Date :</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" readonly class="form-control" id="expire_date" name="expire_date" value="{{$license_expire_date}}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label text-right" for="license_note">Note :</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="license_note" name="license_note" value="{{$isBusinessLicenseExist > 0 ? $businessLicenseInfo->license_note : ''}}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3">
                                                                    <label class="col-md-3 col-form-label text-right" for="renewal_date"></label>
                                                                    <div class="col-md-3">
                                                                        <a href="javascript:void(0);" data-src="{{route('admin.viewDocument',3)}}" id="viewBusinessLicense" class="btn btn-warning text-center col-md-12">View License</a>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <a href="{{route('admin.work-status')}}" class="btn btn-warning text-center col-md-12">Renew Record</a>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <a href="https://ecorp.sos.ga.gov/Account" target="_blank" class="btn btn-warning text-center col-md-12">Renew Here</a>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-3 text-center">
                                                                    <div class="col-md-12 text-center">
                                                                        @if(isset($isBusinessLicenseExist))
                                                                            @if($isBusinessLicenseExist > 0)
                                                                                <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                                                            @else
                                                                                <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                                            @endif
                                                                        @else
                                                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                                        @endif
                                                                        <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </form>

                                                    </div>
                                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 5 ? session()->has('tab_no1') ? session()->get('tab_no1')== 2 ? "active show" : '' : '' : '' : '' }} " id="profile1">
                                                        <form action="{{route('admin.storeProfessionalLicense')}}" method="POST" id="professionLicense">
                                                            @csrf
                                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                                            <input type="hidden" name="professional_id" id="professional_id" value="0">
                                                            <div class="alert alert-Fsc text-center" role="alert">
                                                                <h4>Professional License</h4>
                                                            </div>
                                                            <hr>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label text-right">Profession :</label>
                                                                <div class="col-md-3">
                                                                    <select type="text" class="form-control profession_option" id="profession" name="profession">
                                                                        <option value="">Select</option>
                                                                        @if(isset($professionList))
                                                                            @foreach($professionList as $key=>$profession)
                                                                                <option value="{{$profession->profession_name}}">{{$profession->profession_name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <a class="btn btn-primary" data-toggle="modal" data-target="#modal_Profession"><i class="fa fa-plus"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3" id="license_county_div">
                                                                <label class="col-md-3 col-form-label text-right" for="professional_license_state">Type Of License :</label>
                                                                <div class="col-md-3">
                                                                    <select type="text" class="form-control profession_option" id="type_of_license" name="type_of_license">
                                                                        <option value="">Select License</option>
                                                                        <option value="Profession License">Profession License</option>
                                                                        <option value="Firm License">Firm License</option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-3 col-form-label text-right" for="license_county_code">License Period :</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" class="form-control only-numbers" maxlength="2" id="professional_license_period" name="professional_license_period">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3" id="license_county_div">
                                                                <label class="col-md-3 col-form-label text-right" for="professional_license_state">State :</label>
                                                                <div class="col-md-3">
                                                                    <x-state data="professional_license_state" message=""/>
                                                                </div>
                                                                <label class="col-md-3 col-form-label text-right" for="license_county_code">Effective Date :</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" class="form-control datepickers" id="professional_effective_date" name="professional_effective_date">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label text-right" for="professional_license_no">License # :</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" class="form-control" id="professional_license_no" name="professional_license_no" placeholder="License">
                                                                </div>
                                                                <label class="col-md-3 col-form-label text-right" for="expire_date">Expire Date :</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" class="form-control datepickers" id="professional_expire_date" name="professional_expire_date" placeholder="Expire Date">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label text-right" for="license_note">Note :</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="professional_license_note" name="professional_license_note" placeholder="Note">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3 d-none" id="ifProfession">
                                                                <label class="col-md-3 col-form-label text-right" for="license_ce_status">CE :</label>
                                                                <div class="col-md-9 custom-checkbox">
                                                                    <div class="mt-1">
                                                                        <span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                        <input type="checkbox" name="is_ce_status" value="1" class="custom-control-input" id="Check1">
                                                                        <label class="custom-control-label" for="Check1">Check</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{--                                                            <div class="form-group row mb-3">--}}
                                                            {{--                                                                <label class="col-md-3 col-form-label text-right" for="renewal_date"></label>--}}
                                                            {{--                                                                <div class="col-md-3">--}}
                                                            {{--                                                                    <a class="btn btn-warning text-center col-md-12">License History</a>--}}
                                                            {{--                                                                </div>--}}
                                                            {{--                                                                <div class="col-md-3">--}}
                                                            {{--                                                                    <a class="btn btn-warning text-center col-md-12">File Not Upload</a>--}}
                                                            {{--                                                                </div>--}}
                                                            {{--                                                                <div class="col-md-3">--}}
                                                            {{--                                                                    <a class="btn btn-warning text-center col-md-12">Renew Here</a>--}}
                                                            {{--                                                                </div>--}}
                                                            {{--                                                            </div>--}}
                                                            <div class="form-group row mb-3 text-center">
                                                                <div class="col-md-12 text-center">
                                                                    <button type="submit" id="isProfessionalLicenseEdit" class="col-md-2 btn btn-primary text-center">Save</button>
                                                                    <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>

                                                                    <a href="{{route('admin.licenseHistroy')}}" class="btn btn-warning text-center col-md-3">License History</a>

                                                                </div>
                                                            </div>
                                                        </form>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                    <thead>
                                                                    <tr class="text-center">
                                                                        <th>Profession</th>
                                                                        <th>State</th>
                                                                        <th>License Type</th>
                                                                        <th>License Period</th>
                                                                        <th>License #</th>
                                                                        <th>Effective Date</th>
                                                                        <th>Expire Date</th>
                                                                        <th>CE Status</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    @if(isset($professionalLicenseList))
                                                                        @foreach($professionalLicenseList as $key=>$professionalLicense)
                                                                            <tr>
                                                                                <td>{{$professionalLicense->profession}}</td>
                                                                                <td class="text-center">{{$professionalLicense->professional_license_state}}</td>
                                                                                <td>{{$professionalLicense->type_of_license}}</td>
                                                                                <td class="text-center">{{$professionalLicense->professional_license_period}}</td>
                                                                                <td class="text-center">{{$professionalLicense->professional_license_no}}</td>
                                                                                <td class="text-center">{{$dateFormat->retriveDateInVal($professionalLicense->professional_effective_date)}}</td>
                                                                                <td>{{$dateFormat->retriveDateInVal($professionalLicense->professional_expire_date)}}</td>
                                                                                <td class="text-center">{{$professionalLicense->is_ce_status ? 'Yes' : 'No'}}</td>
                                                                                <td>
                                                                                    <a href="javascript:void(0)" onclick="editProfessionalLicense({{$professionalLicense->id}})"><i class="fas fa-edit"></i> </a>
                                                                                    <a href="javascript:void(0)" class="text-danger" onclick="deleteProfessionalLicense({{$professionalLicense->id}})"><i class="fas fa-trash-alt"></i> </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 6 ? "active show" : '' : '' }}  fade" id="v-pills-taxation" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <form action="{{route('admin.storeTaxation')}}" method="POST">
                                            @csrf
                                            @if($isTaxationListExist>0)
                                                <input type="hidden" name="taxation_id" value="{{$taxationList->id}}">
                                            @else
                                                <input type="hidden" name="taxation_id" value="0">
                                            @endif
                                            <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4>For Income-Tax Purpose - Select Type of Entity</h4>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="emailaddress">Type of Entity :</label>
                                                        <select class="form-control fsc-input" name="type_of_entity" id="type_of_entity" placeholder="Enter Your Company Name">
                                                            <option value=""> ---Select---</option>
                                                            @if(isset($typeOfEntityList))
                                                                @foreach($typeOfEntityList as $Entity)
                                                                    <option value="{{$Entity->id}}"
                                                                        {{$isTaxationListExist > 0 ? $taxationList->type_of_entity == $Entity->id ? "Selected":"":""}}>

                                                                        {{$Entity->type_of_entity}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="emailaddress">Type of Form :</label>
                                                        <input type="text"
                                                               value="{{$isTaxationListExist > 0 ? $taxationList->typeOfForm :""}}" name="typeOfForm" class="form-control" id="typeOfForm" placeholder="Enter Form Name" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="emailaddress">Effective Date :</label>
                                                        <input name="typeofcorp_effect" value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateInVal($taxationList->typeofcorp_effect) :""}}"
                                                               type="text" placeholder="" id="typeofcorp_effect" class="form-control datepickers">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4 class="col-sm-3" style="float:left;">Federal / State</h4><h4 class="col-sm-9" style="text-align:center;">Income Tax</h4>
                                            </div>
                                            <div class="row ">
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Federal ID :</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->income_tax_federal_id :""}}"
                                                               class="form-control txtOnly federal_id" id="income_tax_federal_id" maxlength="9" name="income_tax_federal_id">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Form To File (Federal):</label>
                                                        <input type="text"
                                                               value="{{$isTaxationListExist > 0 ? $taxationList->income_tax_federal_file :""}}"
                                                               class="form-control" id="income_tax_federal_file" name="income_tax_federal_file" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Due Date :</label>
                                                        <input type="text"
                                                               value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->income_tax_federal_due_date) :""}}"
                                                               class="form-control" id="income_tax_federal_due_date" name="income_tax_federal_due_date" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Extension Due Date :</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->income_tax_federal_extension_due_date) :""}}"
                                                               class="form-control" id="income_tax_federal_extension_due_date" name="income_tax_federal_extension_due_date" readonly="">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">State :</label>
                                                        <input type="text" value="{{$companyInfo->pa_state}}"
                                                               readonly="" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">State ID :</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->income_tax_state_id :""}}"
                                                               class="form-control" id="income_tax_state_id" name="income_tax_state_id">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Form To File (State):</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->income_tax_state_file :""}}"
                                                               class="form-control" id="income_tax_state_file" name="income_tax_state_file" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Due Date :</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->income_tax_state_due_date) :""}}"
                                                               class="form-control" id="income_tax_state_due_date" name="income_tax_state_due_date" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group ">
                                                        <label for="emailaddress">Extension Due Date :</label>
                                                        <input type="text" value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->income_tax_extension_state_due_date) :""}}"
                                                               class="form-control" id="income_tax_extension_state_due_date" name="income_tax_extension_state_due_date" readonly="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-Fsc text-center form-inline" role="alert">
                                                <div class="col-sm-4">
                                                    <h4 style="float:left;">Federal / State</h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h4 style="text-align:center;">Payroll Tax</h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <b style="float:right;">Do You have Payroll ? :<select class="bg-green first" name="payroll_required" id="payroll_que">
                                                            <!--<option class="default_val"  selected="">Select</option>-->
                                                            <option class="bg-green first" value="1">Yes</option>
                                                            <option class="bg-red second" value="0">No</option>
                                                        </select></b>
                                                </div>

                                            </div>
                                            <div class="Payroll_section">
                                                <ul class="nav nav-pills navtab-bg nav-justified">
                                                    <li class="nav-item">
                                                        <a href="#FICA" data-toggle="tab" aria-expanded="false" class="nav-link active" style="padding:5px;">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                            <span class="d-none d-sm-inline-block">Federal Withholding & FICA Tax</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#FUTA" data-toggle="tab" aria-expanded="true" class="nav-link" style="padding:5px;">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                            <span class="d-none d-sm-inline-block">Federal Unemployment Tax (FUTA)</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#Withholding" data-toggle="tab" aria-expanded="true" class="nav-link" style="padding:5px;">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                            <span class="d-none d-sm-inline-block">State Withholding</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#SUTA" data-toggle="tab" aria-expanded="true" class="nav-link" style="padding:5px;">
                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                            <span class="d-none d-sm-inline-block">State Unemployment (SUTA)</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane show active" id="FICA">
                                                        <h4 class="header-title text-center mb-4">Federal Withholding & FICA Tax</h4>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Name :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_name :""}}"
                                                                           class="form-control" id="fica_name" name="fica_name" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Department Authority :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_dept_authority :""}}"
                                                                           class="form-control" id="fica_dept_authority" name="fica_dept_authority" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Form No. :</label>
                                                                    <select type="text" class="form-control" id="fica_form_number" name="fica_form_number">
                                                                        <option value="">Select</option>
                                                                        <option value="Form-941" {{$isTaxationListExist>0?$taxationList->fica_form_number == "Form-941"?"selected":"":""}}>Form-941</option>
                                                                        <option value="Form-944" {{$isTaxationListExist>0?$taxationList->fica_form_number == "Form-944"?"selected":"":""}}>Form-944</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Quarter Period :</label>
                                                                    <select type="text" class="form-control" id="fica_quarter_period" name="fica_quarter_period">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" {{$isTaxationListExist>0?$taxationList->fica_quarter_period == "1st Qtr"?"selected":"":""}}>1st Qtr.</option>
                                                                        <option value="2nd Qtr" {{$isTaxationListExist>0?$taxationList->fica_quarter_period == "2nd Qtr"?"selected":"":""}}>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" {{$isTaxationListExist>0?$taxationList->fica_quarter_period == "3rd Qtr"?"selected":"":""}}>3rd Qtr.</option>
                                                                        <option value="4th Qtr" {{$isTaxationListExist>0?$taxationList->fica_quarter_period == "4th Qtr"?"selected":"":""}}>4th Qtr.</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Period End Date :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->fica_quarter_period_date) :""}}"
                                                                           class="form-control" id="fica_quarter_period_date" name="fica_quarter_period_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Filing Frequency :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_quarter_period :""}}"
                                                                           class="form-control" id="fica_quarter_period" name="fica_quarter_period_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Due Date :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->fica_due_date) :""}}"
                                                                           class="form-control-insu form-control" id="fica_due_date" name="fica_due_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">EFTPS PIN :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_eptpspin :""}}"
                                                                           class="form-control" id="fica_eptpspin" name="fica_eptpspin">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">PW :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_pay_pw :""}}"
                                                                           class="form-control" id="fica_pay_pw" name="fica_pay_pw">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Frequency :</label>
                                                                    <select type="text" class="form-control" id="fica_federal_payment_frequency" name="fica_federal_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Daily" {{$isTaxationListExist>0?$taxationList->fica_federal_payment_frequency="Daily"?"selected":"":"" }}>Daily</option>
                                                                        <option value="Weekly" {{$isTaxationListExist>0?$taxationList->fica_federal_payment_frequency="Weekly"?"selected":"":"" }}>Weekly</option>
                                                                        <option value="Monthly" {{$isTaxationListExist>0?$taxationList->fica_federal_payment_frequency="Monthly"?"selected":"":"" }}>Monthly</option>
                                                                        <option value="Quarterly" {{$isTaxationListExist>0?$taxationList->fica_federal_payment_frequency="Quarterly"?"selected":"":"" }}>Quarterly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Due Date :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->retriveDateSpefic($taxationList->fica_payment_frequency_date) :""}}"
                                                                           class="form-control-insu form-control" name="fica_payment_frequency_date" id="fica_payment_frequency_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Federal Note :</label>
                                                                    <input type="text"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->fica_note :""}}"
                                                                           class="form-control" name="fica_note">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="tab-pane show" id="FUTA">
                                                        <h4 class="header-title text-center mb-4">Federal Unemployment Tax (FUTA)</h4>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Name :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->futa_name :""}}"
                                                                           class="form-control" id="futa_name" name="futa_name" value="IRS" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Department Authority :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->futa_dept_authority :""}}"
                                                                           class="form-control" id="futa_dept_authority" name="futa_dept_authority" value="Internal Revenue Service" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Filing Frequency :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->futa_frequency :""}}"
                                                                           class="form-control" id="futa_frequency" name="futa_frequency" readonly="">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Form No. :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->futa_form_no :""}}"
                                                                           class="form-control" id="futa_form_no" readonly="" name="futa_form_no">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Due Date :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ?  $dateFormat->retriveDateSpefic($taxationList->futa_frequency_due_date) :""}}"
                                                                           class="form-control-insu form-control" id="futa_frequency_due_date" name="futa_frequency_due_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Frequency :</label>
                                                                    <select type="text" class="form-control" id="futa_payment_frequency" name="futa_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Daily" {{$isTaxationListExist>0?$taxationList->futa_payment_frequency="Daily"?"":"":""}}>Monthly</option>
                                                                        <option value="Weekly" {{$isTaxationListExist>0?$taxationList->futa_payment_frequency="Weekly"?"":"":""}}>Monthly</option>
                                                                        <option value="Monthly" {{$isTaxationListExist>0?$taxationList->futa_payment_frequency="Monthly"?"":"":""}}>Monthly</option>
                                                                        <option value="Quarterly" {{$isTaxationListExist>0?$taxationList->futa_payment_frequency="Quarterly"?"":"":""}}>Quarterly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Due Date :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ?  $dateFormat->retriveDateSpefic($taxationList->futa_payment_frequency_date) :""}}"
                                                                           class="form-control-insu form-control" name="futa_payment_frequency_date" id="futa_payment_frequency_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Note :</label>
                                                                    <input type="text" value="{{$isTaxationListExist > 0 ? $taxationList->futa_note :""}}"
                                                                           class="form-control" id="futa_note" name="futa_note">
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="tab-pane show" id="Withholding">
                                                        <h4 class="header-title text-center mb-4">State Withholding</h4>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Name :</label>
                                                                    <input type="text" class="form-control" value="GA" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Department Authority :</label>
                                                                    <input type="text" class="form-control" value="GA Dept of Revenue" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Form No. :</label>
                                                                    <input type="text" class="form-control" value="Form-G-7" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Quarter Period :</label>
                                                                    <select type="text" class="form-control" id="state_holding_quarter_type" name="state_holding_quarter_type">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" {{$isTaxationListExist > 0 ? $taxationList->state_holding_quarter_type=="1st Qtr" ? "Selected" :"":""}}>1st Qtr.</option>
                                                                        <option value="2nd Qtr" {{$isTaxationListExist > 0 ? $taxationList->state_holding_quarter_type=="2nd Qtr" ? "Selected" :"":""}}>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" {{$isTaxationListExist > 0 ? $taxationList->state_holding_quarter_type=="3rd Qtr" ? "Selected" :"":""}}>3rd Qtr.</option>
                                                                        <option value="4th Qtr" {{$isTaxationListExist > 0 ? $taxationList->state_holding_quarter_type=="4th Qtr" ? "Selected" :"":""}}>4th Qtr.</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Period End Date :</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->futureDate($taxationList->state_holding_quarter_date) :""}}"
                                                                           id="state_holding_quarter_date" name="state_holding_quarter_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Filing Frequency :</label>
                                                                    <input type="text" class="form-control" value="Quarterly" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->futureDate($taxationList->state_holding_due_date) :""}}"
                                                                           id="state_holding_due_date" name="state_holding_due_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">State Withholding No :</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->state_holding_no :""}}"
                                                                           id="state_holding_no" name="state_holding_no">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">PIN :</label>
                                                                    <input type="text" value="{{$isTaxationListExist>0?$taxationList->state_holding_eptpspin:""}}" class="form-control" name="state_holding_eptpspin">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Username :</label>
                                                                    <input type="text" value="{{$isTaxationListExist>0?$taxationList->state_holding_Username:""}}" class="form-control" name="state_holding_Username">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Password :</label>
                                                                    <input type="text" value="{{$isTaxationListExist>0?$taxationList->state_holding_pay_pw:""}}" class="form-control" name="state_holding_pay_pw">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Frequency :</label>
                                                                    <select type="text" class="form-control" name="state_holding_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Daily" {{$isTaxationListExist>0?$taxationList->state_holding_payment_frequency="Daily"?"":"":""}}>Daily</option>
                                                                        <option value="Weekly" {{$isTaxationListExist>0?$taxationList->state_holding_payment_frequency="Weekly"?"":"":""}}>Weekly</option>
                                                                        <option value="Monthly" {{$isTaxationListExist>0?$taxationList->state_holding_payment_frequency="Monthly"?"":"":""}}>Monthly</option>
                                                                        <option value="Quarterly" {{$isTaxationListExist>0?$taxationList->state_holding_payment_frequency="Quarterly"?"":"":""}}>Quarterly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Due Date :</label>
                                                                    <input type="text" value="{{$isTaxationListExist>0?$dateFormat->futureDate($taxationList->state_holding_payment_due_date):""}}"
                                                                           class="form-control-insu form-control" name="state_holding_payment_due_date" readonly="">
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="tab-pane show" id="SUTA">
                                                        <h4 class="header-title text-center mb-4">State Unemployment (SUTA)</h4>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Name :</label>
                                                                    <input type="text" class="form-control" value="GA" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Department Authority :</label>
                                                                    <input type="text" class="form-control" value="GA Dept of Labor" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Form No. :</label>
                                                                    <input type="text" class="form-control" value="Form-DOL-4N" readonly="">
                                                                    {{--                                                                    <select type="text" class="form-control" id="form_number_1" name="form_number_1">--}}
                                                                    {{--                                                                        <option value="">Select</option>--}}
                                                                    {{--                                                                        <option value="Form-941" selected="">Form-941</option>--}}
                                                                    {{--                                                                        <option value="Form-944">Form-944</option>--}}
                                                                    {{--                                                                    </select>--}}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Quarter Period :</label>
                                                                    <select type="text" class="form-control" name="suta_quarter_period">
                                                                        <option value="">Select</option>
                                                                        <option value="1st Qtr" {{$isTaxationListExist > 0 ? $taxationList->suta_quarter_period=="1st Qtr" ? "Selected" :"":""}}>1st Qtr.</option>
                                                                        <option value="2nd Qtr" {{$isTaxationListExist > 0 ? $taxationList->suta_quarter_period=="2nd Qtr" ? "Selected" :"":""}}>2nd Qtr.</option>
                                                                        <option value="3rd Qtr" {{$isTaxationListExist > 0 ? $taxationList->suta_quarter_period=="3rd Qtr" ? "Selected" :"":""}}>3rd Qtr.</option>
                                                                        <option value="4th Qtr" {{$isTaxationListExist > 0 ? $taxationList->suta_quarter_period=="4th Qtr" ? "Selected" :"":""}}>4th Qtr.</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Period End Date :</label>
                                                                    <input type="text" class="form-control" value="{{$isTaxationListExist > 0 ? $taxationList->suta_peroid_end_date:""}}" name="suta_peroid_end_date" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Filling Frequency :</label>
                                                                    <input type="text" class="form-control" value="Quarterly" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control"
                                                                           id="suta_peroid_due_date" name="suta_peroid_due_date"
                                                                           value="{{$isTaxationListExist > 0 ? $dateFormat->futureDate($taxationList->suta_peroid_due_date) :""}}" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">State Unemployment No :</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->suta_unemployment_no :""}}"
                                                                           id="suta_unemployment_no" name="suta_unemployment_no">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Username :</label>
                                                                    <input type="text" class="form-control" id="suta_username" name="suta_username"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->suta_username :""}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Password :</label>
                                                                    <input type="text" class="form-control" id="suta_pay_pw" name="suta_pay_pw"
                                                                           value="{{$isTaxationListExist > 0 ? $taxationList->suta_pay_pw :""}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Frequency :</label>
                                                                    <select type="text" class="form-control" id="suta_payment_frequency" name="suta_payment_frequency">
                                                                        <option value="">-Select-</option>
                                                                        <option value="Daily" {{$isTaxationListExist>0?$taxationList->suta_payment_frequency="Daily"?"":"":""}}>Daily</option>
                                                                        <option value="Weekly" {{$isTaxationListExist>0?$taxationList->suta_payment_frequency="Weekly"?"":"":""}}>Weekly</option>
                                                                        <option value="Monthly" {{$isTaxationListExist>0?$taxationList->suta_payment_frequency="Monthly"?"":"":""}}>Monthly</option>
                                                                        <option value="Quarterly" {{$isTaxationListExist>0?$taxationList->suta_payment_frequency="Quarterly"?"":"":""}}>Quarterly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group ">
                                                                    <label for="emailaddress">Payment Due Date :</label>
                                                                    <input type="text" class="form-control-insu form-control"
                                                                           value="{{$isTaxationListExist>0?$dateFormat->futureDate($taxationList->suta_payment_frequency_date):""}}"
                                                                           name="suta_payment_frequency_date" id="suta_payment_frequency_date"
                                                                           readonly="">
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3 text-center">
                                                <label class="col-md-3 col-form-label text-right" for="county"></label>
                                                <div class="col-md-12 text-center">
                                                    @if(isset($taxtionList))
                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                                    @else
                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Save</button>
                                                    @endif
                                                    <button type="reset" class=" col-md-2 btn btn-danger text-center">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 7 ? "active show" : '' : '' }}  fade" id="v-pills-banking" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                            <li class="nav-item">
                                                <a href="#bank" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                    <span class="d-none d-sm-inline-block">Bank Information</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#credit_card" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                    <span class="d-none d-sm-inline-block">Credit Card</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane show active" id="bank">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-Bank-records">
                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Bank A/C Record
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Bank Name</th>
                                                                        <th>A/C Nick Name</th>
                                                                        <th>Last 4 Digit A/C No</th>
                                                                        <th>Check Status</th>
                                                                        <th>OP Date</th>
                                                                        <th>Statement</th>
                                                                        <th>Staus</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @if(isset($adminBankList))
                                                                        @foreach($adminBankList as $key=>$banks)
                                                                            <tr>
                                                                                <td>{{$banks->bank_name}}</td>
                                                                                <td>{{$banks->nick_name}}</td>
                                                                                <td>{{$banks->last_four_digit}}</td>
                                                                                <td>{{$banks->check_stubs}}</td>
                                                                                <td>{{$dateFormat->retriveDateInVal($banks->op_date)}}</td>
                                                                                <td>{{$banks->statement}}</td>
                                                                                <td>
                                                                                    <x-status type="{{$banks->ac_status}}"/>
                                                                                </td>
                                                                                <td>

                                                                                    <x-actionlink
                                                                                        id="modal-Bank-records-{{$banks->id}}"
                                                                                        class="waves-effect waves-light"
                                                                                        buttons="edit"
                                                                                        route="javascript:void(0);"
                                                                                    />
                                                                                    <x-actionlink
                                                                                        id=""
                                                                                        class=""
                                                                                        buttons="delete"
                                                                                        route=""
                                                                                    />
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Add Bank Account  -->
                                                                            <!-- start -->
                                                                            <div class="modal fade" id="modal-Bank-records-{{$banks->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                <div class="modal-dialog modal-lg">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <div class="text-center col-md-12">
                                                                                                <h4 class="modal-title" id="myLargeModalLabel">Bank Account Record</h4>
                                                                                            </div>
                                                                                            {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <div class="card">
                                                                                                <div class="card-body">
                                                                                                    <form class="control-form" method="post" action="{{route('admin.storeBankAccount')}}">
                                                                                                        @csrf
                                                                                                        <input type="hidden" name="bank_ac_id" value="{{$banks->id}}">
                                                                                                        <div class="form-group row mb-3">
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Bank Name :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <select name="bank_id" id="bank_id_{{$banks->id}}" class="form-control text-center bank_id">
                                                                                                                    <option value="">Select Bank</option>
                                                                                                                    @if(isset($bankList))
                                                                                                                        @foreach($bankList as $key=>$bank)
                                                                                                                            <option value="{{$bank->id}}" @if($banks->id == $bank->id) selected @endif>{{$bank->bank_name}}</option>
                                                                                                                        @endforeach
                                                                                                                    @endif
                                                                                                                </select>
                                                                                                                <input type="hidden" id="banks_name_{{$banks->id}}" name="bank_name" value="{{$banks->bank_name}}">
                                                                                                            </div>
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Account Nick Name :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <input type="text" class="form-control" id="nick_name" name="nick_name" value="{{$banks->nick_name}}">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group row mb-3">
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Last 4 digit of A/C # :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <input type="text" class="form-control account-no" id="last_four_digit" name="last_four_digit" value="{{$banks->last_four_digit}}">
                                                                                                            </div>
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Check Stubs :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <select name="check_stubs" id="check_stubs" class="form-control">
                                                                                                                    <option value="">Select</option>
                                                                                                                    <option value="St. With Check Image" @if($banks->check_stubs=="St. With Check Image") selected @endif>St. With Check Image</option>
                                                                                                                    <option value="Counter Check" @if($banks->check_stubs=="Counter Check") selected @endif>Counter Check</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group row mb-3">
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Open Date :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                @php $dateformate = new \App\Utility\DateUtility(); @endphp
                                                                                                                <input type="text" class="form-control datepickers" id="op_date" name="op_date" value="{{$dateformate->retriveDateInVal($banks->op_date)}}">
                                                                                                            </div>
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">Statement :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <select name="statement" id="statement" class="form-control">
                                                                                                                    <option value="">select</option>
                                                                                                                    <option value="E-statement" @if($banks->statement=="E-statement") selected @endif>E-statement</option>
                                                                                                                    <option value="Paper-Statement" @if($banks->statement=="Paper-Statement") selected @endif>Paper-Statement</option>
                                                                                                                    <option value="FSC Get Online" @if($banks->statement=="FSC Get Online") selected @endif>FSC Get Online</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group row mb-3">
                                                                                                            <label class="col-md-3 col-form-label text-right" for="fax_no">status :</label>
                                                                                                            <div class="col-md-3">
                                                                                                                <select name="ac_status" id="ac_status" class="form-control">
                                                                                                                    <option value="active" @if($banks->ac_status=="active") selected @endif>Active</option>
                                                                                                                    <option value="close" @if($banks->ac_status=="close") selected @endif>Close</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="form-group row mb-3 text-center">
                                                                                                            <div class="col-md-6">
                                                                                                                <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                                                                            </div>
                                                                                                            <div class="col-md-6">
                                                                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div><!-- /.modal-content -->
                                                                                </div><!-- /.modal-dialog -->
                                                                            </div><!-- /.modal -->
                                                                            <!-- End -->
                                                                        @endforeach
                                                                    @endif
                                                                    </tbody>
                                                                </table>

                                                            </div> <!-- end card body-->
                                                        </div> <!-- end card -->
                                                    </div><!-- end col-->
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="credit_card">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                                        <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-credit-card-records">
                                                                            <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Credit Card Record
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <table id="corporation-renewal-table" class="table table-bordered table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Type of CC</th>
                                                                        <th>CC Issuer name</th>
                                                                        <th>Last 4 Digit A/C No</th>
                                                                        <th>Check Status</th>
                                                                        <th>OP Date</th>
                                                                        <th>Statement</th>
                                                                        <th>Staus</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Tiger Nixon</td>
                                                                        <td>System Architect</td>
                                                                        <td>Edinburgh</td>
                                                                        <td>61</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                        <td>2011/04/25</td>
                                                                        <td>$320,800</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div> <!-- end card body-->
                                                        </div> <!-- end card -->
                                                    </div><!-- end col-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{session()->has('tab_no') ? session()->get('tab_no')== 8 ? "active show" : '' : '' }}  fade" id="v-pills-other" role="tabpanel" aria-labelledby="v-pills-company-tab">
                                        <form action="{{route('admin.storeProfile')}}" method="post" name="storeProfile" id="storeProfile">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="admin_id">
                                            <input type="hidden" @if(isset($isProfileExsist)) @if($isProfileExsist > 0) name=profile_id @endif @endif value="1">
                                            <div class="alert alert-Fsc text-center" role="alert">
                                                <h4>Tech. Company Info</h4>
                                            </div>
                                            <hr/>
                                            <div class="form-group row mb-3">
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="col-md-12 text-right">
                                                            <label class="col-form-label text-right" for="company_name_lbl">Company Name : <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 text-right">
                                                            <cite title="Source Title">(Legal Name)<span>&nbsp;&nbsp;&nbsp;</span></cite>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="VARR Tech LLC" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="col-md-12 text-right">
                                                            <label class="col-form-label text-right" for="company_name_lbl">Business Name :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 text-right">
                                                            <cite title="Source Title">(DBA)<span>&nbsp;&nbsp;&nbsp;</span></cite>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-9">
                                                    <input type="text" value="Vimbo" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_1">Reg Address1 : <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="4550 Jimmy Carter Blvd." readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address_2">Reg Address2 :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">City / state / zip : <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="Norcross" readonly>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="GA" readonly>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="30093" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-form-label col-md-3 text-right">Telephone # : <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" value="(770) 869-2531" class="form-control " readonly/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" value="Office" class="form-control " readonly/>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" value="" placeholder="Ext" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="fax_no">Fax # :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="(770) 414-5351" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="website">Website :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="www.vimbo.com" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email :<span class="text-danger">&nbsp;&nbsp;&nbsp;</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="info@vimbo.com" readonly>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div> <!-- end col-->

                        </div> <!-- end row-->

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- Add Bank Account  -->
    <!-- start -->
    <div class="modal fade" id="modal-Bank-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Bank Account Record</h4>
                    </div>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" action="{{route('admin.storeBankAccount')}}">
                                @csrf
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Bank Name :</label>
                                    <div class="col-md-3">
                                        <select name="bank_id" id="banks_id" class="form-control text-center bank_id">
                                            <option value="">Select Bank</option>
                                            @if(isset($bankList))
                                                @foreach($bankList as $key=>$bank)
                                                    <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <input type="hidden" id="banks_name" name="bank_name">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Account Nick Name :</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="nick_name" name="nick_name">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Last 4 digit of A/C # :</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control account-no" id="last_four_digit" name="last_four_digit">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Check Stubs :</label>
                                    <div class="col-md-3">
                                        <select name="check_stubs" id="check_stubs" class="form-control">
                                            <option value="">Select</option>
                                            <option value="St. With Check Image">St. With Check Image</option>
                                            <option value="Counter Check">Counter Check</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Open Date :</label>
                                    <div class="col-md-3">
                                        <input type="text" value="dd/mm/yyyy" class="form-control datepickers" id="op_date" name="op_date">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Statement :</label>
                                    <div class="col-md-3">
                                        <select name="statement" id="statement" class="form-control">
                                            <option value="">select</option>
                                            <option value="E-statement">E-statement</option>
                                            <option value="Paper-Statement">Paper-Statement</option>
                                            <option value="FSC Get Online">FSC Get Online</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">status :</label>
                                    <div class="col-md-3">
                                        <select name="ac_status" id="ac_status" class="form-control">
                                            <option value="active">Active</option>
                                            <option value="close">Close</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Share Holders  -->
    <!-- start -->
    <div class="modal fade" id="modal-Share-holder-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Share Holder</h4>
                    </div>
                </div>
                <div class="modal-body2">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" action="{{route('admin.storeShareHolder')}}" method="post" name="storeShareholder" id="storeShareholder0">
                                @csrf
                                <input type="hidden" name="formation_id" value="1">
                                <input type="hidden" id="shareholder_id" name="shareholder_id" value="0">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Name :</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="shareHolder_fname" name="fname">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="shareHolder_mname" name="mname">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="shareHolder_lname" name="lname">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="position">Position :</label>
                                    <div class="col-md-3">
                                        <select name="position" id="shareHolder_position" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($positionsList as $position)
                                                @if(in_array($position->position_name,$submittedPosition))

                                                @else
                                                    <option value="{{$position->position_name}}">{{$position->position_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="percentage">Percentage :</label>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="Value In Percentage" class="form-control" id="shareHolder_percentage" name="percentage0">
                                        <input type="hidden" id="shareHolder_percentage0" name="percentage">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date :</label>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="dd/mm/yyyy" class="form-control datepickers" id="shareHolder_effective_date" name="effective_date">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="SH_status">Status :</label>
                                    <div class="col-md-3">
                                        <select name="status" id="shareHolder_SH_status" class="form-control">
                                            <option value="active">Active</option>
                                            <option value="inactive">In-Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" id="shareHolderButton" class="addShareHolderEvent col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

    <!-- Add Credit card Account  -->
    <!-- start -->
    <div class="modal fade" id="modal-credit-card-records" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Credit Card Account Record</h4>
                    </div>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Type of CC :</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">CC Issuer name :</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Last 4 digit of A/C # :</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Statement :</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="Office">Office</option>
                                            <option value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Open Date :</label>
                                    <div class="col-md-3">
                                        <input type="text" value="dd/mm/yyyy" class="form-control" id="agent_names" name="agent_mname">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">status :</label>
                                    <div class="col-md-3">
                                        <select name="state_of_formation" id="state_of_formation" class="form-control">
                                            <option value="active">Active</option>
                                            <option value="close">Close</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="fax_no">Note :</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="fax_no" name="fax_no">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->


    <div id="documentLoaders" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-fsc-title col-md-6">Modal Header</h4>
                    <h4 class="modal-sec-title text-right col-md-5">Modal Header</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body-documents">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade show" id="modal_Profession" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Profession</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" id="addProfessionForm">
                                @csrf
                                <div class="form-group row mb-3">
                                    <div class="input-group">
                                        <input type="text" id="profession_name" name="profession_name" class="form-control" placeholder="Profession Name">
                                        <div class="input-group-append">
                                            <button id="addNewProfession" data-src="{{route('admin.storeProfession')}}" class="btn btn-primary waves-effect waves-light" type="button">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="form-group row mb-3">
                                <table id="profession_name_table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="new-application-table_info">
                                    <thead>
                                    <tr role="row">
                                        <th>No</th>
                                        <th>ProfessionName</th>
                                        <th>Action</th>
                                    </tr>

                                    </thead>

                                    <tbody>
                                    @if(isset($professionList))
                                        @foreach($professionList as $key=>$profession)
                                            <tr class="odd">
                                                <td class="sorting_1">{{$key+1}}</td>
                                                <td>{{$profession->profession_name}}</td>
                                                <td style="text-align:center;">
                                                    <a id="{{$profession->id}}" onclick="delProfession({{$profession->id}});" class="deleteProfession1"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group row mb-3" style="float:right;">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-danger text-center">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>

        $('#payroll_que').change(function () {
            if ($(this).val() == '1') {
                $('.Payroll_section').show();
            } else {
                $('.Payroll_section').hide();
            }
        });
        $('#income_tax_federal_id').mask('99-9999999')

        const mainLibs = {
            ajaxCalling: function (url, data, method, callback) {
                $.ajax({
                    url: url,
                    method: method,
                    data: data,
                    success: callback,
                    error: function (error) {
                        //returnval = error;
                    }
                });
            }
        };

        $('#type_of_entity').change(function () {

            const value = $(this).val();
            console.log(value);
            if (value.length != 0) {
                const url = '/ajax/getFormsByEntity/' + value;
                var data = {};
                mainLibs.ajaxCalling(url, data, "get", function (response) {
                    const jsonData = JSON.parse(response);
                    const todayDate = new Date();
                    const year = todayDate.getFullYear();
                    $('#income_tax_federal_file').val(jsonData['form_name']);
                    $('#typeOfForm').val(jsonData['form_name']);
                    $('#income_tax_federal_due_date').val("Mar-15-" + year);
                    $('#income_tax_federal_extension_due_date').val("Sep-15-" + year);
                    $('#income_tax_state_file').val(jsonData['state_form_name']);
                    $('#income_tax_state_due_date').val("Mar-15-" + year);
                    $('#income_tax_extension_state_due_date').val("Sep-15-" + year);
                });
            }

        });

        $('#select_county').select2();
        $('#select_county').change(function () {
            const countyTextVal = $(this).find(':selected').text();
            const countyVal = $(this).find(':selected').val();
            $('#county').val(countyTextVal);
            $('#county_code').val(countyVal);
        });


        $('#select_license_county').select2();
        $('#select_license_county').change(function () {
            const countyTextVal = $(this).find(':selected').text();
            const countyVal = $(this).find(':selected').val();
            $('#license_county').val(countyTextVal);
            $('#license_county_code').val(countyVal);
        });

        $('#alert_shareholder').click(function () {
            Swal.fire('Shareholder\'s percentage should 100%');
        });

        const sharholder_percentage = $('#formation_sharholder_percentage').html();
        var split_str = sharholder_percentage.split('.');
        var percentage = split_str[0];

        $('#shareHolder_percentage').blur(function () {
            const val = parseInt($(this).val());
            if (!isNaN(val)) {
                const percent = parseInt(percentage) + val;
                if (percent > 100) {
                    $('#CheckShareHolderPercentage').show();
                    $('#shareHolderButton').hide();
                } else {
                    $('#CheckShareHolderPercentage').hide();
                    $('#shareHolderButton').show();
                }
                $(this).val(val + ".00%");
            } else {
                $(this).val("0.00%");
                $('#shareHolder_percentage0').val(0);
            }
        });
        $('#shareHolder_percentage').focus(function () {
            const val = parseInt($(this).val());
            if (!isNaN(val)) {
                $(this).val(val);
            }
        });
        $('#shareHolder_percentage').keyup(function () {
            const val = parseInt($(this).val());
            if (!isNaN(val)) {
                $('#shareHolder_percentage0').val(val);
            } else {
                $('#shareHolder_percentage0').val(0);
            }
            console.log(val);
        });

        $("#storeShareholder0").validate({
            rules: {
                position: {required: true},
                fname: {required: true},
                lname: {required: true},
                effective_date: {required: true}
            },
            messages: {
                position: {required: "Select Position must required."},
                fname: {required: "Enter First name required."},
                lname: {required: "Enter Last Name required."},
                effective_date: {required: "Please Select Effective Date required."}
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');
                $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
                $(e).remove();
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        @if(isset($shareHolders))
            @if(count($shareHolders) > 0)
        if (percentage == 100) {
            $('#CheckShareHolderPercentage').hide();
        } else {
            $('.nav-link').removeAttr('data-toggle');
            $('#v-pills-formation').addClass('active show');
            $('#v-pills-formation-tab').addClass('active');
            $('#v-pills-company').removeClass('active show');
            $('#v-pills-company-tab').removeClass('active');
            $('#CheckShareHolderPercentage').show();

        }
        @endif
        @endif


    </script>
@endsection
