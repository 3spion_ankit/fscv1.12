<div class="card">
    <div class="card-body">
        <form class="control-form" method="post" name="storeShareholder" id="storeShareholder">
            @csrf
            <input type="hidden" name="formation_id" value="1">
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="name" name="fname">
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" id="name" name="mname">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="name" name="lname">
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label text-right" for="position">Position</label>
                <div class="col-md-3">
                    <select name="position" id="position" class="form-control">
                        <option value="">Select</option>
                        <option value="Agent">Agent</option>
                        <option value="CEO">CEO</option>
                        <option value="CFO">CFO</option>
                        <option value="Secretary">Secretary</option>
                        <option value="Sec">CEO / CFO / Sec.</option>
                    </select>
                </div>
                <label class="col-md-3 col-form-label text-right" for="percentage">Percentage</label>
                <div class="col-md-3">
                    <input type="text" placeholder="Value In Percentage" class="form-control" id="percentage" name="percentage">
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label text-right" for="effective_date">Effective Date</label>
                <div class="col-md-3">
                    <input type="text" placeholder="dd/mm/yyyy" class="form-control datepickers" id="effective_date" name="effective_date">
                </div>
                <label class="col-md-3 col-form-label text-right" for="SH_status">Status</label>
                <div class="col-md-3">
                    <select name="status" id="SH_status" class="form-control">
                        <option value="">select</option>
                        <option value="active">Active</option>
                        <option value="inactive">In-Active</option>
                    </select>
                </div>
            </div>
            <div class="form-group row mb-3 text-center">
                <div class="col-md-6">
                    <a href="javascript:void(0);" data-src="{{route('admin.storeShareHolder')}}" class="addShareHolderEvent col-md-5 btn btn-primary text-center">Submit</a>
                </div>
                <div class="col-md-6">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>
