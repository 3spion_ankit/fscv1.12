<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>FSC Administrator</title>
    <meta content="A Fully Responsive Financial Service Center where User Can Manage Accounts." name="description" />
    <meta content="Vimbo" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('Backend/images/favicon.png')}}">

    <!-- App css -->
    <link href="{{asset('Backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{asset('Backend/css/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <!-- icons -->
    <link href="{{asset('Backend/css/icons.min.css')}}" rel="stylesheet" type="text/css" />

</head>

<body class="loading">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card">

                    <div class="card-body p-4">
{{--                        @php echo \Illuminate\Support\Facades\Hash::make("password") @endphp--}}
                        <div class="text-center w-75 m-auto">
                            <div class="auth-logo">
                                <a href="index.html" class="logo logo-dark text-center">
                                    <span class="logo-lg">
                                        <img src="{{asset('Backend/images/fsc_logo.png')}}" alt="" height="75">
                                    </span>
                                </a>

                            </div>
                            <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger text-center">
                                @foreach ($errors->all() as $error)
                                    <h5><i class="icon-warning-sign"></i> {{ $error }}</h5>
                                @endforeach
                            </div>
                        @endif

                        <form action="{{url($guard.'/login')}}" method="post" name="adminLogin">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="emailaddress">Email address</label>
                                <input class="form-control" type="email" id="emailaddress" name="email" required="" placeholder="Enter your email">
                            </div>

                            <div class="form-group mb-3">
                                <label for="password">Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" class="form-control" name="password" placeholder="Enter your password">
                                    <div class="input-group-append" data-password="false">
                                        <div class="input-group-text">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                    <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                </div>
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block" type="submit"> Log In </button>
                            </div>

                        </form>



                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p> <a href="auth-recoverpw.html" class="text-muted ml-1">Forgot your password?</a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt">
    <script>document.write(new Date().getFullYear())</script> &copy; <a href="#" class="text-dark">Vimbo</a>
</footer>

<!-- Vendor js -->
<script src="{{asset('Backend/js/vendor.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('Backend/js/app.min.js')}}"></script>

</body>
</html>
