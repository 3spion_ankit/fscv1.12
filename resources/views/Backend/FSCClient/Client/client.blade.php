@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
<style>
table.dataTable thead .sorting_asc
{
	background-image:none !important;
}
</style>
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">FSC Clients</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Employee</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Client</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="{{route('fsc_client.client')}}" class="btn btn-warning waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>New Client
                                                </a>
                                            </div>
                                        </div>

                                        <table id="client-list" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Client ID</th>
                                                <th>Company Name(Legal Name)</th>
                                                <th>Business Name(DBA Name)</th>
                                                <th>Business Telephone</th>
                                                <th>Contact Name</th>
                                                <th>Contact Telephone</th>
                                                <th>Status</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <th>1</th>
                                                <th>AL-003</th>
                                                <th>Milestone Inc.</th>
                                                <th>Depot Food Store</th>
                                                <th>(770) 717-8945</th>
                                                <th>Mohammad Hossain</th>
                                                <th>(770) 717-8945</th>
                                                <th><x-status type="1" /></th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/client.js')}}"></script>
@endsection
