@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')

    <style>
        a:hover{
            cursor:pointer !important;
        }
        .alert-Fsc{
            color: #fff;
            background-color: #113c68;
            border-color: #113c68;
        }
        .table_small td,.table_small th
        {
            padding:0.20rem !important;
            font-size:12px !important;
            text-align:center;
        }
        .x_small{
            font-size:12px !important;
        }

        .fileupload_mt{
            margin-top:1.8rem !important;
        }
        .active
        {
            display:block !important;
        }





        .alert-Fsc{
            background-color:#fff2b3 !important;
            border:1px solid #103b68 !important;
            color:#103b68 !important;
        }

        .Header{
            background-color:#f1faff !important;
            border:1px solid #12186b !important;
            color:#000 !important;
        }

        .nav-pills > a
        {
            border:1px solid #ccc !important;
            background:#fff !important;
        }
        .nav-item {
            border:#ccc !important;
        }
        .page-title-box
        {
            margin:0 -27px 5px !important;
        }
        .padding_off{
            padding:0px !important;
        }
        .content-page
        {
            padding:70px 5px 65px 5px !imporatant;
        }
        .align-right
        {
            text-align:right !important;
        }
        .tab-pane .active{
            display:block !imporatant;
        }
        .link_dis{
            cursor: default;
            pointer-events: none;
            text-decoration: none;
        }
        .previous{
            display:none;
        }
    </style>
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Admin Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12 padding_off">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                            <h4>Add New Client</h4>
                        </div>

                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">

                                    <div id="rootwizard">
                                        <ul class="nav nav-pills nav-justified form-wizard-header mb-3">
                                            <li class="nav-item" data-target-form="#accountForm">
                                                <a href="#first" data-toggle="tab" class="nav-link active link_dis">
                                                    <span class="number">1</span>
                                                    <span class="d-none d-sm-inline">Basic Information</span>
                                                </a>
                                            </li>
                                            <li class="nav-item" data-target-form="#profileForm">
                                                <a href="#second" data-toggle="tab" class="nav-link link_dis">
                                                    <span class="number">2</span>
                                                    <span class="d-none d-sm-inline">Contact Information</span>
                                                </a>
                                            </li>
                                            <li class="nav-item" data-target-form="#otherForm">
                                                <a href="#third" data-toggle="tab" class="nav-link link_dis">
                                                    <span class="number">3</span>
                                                    <span class="d-none d-sm-inline">Service Information</span>
                                                </a>
                                            </li>
                                            <li class="nav-item" data-target-form="#Notes">
                                                <a href="#Forth" data-toggle="tab" class="nav-link link_dis">
                                                    <span class="number">4</span>
                                                    <span class="d-none d-sm-inline">Notes</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content mb-0 b-0">

                                            <div class="tab-pane active" id="first">
                                                <form id="accountForm" method="post" action="#" class="form-horizontal">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                <h4>Basic Information </h4>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Client ID (File No)</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" placeholder="AB" onkeypress="autotab(this, document.clientname.filename)" maxlength="2" class="form-control" id="clientid" name="filename1" im-insert="true">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" placeholder="123" required onkeypress="autotab1(this, document.clientname.filename3)" class="form-control" maxlength="3" id="clientid1" name="filename" required="">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" placeholder="A" required class="form-control" maxlength="12" id="clientid2" name="filename3" style="text-transform:uppercase;" required="">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <select name="status" id="status5" class="form-control" required="">
                                                                        <option value="">Status</option>
                                                                        <option value="Hold" class="Blue">Hold
                                                                        </option>
                                                                        <option class="Orange" value="Pending">
                                                                            Pending
                                                                        </option>
                                                                        <option value="Approval" class="Yellow">Approve
                                                                        </option>
                                                                        <option value="Active" class="Green">
                                                                            Active
                                                                        </option>
                                                                        <option value="Inactive" class="Red" style="background:red;color:#fff">
                                                                            Inactive
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Type</label>
                                                                <div class="col-md-4">
                                                                    <select name="business_id" id="business_id" required="" class="form-control" aria-required="true" aria-describedby="business_id-error" aria-invalid="false">
                                                                        <option value="">---Select---</option>
                                                                        <option value="1">Business</option>
                                                                        <option value="5">Investor</option>
                                                                        <option value="2">Non-Profit Organization</option>
                                                                        <option value="8">Other Business</option>
                                                                        <option value="6">Personal</option>
                                                                        <option value="4">Profession</option>
                                                                        <option value="3">Service Industry</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-5">

                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Category Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="business_catagory_name" required="" id="business_catagory_name" class="form-control" aria-invalid="false"><option value="">---Select---</option><option value="1">Auto Broker Dealer</option><option value="2">Cloth Store</option><option value="40">Coam Master License</option><option value="33">Conv. Store Only</option><option value="34">Conv. Store With Gas Station</option><option value="3">Cup Cake</option><option value="55">Farm</option><option value="4">Fast Food Restaurant</option><option value="52">Fuel Dealer</option><option value="12">Full Service Restaurant</option><option value="32">Gas Station Only</option><option value="6">General Merchandise Store</option><option value="7">Grocery Store</option><option value="8">Hotel / Motel</option><option value="9">Jewelry Store</option><option value="10">Liquor Store</option><option value="39">Mailing &amp; Packaging Store</option><option value="51">Misc Business</option><option value="53">Online Business-Retail</option><option value="11">Pizza Store</option><option value="13">Sandwich Store</option><option value="14">Tobacco Store</option><option value="15">Wholesale Store</option></select>
                                                                </div>
                                                                <div class="col-md-5">

                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">NAICS / SIC Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" required="" class="form-control" id="" name="" placeholder="NAICS" value="" readonly="" aria-invalid="false">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" required="" class="form-control fsc-input sic" placeholder="SIC" id="" name="" value="" readonly="">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <a href="javascript: void(0);" class="btn btn-primary">Go to Link</a>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Company Name</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control fsc-input" placeholder="Please Enter Your Company Name" name="company_name" id="company_name" value="">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Name</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" name="business_name" placeholder="Please Enter Your Business Name" id="business_name" value="">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Address 1</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control fsc-input" placeholder="Please Enter Your  Address 1" name="address" id="address">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Address 2</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control fsc-input" placeholder="Please Enter Your  Address 2" name="address1" id="address1">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Country</label>
                                                                <div class="col-md-3">
                                                                    <select name="countryId" required="" id="countries_states1" data-country="USA" class="form-control" aria-required="true" aria-describedby="countries_states1-error" aria-invalid="false"><option value="IND">IND</option><option value="USA">USA</option></select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">City / State / Zip</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" id="city" name="city" placeholder="City" value="">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="stateId" required="" id="stateId" class="form-control" data-country="countries_states1"><option value="AL">AL</option><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="AF">AF</option><option value="AA">AA</option><option value="AC">AC</option><option value="AE">AE</option><option value="AM">AM</option><option value="AP">AP</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" required="" class="form-control" id="zip" name="zip" maxlength="5" placeholder="Zip">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3"></label>
                                                                <div class="col-md-8">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="billingtoo" class="custom-control-input" id="billingtoo" >
                                                                        <label class="custom-control-label" for="customCheck1">Mailing address Same as above Business</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Mailing Address 1</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" id="second_address" name="second_address" placeholder="Mailing Address 1">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Mailing Address 2</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" id="second_address1" name="second_address1" placeholder="Mailing Address 2">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">City / State / Zip</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" id="second_city" name="second_city" placeholder="City" value="">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="second_stateId" required="" id="second_stateId" class="form-control" data-country="countries_states1"><option value="AL">AL</option><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="AF">AF</option><option value="AA">AA</option><option value="AC">AC</option><option value="AE">AE</option><option value="AM">AM</option><option value="AP">AP</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" required="" class="form-control" id="second_zip" name="second_zip" maxlength="5" placeholder="Zip">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Business Email</label>
                                                                <div class="col-md-6">
                                                                    <input type="email" required="" class="form-control" id="email" name="email" placeholder="Email ID" value="">
                                                                </div>
                                                                <div class="col-sm-2 mt-1">
                                                                    (Primary Email)
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Telephone No 1.</label>
                                                                <div class="col-md-3">
                                                                    <input name="business_no" required="" value="" type="tel" id="motelephoneile" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999" aria-invalid="false">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="businesstype" required="" id="telephoneNo1Type" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="Business">Business</option>
                                                                        <option value="Office">Office</option>
                                                                        <option value="Resid">Res.</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input class="form-control" required="" id="ext1" maxlength="4" readonly="" name="businessext" value="" placeholder="Ext" type="text">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Telephone No 2.</label>
                                                                <div class="col-md-3">
                                                                    <input name="business_no" required="" value="" type="tel" id="motelephoneile1" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999" aria-invalid="false">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="businesstype" required="" id="telephoneNo1Type1" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="Business">Business</option>
                                                                        <option value="Office">Office</option>
                                                                        <option value="Resid">Res.</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input class="form-control" required="" id="ext1" maxlength="4" readonly="" name="businessext1" value="" placeholder="Ext" type="text">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Fax #</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" value="" id="business_fax" name="business_fax" placeholder="(999) 999-9999">
                                                                </div>
                                                                <div class="col-md-6">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Website</label>
                                                                <div class="col-md-8">
                                                                    <input type="url" required="" class="form-control" id="website" value="" name="website" placeholder="Website address">
                                                                    <span style="float:right;"><label>Note :</label> [ http://or https:// required ]</span>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>

                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->

                                                </form>
                                            </div>

                                            <div class="tab-pane fade" id="second">
                                                <form id="profileForm" method="post" action="#" class="form-horizontal">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                <h4>Contact Information</h4>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Name</label>

                                                                <div class="col-md-2">
                                                                    <select type="text" required="" class="form-control" id="minss" name="minss" aria-invalid="false">
                                                                        <option value="Mr.">Mr.</option>
                                                                        <option value="Mrs.">Mrs.</option>
                                                                        <option value="Miss.">Miss.</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" placeholder="First Name" id="firstname" name="firstname" value="">
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <input type="text" required="" class="form-control" id="middlename" placeholder="M" name="middlename" maxlength="1" value="">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" required="" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="">
                                                                </div>

                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3"></label>
                                                                <div class="col-md-8">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="faxbli3" class="custom-control-input" id="faxbli3">
                                                                        <label class="custom-control-label" for="customCheck1">Same As Business Address</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Address 1</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" placeholder="Please Enter Your  Address 1" name="contact_address1" id="contact_address1">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Address 2</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" placeholder="Please Enter Your  Address 2" name="contact_address2" id="contact_address2">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">City / State / Zip</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" id="city_1" name="city_1" placeholder="City" value="">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="state_1" required="" id="state_1" class="form-control" data-country="countries_states1"><option value="AL">AL</option><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="AF">AF</option><option value="AA">AA</option><option value="AC">AC</option><option value="AE">AE</option><option value="AM">AM</option><option value="AP">AP</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="text" required="" class="form-control" id="zip_1" name="zip_1" maxlength="5" placeholder="Zip">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3"></label>
                                                                <div class="col-md-8">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="faxbli3_g" class="custom-control-input" id="faxbli3_g" >
                                                                        <label class="custom-control-label" for="customCheck1">Same As Basic Telephone</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Telephone No 1.</label>
                                                                <div class="col-md-3">
                                                                    <input name="etelephone1" required="" value="" type="tel" id="etelephone1" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999" aria-invalid="false">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="mobiletype_1" required="" id="mobiletype_1" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="Business">Business</option>
                                                                        <option value="Office">Office</option>
                                                                        <option value="Resid">Res.</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input class="form-control" required="" id="ext2_1" maxlength="4" readonly="" name="ext2_1" value="" placeholder="Ext" type="text">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Telephone No 2.</label>
                                                                <div class="col-md-3">
                                                                    <input name="mobile_2" required="" value="" type="tel" id="mobile_2" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999" aria-invalid="false">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="mobiletype_2" required="" id="mobiletype_2" class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option value="Business">Business</option>
                                                                        <option value="Office">Office</option>
                                                                        <option value="Resid">Res.</option>
                                                                        <option value="Mobile">Mobile</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input class="form-control" required="" id="ext2_2" maxlength="4" readonly="" name="ext2_2" value="" placeholder="Ext" type="text">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3"></label>
                                                                <div class="col-md-8">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="faxbli2" class="custom-control-input" id="faxbli2" >
                                                                        <label class="custom-control-label" for="customCheck1">Same As Business Fax</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Fax #</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" required="" class="form-control" data-country="countries_states1" data-format="  (999) 999-9999" value="" id="contact_fax" name="contact_fax" placeholder="(999) 999-9999">
                                                                </div>
                                                                <div class="col-md-6">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3"></label>
                                                                <div class="col-md-8">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="emailbli2" class="custom-control-input" id="emailbli2" >
                                                                        <label class="custom-control-label" for="customCheck1">Same As Business Email</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Email</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" required="" class="form-control" id="email_1" name="email_1" placeholder="Email ID">
                                                                </div>
                                                                <div class="col-md-1">
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <!-- end col -->
                                                    </div>

                                                    <!-- end row -->
                                                </form>
                                            </div>

                                            <div class="tab-pane fade" id="third">
                                                <form id="otherForm" method="post" action="#" class="form-horizontal">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-center">
                                                                <h2 class="mt-0">
                                                                    <i class="mdi mdi-check-all"></i>
                                                                </h2>
                                                                <h3 class="mt-0">Thank you !</h3>

                                                                <p class="w-75 mb-2 mx-auto">Quisque nec turpis at urna dictum luctus. Suspendisse convallis dignissim eros at volutpat. In egestas mattis
                                                                    dui. Aliquam mattis dictum aliquet.</p>

                                                                <div class="mb-3">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="customCheck4" required="">
                                                                        <label class="custom-control-label" for="customCheck4">I agree with the Terms and Conditions</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end col -->
                                                    </div>
                                                    <!-- end row -->
                                                </form>
                                            </div>

                                            <div class="tab-pane fade" id="Forth">
                                                <form id="Notes" method="post" action="#" class="form-horizontal">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-center">
                                                                <h2 class="mt-0">
                                                                    <i class="mdi mdi-check-all"></i>
                                                                </h2>
                                                                <h3 class="mt-0">Thank you !</h3>

                                                                <p class="w-75 mb-2 mx-auto">Quisque nec turpis at urna dictum luctus. Suspendisse convallis dignissim eros at volutpat. In egestas mattis
                                                                    dui. Aliquam mattis dictum aliquet.</p>

                                                                <div class="mb-3">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="customCheck4" required="">
                                                                        <label class="custom-control-label" for="customCheck4">I agree with the Terms and Conditions</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end col -->
                                                    </div>
                                                    <!-- end row -->
                                                </form>
                                            </div>

                                            <ul class="list-inline wizard mb-0">
                                                <li class="previous list-inline-item disabled"><a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                                                </li>
                                                <li class="next list-inline-item float-right"><a href="javascript: void(0);" class="btn btn-secondary">Next</a></li>
                                            </ul>

                                        </div> <!-- tab-content -->
                                    </div> <!-- end #rootwizard-->

                                </div> <!-- end card-body -->
                            </div> <!-- end card-->
                        </div>




                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $( ".next" ).click(function() {
                $(".previous").css("display","block");
            });
        });
    </script>
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
    <script src="{{asset('Backend/js/pages/form-wizard.init.js')}}"></script>
    <script src="{{asset('Backend/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
@endsection
