@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee / User</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Hiring Ads.</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">Hiring</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-new-hiring">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>New Hiring
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-hiring-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                            <tr>
                                                <th>No.</th>
                                                <th>Post Date</th>
                                                <th>Position Name</th>
                                                <th>Country</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <!--<th>Description</th>

                                                <th>Job Type</th>-->
                                                <th width="7%">Action</th>
                                            </tr>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @php $dateFormat = new \App\Utility\DateUtility() @endphp
                                            @if(isset($hiringList))
                                                @foreach($hiringList as $key=>$hiring)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$dateFormat->retriveDate($hiring->post_date)}}</td>
                                                        <td>{{$hiring->position}}</td>
                                                        <td>{{$hiring->country}}</td>
                                                        <td>{{$hiring->state}}</td>
                                                        <td>{{$hiring->city}}</td>
                                                        <td>
                                                            <x-actionlink
                                                                id="modal-new-hiring_{{$hiring->id}}"
                                                                class="waves-effect waves-light"
                                                                buttons="edit"
                                                                route="javascript:void(0);"
                                                            />
                                                            <x-actionlink route="" id="" class="waves-effect waves-light" buttons="delete"/>
                                                        </td>
                                                    </tr>

                                                    <!-- Edit hiring Form  -->
                                                    <!-- start -->
                                                    <div class="modal fade edit_mode" data-state="{{$hiring->state}}" data-src="{{route('ajax.setState')}}" id="modal-new-hiring_{{$hiring->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <div class="text-center col-md-12">
                                                                        <h4 class="modal-title" id="myLargeModalLabel">New Hiring</h4>
                                                                    </div>
                                                                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="card">
                                                                        <div class="card-body">

                                                                            <form class="control-form" method="POST" action="{{route('employee.hiring.storeHiring')}}">
                                                                                @csrf
                                                                                <input type="hidden" name="hiring_id" value="{{$hiring->id}}">
                                                                                <div class="form-group row mb-3">
                                                                                    <label class="col-form-label col-md-3 text-right">Posting Date :</label>
                                                                                    <div class="col-md-3">
                                                                                        <input name="post_date" type="text" id="post_date" value="{{$dateFormat->retriveDateInVal($hiring->post_date)}}" class="form-control datepickers">
                                                                                    </div>
                                                                                    <label class="col-form-label col-md-3 text-right">Job type :</label>
                                                                                    <div class="col-md-3">
                                                                                        <select class="form-control" name="job_type" id="job_type">
                                                                                            <option value="">Select</option>
                                                                                            <option value="Full Time" {{$hiring->job_type=="Full Time" ? "Selected" : ""}}>Full Time</option>
                                                                                            <option value="Part Time" {{$hiring->job_type=="Part Time" ? "Selected" : ""}}>Part Time</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row mb-3">
                                                                                    <label class="col-md-3 col-form-label text-right" for="position">Position Name</label>
                                                                                    <div class="col-md-9">
                                                                                        <input type="text" name="position" class="form-control" id="position" value="{{$hiring->position}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row mb-3">
                                                                                    <label class="col-md-3 col-form-label text-right">Country / State / City</label>
                                                                                    <div class="col-md-3">
                                                                                        <select class="form-control country" name="country" id="country_{{$hiring->id}}" data-src="{{route('ajax.setState')}}">
                                                                                            <option value="">Select</option>
                                                                                            <option value="IND" {{$hiring->country=="IND" ? "Selected" : ""}}>IND</option>
                                                                                            <option value="USA" {{$hiring->country=="USA" ? "Selected" : ""}}>USA</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <select class="form-control js-select-2" name="state" id="states_{{$hiring->id}}">
                                                                                            <option value="">Select</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <input type="text" name="city" placeholder="warning" class="form-control" id="city" value="{{$hiring->city}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row mb-3">
                                                                                    <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                                                                    <div class="col-md-3">
                                                                                        <input type="text" name="name" class="form-control" id="name" value="{{$hiring->name}}">
                                                                                    </div>
                                                                                    <label class="col-md-3 col-form-label text-right" for="certificate_no">Telephone</label>
                                                                                    <div class="col-md-3">
                                                                                        <input type="text" class="form-control" name="contact_no" id="telephone_{{$hiring->id}}" value="{{$hiring->contact_no}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row mb-3">
                                                                                    <label class="col-md-3 col-form-label text-right" for="description">Description</label>
                                                                                    <div class="col-md-9">
                                                                                        <textarea id="description" name="description" class="form-control">{{$hiring->description}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row mb-3 text-center">
                                                                                    <div class="col-md-6">
                                                                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                    <!-- End -->
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- New hiring Form  -->
    <!-- start -->
    <div class="modal fade" id="modal-new-hiring" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">New Hiring</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="POST" action="{{route('employee.hiring.storeHiring')}}">
                                @csrf
                                <div class="form-group row mb-3">
                                    <label class="col-form-label col-md-3 text-right">Posting Date :</label>
                                    <div class="col-md-3">
                                        <input name="post_date" type="text" id="post_date" value="{{\Carbon\Carbon::now()}}" class="form-control">
                                    </div>
                                    <label class="col-form-label col-md-3 text-right">Job type :</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="job_type" id="job_type">
                                            <option value="">Select</option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="position_name">Position Name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="position" class="form-control" id="position_name">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right">Country / State / City</label>
                                    <div class="col-md-3">
                                        <select class="form-control country" name="country" id="country_0" data-src="{{route('ajax.setState')}}">
                                            <option value="">Select</option>
                                            <option value="IND">IND</option>
                                            <option value="USA">USA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control js-select-2" name="state" id="states_0">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" name="city" placeholder="warning" class="form-control" id="city">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Name</label>
                                    <div class="col-md-3">
                                        <input type="text" name="name" class="form-control" id="name">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="certificate_no">Telephone</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="contact_no" id="telephone_0">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="description">Description</label>
                                    <div class="col-md-9">
                                        <textarea id="description" name="description" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/employee.js')}}"></script>
@endsection
