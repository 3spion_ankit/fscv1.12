@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee/Add-Edit Profile </a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12 padding_off">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                            <h3>
                                Edit Employee / User
                            </h3>
                        </div>

                        <div class="row">
                            <div class="col col-md-12">

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                <a class="nav-link show mb-1 active" id="v-pills-General-tab" data-toggle="pill" href="#v-pills-General" role="tab" aria-controls="v-pills-General" aria-selected="false">
                                                    General Info</a>
                                                <a class="nav-link mb-1" id="v-pills-Hiring-tab" data-toggle="pill" href="#v-pills-Hiring" role="tab" aria-controls="v-pills-Hiring" aria-selected="true">
                                                    Hiring Info</a>
                                                <a class="nav-link mb-1" id="v-pills-Pay-tab" data-toggle="pill" href="#v-pills-Pay" role="tab" aria-controls="v-pills-Pay" aria-selected="false">
                                                    Pay Info</a>
                                                <a class="nav-link mb-1" id="v-pills-Personal-tab" data-toggle="pill" href="#v-pills-Personal" role="tab" aria-controls="v-pills-Personal" aria-selected="false">
                                                    Personal Info</a>
                                                <a class="nav-link mb-1" id="v-pills-Security-tab" data-toggle="pill" href="#v-pills-Security" role="tab" aria-controls="v-pills-Security" aria-selected="false">
                                                    Security Info</a>
                                                <a class="nav-link mb-1" id="v-pills-Rights-tab" data-toggle="pill" href="#v-pills-Rights" role="tab" aria-controls="v-pills-Rights" aria-selected="false">
                                                    User Rights</a>
                                                <a class="nav-link mb-1" id="v-pills-Rules-tab" data-toggle="pill" href="#v-pills-Rules" role="tab" aria-controls="v-pills-Rules" aria-selected="false">
                                                    Rules</a>
                                                <a class="nav-link mb-1" id="v-pills-Responsiblity-tab" data-toggle="pill" href="#v-pills-Responsiblity" role="tab" aria-controls="v-pills-Responsiblity" aria-selected="false">
                                                    Responsiblity</a>
                                                <a class="nav-link mb-1" id="v-pills-Agreement-tab" data-toggle="pill" href="#v-pills-Agreement" role="tab" aria-controls="v-pills-Agreement" aria-selected="false">
                                                    Agreement</a>
                                                <a class="nav-link mb-1" id="v-pills-Other-tab" data-toggle="pill" href="#v-pills-Other" role="tab" aria-controls="v-pills-Other" aria-selected="false">
                                                    Other</a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-sm-10">
                                            <form action="{{route('employee.updateInfo')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="emp_id" value="{{$user_id}}">
                                                @php $dateFormat = new \App\Utility\DateUtility() @endphp
                                                <div class="tab-content pt-0">
                                                    <div class="card border-secondary mb-3">

                                                        <div class="tab-pane fade active show" id="v-pills-General" style="display:none;" role="tabpanel" aria-labelledby="v-pills-General-tab">
                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                <h4> General Infomation </h4>
                                                            </div>
                                                            <div class="card-body text-secondary">

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <label for="Type">Type</label>
                                                                            <select class="form-control" id="Type">
                                                                                <option value="">select</option>
                                                                                <option value="1" @if($edit_employee->user_type==1) selected @endif> Employee</option>
                                                                                <option value="2" @if($edit_employee->user_type==2) selected @endif>User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Employee ID</label>
                                                                            <input class="form-control" id="Employee_ID" name="user_code" value="@if(isset($edit_employee)) {{$edit_employee->user_code}} @endif" placeholder="Employee ID" readonly/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <label for="Status">Status</label>
                                                                            <select class="form-control" id="Status" name="status">
                                                                                <option value="1" @if($edit_employee->status==1) selected @endif>Active</option>
                                                                                <option value="2" @if($edit_employee->status==2) selected @endif>In-Active</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <div>
                                                                                <label for="Nickname" style="float:left;">Nickname</label>
                                                                                <a style="float:right;" data-toggle="modal" data-target="#modal_NickName"><i class="fa fa-plus"></i></a>
                                                                            </div>
                                                                            <select class="form-control" id="Nickname">
                                                                                <option>Manager</option>
                                                                                <option>B-Vijay</option>
                                                                                <option>Team-A</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Name</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-2">
                                                                                    <select class="form-control" id="prefix_name" name="prefix_name">
                                                                                        <option value="Mr" @if($edit_employee->prefix_name=="Mr") selected @endif>Mr.</option>
                                                                                        <option value="Mrs" @if($edit_employee->prefix_name=="Mrs") selected @endif>Mrs.</option>
                                                                                        <option value="Miss" @if($edit_employee->prefix_name=="Miss") selected @endif>Miss.</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control" placeholder="First Name" value="@if(isset($edit_employee)) {{$edit_employee->fname}} @endif"/>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <input class="form-control" placeholder="M" value="@if(isset($edit_employee)) {{$edit_employee->mname}} @endif"/>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control" placeholder="Last Name" value="@if(isset($edit_employee)) {{$edit_employee->lname}} @endif"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Type">Address 1</label>
                                                                            <input class="form-control" id="Type" placeholder="Address 1" value="@if(isset($edit_employee)) {{$edit_employee->address_1}} @endif"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Type">Address 2</label>
                                                                            <input class="form-control" placeholder="Address 2" id="Type" value="@if(isset($edit_employee)) {{$edit_employee->address_2}} @endif"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Type">Country</label>
                                                                            <select name="countryId" id="countries_states1" class="form-control" data-country="IND">
                                                                                <option value="IND" @if($edit_employee->prefix_name=="IND") selected @endif>IND</option>
                                                                                <option value="USA" @if($edit_employee->prefix_name=="USA") selected @endif>USA</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Type">Fax</label>
                                                                            <input class="form-control" id="Fax" placeholder="(999) 999-9999" value="@if(isset($edit_employee)){{$edit_employee->address_2}}@endif"/>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">City / State / Zip</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control" placeholder="City" value="@if(isset($edit_employee)){{$edit_employee->city}}@endif"/>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <x-state data="state" message="@if(isset($edit_employee)) {{$edit_employee->state}} @endif"/>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control input-usa-zip-mask" placeholder="Zip" value="@if(isset($edit_employee)){{$edit_employee->zip}}@endif"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Personal Telephone</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control input-tel-mask" placeholder="(999) 999-9999" value="@if(isset($edit_employee)){{$edit_employee->contact_no_1}}@endif"/>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control">
                                                                                        <option value="Home" @if($edit_employee->contact_no_type_1=="Home") selected @endif>Home</option>
                                                                                        <option value="Mobile" @if($edit_employee->contact_no_type_1=="Mobile") selected @endif>Mobile</option>
                                                                                        <option value="Office" @if($edit_employee->contact_no_type_1=="Office") selected @endif>Office</option>
                                                                                        <option value="Other" @if($edit_employee->contact_no_type_1=="Other") selected @endif>Other</option>
                                                                                        <option value="Work" @if($edit_employee->contact_no_type_1=="Work") selected @endif>Work</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control input-extension-mask" placeholder="Ext" value="@if(isset($edit_employee)){{$edit_employee->contact_no_extension_1}}@endif"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">FSC Telephone</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control input-tel-mask" id="FSC_Telephone" placeholder="(999) 999-9999" value="@if(isset($edit_employee)){{$edit_employee->contact_no_2}}@endif"/>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                                                        <option value="Home" @if($edit_employee->contact_no_type_2=="Home") selected @endif>Home</option>
                                                                                        <option value="Mobile" @if($edit_employee->contact_no_type_2=="Mobile") selected @endif>Mobile</option>
                                                                                        <option value="Office" @if($edit_employee->contact_no_type_2=="Office") selected @endif>Office</option>
                                                                                        <option value="Other" @if($edit_employee->contact_no_type_2=="Other") selected @endif>Other</option>
                                                                                        <option value="Work" @if($edit_employee->contact_no_type_2=="Work") selected @endif>Work</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <input class="form-control input-extension-mask" placeholder="Ext" value="@if(isset($edit_employee)){{$edit_employee->contact_no_extension_2}}@endif"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Type">Personal Email</label>
                                                                            <input class="form-control" placeholder="Personal Email" value="@if(isset($edit_employee)){{$edit_employee->email}}@endif" readonly/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Type">FSC Email</label>
                                                                            <input class="form-control" placeholder="FSC Email"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Type">Picture</label>
                                                                            <div>
                                                                                <div class="fileupload btn btn-success waves-effect waves-light">
                                                                                    <span><i class="mdi mdi-cloud-upload mr-1"></i> Upload Files</span>
                                                                                    <input type="file" class="upload">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Hiring" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Hiring-tab">
                                                            @if(isset($edit_employee->hiring["id"]))
                                                                <input type="hidden" name="hiring_id" value="{{$edit_employee->hiring["id"]}}">
                                                            @endif

                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                <h4>Hiring Infomation</h4>
                                                            </div>
                                                            <div class="card-body text-secondary">
                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Type">Hire Date</label>
                                                                            <div class="input-group">
                                                                                <input type="text" id="Hire_Date"
                                                                                       value="{{isset($edit_employee->hiring["id"]) ? $dateFormat->retriveDateInVal($edit_employee->hiring["hire_date"]) : ""}}"
                                                                                       name="hire_date" class="form-control datepickers" placeholder="mm/dd/yyyy">
                                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text"><i class="ri-calendar-event-fill"></i></span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Status">Termination Date</label>
                                                                            <div class="input-group">
                                                                                <input type="text" id="Termination_Date"
                                                                                       value="{{isset($edit_employee->hiring["id"]) ? $dateFormat->retriveDateInVal($edit_employee->hiring["termination_date"]) : ""}}"
                                                                                       name="termination_date" class="form-control datepickers" placeholder="mm/dd/yyyy" data-provide="datepicker" data-date-format="mm/dd/yyyy">
                                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text"><i class="ri-calendar-event-fill"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Note</label>
                                                                            <textarea id="t_Note" type="text" name="t_note" placeholder="Note" class="form-control">{{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["t_note"] : ""}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">

                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Type">Re-Hire Date</label>

                                                                            <div class="input-group">
                                                                                <input type="text" id="ReHire_Date" value="{{isset($edit_employee->hiring["id"]) ? $dateFormat->retriveDateInVal($edit_employee->hiring["termination_date"]) : ""}}" name="rehire_date" class="form-control" placeholder="mm/dd/yyyy" data-provide="datepicker" data-date-format="mm/dd/yyyy">
                                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text"><i class="ri-calendar-event-fill"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="Status">Termination Date</label>

                                                                            <div class="input-group">
                                                                                <input type="text" id="Termination_Date2" value="{{isset($edit_employee->hiring["id"]) ? $dateFormat->retriveDateInVal($edit_employee->hiring["retermination_date"]) : ""}}" name="retermination_date" class="form-control" placeholder="mm/dd/yyyy" data-provide="datepicker" data-date-format="mm/dd/yyyy">
                                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text"><i class="ri-calendar-event-fill"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                    </div>

                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Note</label>
                                                                            <textarea id="t_Note2" type="text" name="t_note2" placeholder="Note" class="form-control">{{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["t_note2"] : ""}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                <h4>Branch / Department Information</h4>
                                                            </div>
                                                            <div class="card-body text-secondary">
                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="Type">Branch City</label>
                                                                            <select name="branch_city" id="branch_city" class="form-control">
                                                                                <option value="">---Select---</option>
                                                                                <option value="Norcross, GA" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["branch_city"]=="Norcross, GA" ? "Selected" : "" : ""}} >Norcross, GA</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="Type">Branch Name</label>
                                                                            <input class="form-control" value="{{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["branch_name"] : ""}}" name="branch_name" id="Branch_Name" placeholder="Branch Name"/>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="Type">position</label>
                                                                            <select name="hiring_position" id="hiring_position" class="form-control">
                                                                                <option value="">---Select Position---</option>
                                                                                <option value="1" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==1 ? "Selected" : "" : ""}}>Asst. Bookeeper</option>
                                                                                <option value="6" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==6 ? "Selected" : "" : ""}}>CPA</option>
                                                                                <option value="14" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==14 ? "Selected" : "" : ""}}>Head Bookkeeper</option>
                                                                                <option value="9" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==9 ? "Selected" : "" : ""}}>Manager</option>
                                                                                <option value="2" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==2 ? "Selected" : "" : ""}}>Office Assistant</option>
                                                                                <option value="11" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==11 ? "Selected" : "" : ""}}>Project Manager</option>
                                                                                <option value="13" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==13 ? "Selected" : "" : ""}}>Sales Person</option>
                                                                                <option value="10" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==10 ? "Selected" : "" : ""}}>Team Leader</option>
                                                                                <option value="7" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["hiring_position"]==7 ? "Selected" : "" : ""}}>Temp</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <div class="mt-4">
                                                                                <input type="checkbox" id="is_supervisor" name="is_supervisor" value="1" {{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["is_supervisor"]==1 ? "checked" : "" : ""}}>
                                                                                <label for="customCheck1">Supervisor</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label for="Employee">Note</label>
                                                                            <textarea id="branch_note" type="text" name="branch_note" placeholder="Note" class="form-control">{{isset($edit_employee->hiring["id"]) ? $edit_employee->hiring["branch_note"] : ""}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Pay" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Pay-tab">
                                                            <div class="">
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>Pay Information</h4>
                                                                </div>
                                                                <div class="card-body text-secondary mb-3" style="padding:0px !Important;">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="mb-2">
                                                                            <div class="row">
                                                                                <div class="col-12 text-sm-center form-inline">
                                                                                    <div class="form-group mr-2">
                                                                                        <a href="javascript:void(0);" id="demo-btn-addrow" data-toggle="modal" data-target="#modal_Add_Pay" class="btn btn-warning">
                                                                                            <i class="mdi mdi-plus-circle mr-2"></i> Add Pay
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <table id="pay_information_table" class="table table-bordered no-paging">
                                                                            <thead>
                                                                            <tr class="text-center">
                                                                                <th>Pay Method</th>
                                                                                <th>Pay Duration</th>
                                                                                <th>Pay Rate</th>
                                                                                <th>Effective Date</th>
                                                                                <th>Note</th>
                                                                                <th width="7%">Action</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @if(count($edit_employee->Pay) > 0)
                                                                                @foreach($edit_employee->Pay as $key=>$pay)
                                                                                    <tr class="text-center">
                                                                                        <td>{{$pay->pay_method}}</td>
                                                                                        <td>{{$pay->pay_frequency}}</td>
                                                                                        <td>${{$pay->pay_rate}}.00</td>
                                                                                        <td>{{$dateFormat->retriveDate($pay->effective_date)}}</td>
                                                                                        <td>{{$pay->notes}}</td>
                                                                                        <td>
                                                                                            <x-actionlink
                                                                                                id="modal_Add_Pay-{{$pay->id}}"
                                                                                                class="waves-effect waves-light"
                                                                                                buttons="edit"
                                                                                                route="javascript:void(0);"
                                                                                            />
                                                                                            <x-actionlink route="" id="" class="waves-effect waves-light" buttons="delete"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <div class="modal fade" id="modal_Add_Pay-{{$pay->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                        <div class="modal-dialog modal-lg">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <div class="text-center col-md-12">
                                                                                                        <h4 class="modal-title" id="myLargeModalLabel">Add Pay Information</h4>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div class="card">
                                                                                                        <div class="card-body">
                                                                                                            <form class="control-form" name="pay_form" id="pay_form">
                                                                                                                @csrf
                                                                                                                <input type="hidden" name="user_id" value="{{$user_id}}"/>
                                                                                                                <input type="hidden" name="pay_id" value="{{$pay->id}}"/>
                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Pay Method</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <select class="form-control form-control" name="pay_method">
                                                                                                                            <option value="">Select</option>
                                                                                                                            <option value="Salary" {{$pay->pay_method == "Salary" ? 'selected' : ""}}>Salary</option>
                                                                                                                            <option value="Hourly" {{$pay->pay_method == "Hourly" ? 'selected' : ""}}>Hourly</option>
                                                                                                                        </select>
                                                                                                                    </div>
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="name">Pay Duration</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <select class="form-control form-control" name="pay_frequency">
                                                                                                                            <option value="">Select</option>
                                                                                                                            <option value="Weekly" {{$pay->pay_frequency == "Weekly" ? 'selected' : ""}}>Weekly</option>
                                                                                                                            <option value="Bi-Weekly" {{$pay->pay_frequency == "Bi-Weekly" ? 'selected' : ""}}>Bi-Weekly</option>
                                                                                                                            <option value="Semi-Monthly" {{$pay->pay_frequency == "Semi-Monthly" ? 'selected' : ""}}>Semi-Monthly</option>
                                                                                                                            <option value="Monthly" {{$pay->pay_frequency == "Monthly" ? 'selected' : ""}}>Monthly</option>
                                                                                                                        </select>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Pay Rate</label>
                                                                                                                    <div class="col-md-3 input-group">
                                                                                                                        <div class="input-group-prepend">
                                                                                                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                                                                                                        </div>
                                                                                                                        <input type="text" name="pay_rate" class="form-control" id="pay_rate" required="" value="{{$pay->pay_rate}}">
                                                                                                                    </div>
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="name">Effective Date</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <input type="text" name="effective_date" placeholder="mm/dd/yyyy"
                                                                                                                               class="form-control datepickers" value="{{$dateFormat->retriveDateInVal($pay->effective_date)}}">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Note</label>
                                                                                                                    <div class="col-md-9">
                                                                                                                        <input class="form-control" name="notes" id="notes" placeholder="Note" value="{{$pay->notes}}">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3 text-center">
                                                                                                                    <div class="col-sm-12">
                                                                                                                        <div class="form-group">
                                                                                                                            <a href="javascript:void(0);" data-src="{{route('ajax.storePay')}}" id="pay_form_submit" class="col-md-2 btn btn-primary text-center">Update</a>
                                                                                                                            <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div><!-- /.modal-content -->
                                                                                        </div><!-- /.modal-dialog -->
                                                                                    </div>
                                                                                @endforeach
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                </div>

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>Review Information</h4>
                                                                </div>

                                                                <div class="card-body text-secondary mb-3" style="padding:0px !Important;">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="mb-2">
                                                                            <div class="row">
                                                                                <div class="col-12 text-sm-center form-inline">
                                                                                    <div class="form-group mr-2">
                                                                                        <a href="javascript:void(0);" id="demo-btn-Review" data-toggle="modal" data-target="#modal_Review_Pay" class="btn btn-warning">
                                                                                            <i class="mdi mdi-plus-circle mr-2"></i> Add Review
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <table id="review_information_table" class="table table-bordered no-paging">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Review No.</th>
                                                                                <th>Review Date</th>
                                                                                <th>Next Review</th>
                                                                                <th>Next Review Date</th>
                                                                                <th>Next Pay Rate</th>
                                                                                <th>Comments</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @if(count($edit_employee->Review) > 0)
                                                                                @foreach($edit_employee->Review as $key=>$review)
                                                                                    <tr class="text-center">
                                                                                        <td>{{$key+1}}</td>
                                                                                        <td>{{$dateFormat->retriveDate($review->review_date)}}</td>
                                                                                        <td>{{$review->next_review}}</td>
                                                                                        <td>{{$dateFormat->retriveDate($review->next_review_date)}}</td>
                                                                                        <td>${{$review->next_pay_rate}}.00</td>
                                                                                        <td>{{$review->comments}}</td>
                                                                                        <td>
                                                                                            <x-actionlink
                                                                                                id="modal_Review_Pay-{{$review->id}}"
                                                                                                class="waves-effect waves-light"
                                                                                                buttons="edit"
                                                                                                route="javascript:void(0);"
                                                                                            />
                                                                                            <x-actionlink route="" id="" class="waves-effect waves-light" buttons="delete"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <div class="modal fade" id="modal_Review_Pay-{{$review->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                                        <div class="modal-dialog modal-lg">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <div class="text-center col-md-12">
                                                                                                        <h4 class="modal-title" id="myLargeModalLabel">Add Review Information</h4>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div class="card">
                                                                                                        <div class="card-body">
                                                                                                            <form class="control-form" method="post" id="review_form">
                                                                                                                @csrf
                                                                                                                <input type="hidden" name="user_id" value="{{$user_id}}">
                                                                                                                <input type="hidden" name="review_id" value="{{$review->id}}">
                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Review Date</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <input type="text" value="{{$dateFormat->retriveDateInVal($review->review_date)}}" name="review_date" placeholder="mm/dd/yyyy" class="form-control datepickers">
                                                                                                                    </div>
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Next Review</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <input type="text" value="{{$review->next_review}}" name="next_review" id="next_review" placeholder="Reviews" class="form-control only-numbers" onblur="">
                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="name">Next Review Date</label>
                                                                                                                    <div class="col-md-3">
                                                                                                                        <input type="text" value="{{$dateFormat->retriveDateInVal($review->next_review_date)}}" name="next_review_date" id="next_review_date" class="form-control" readonly>
                                                                                                                    </div>
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="name">Next Pay Rate</label>
                                                                                                                    <div class="col-md-3 input-group">
                                                                                                                        <div class="input-group-prepend">
                                                                                                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                                                                                                        </div>
                                                                                                                        <input type="text" value="{{$review->next_pay_rate}}" name="next_pay_rate" id="next_pay_rate" class="form-control only-numbers">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3">
                                                                                                                    <label class="col-md-3 col-form-label text-right" for="Type">Comment</label>
                                                                                                                    <div class="col-md-9">
                                                                                                                        <textarea type="text" name="comments" placeholder="Write a comments ..." class="form-control">{{$review->comments}} </textarea>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group row mb-3 text-center">
                                                                                                                    <div class="col-sm-12">
                                                                                                                        <div class="form-group">
                                                                                                                            <a href="javascript:void(0);" data-src="{{route('ajax.storeReview')}}" id="review_submit" class="col-md-2 btn btn-primary text-center">Submit</a>
                                                                                                                            <a class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div><!-- /.modal-content -->
                                                                                        </div><!-- /.modal-dialog -->
                                                                                    </div>

                                                                                @endforeach
                                                                            @endif
                                                                            </tbody>

                                                                        </table>
                                                                    </div>

                                                                </div>

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>
                                                                        Tax Withholding Information
                                                                    </h4>
                                                                </div>

                                                                <div class="card-body text-secondary mb-3" style="padding:0px !Important;">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="mb-2">
                                                                            <div class="row">
                                                                                <div class="col-12 text-sm-center form-inline">
                                                                                    <div class="form-group mr-2">
                                                                                        <a href="javascript:void(0);" id="demo-btn-Tax_Withhold" data-toggle="modal" data-target="#modal_Tax" class="btn btn-primary"><i class="mdi mdi-plus-circle mr-2"></i> Add Tax Withhold</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <table id="tax_information_table" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                            <thead>
                                                                            <tr>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Year</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:30% !important;">Filling Status</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">Fed. Claim</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">Fed. Add. Wh.</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">St. Claim</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">St. Add. Wh.</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">Local Claim</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">Local Add. Wh.</th>
                                                                                <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:8% !important;">Action</th>

                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>

                                                                            </tbody>

                                                                        </table>
                                                                    </div>

                                                                </div>

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>
                                                                        Company Benefit
                                                                    </h4>
                                                                </div>

                                                                <div class="card-body text-secondary mb-3" style="padding:0px !Important;">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <input type="checkbox" id="chk_None">
                                                                            <label for="None"> None </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Personal" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Personal-tab">
                                                            <div class="">
                                                                @if(isset($edit_employee->personal["id"]))
                                                                    <input type="hidden" name="personal_id" value="{{$edit_employee->personal["id"]}}">
                                                                @endif
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>Personal Information</h4>
                                                                </div>
                                                                <div class="card-body text-secondary">

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label for="Type">Gender </label>
                                                                                <select name="gender" id="gender" class="form-control fsc-input">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Male" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["gender"]=="Male" ? "Selected" : "" : ""}}>Male</option>
                                                                                    <option value="Female" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["gender"]=="Female" ? "Selected" : "" : ""}}>Female</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Marital Status</label>
                                                                                <select name="marital_staus" id="marital_staus" class="form-control fsc-input">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Married" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["marital_staus"]=="Married" ? "Selected" : "" : ""}}>Married</option>
                                                                                    <option value="UnMarried" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["marital_staus"]=="UnMarried" ? "Selected" : "" : ""}}>UnMarried</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label for="Status">Date of Birth</label>
                                                                                <div class="input-group">
                                                                                    <input type="text" id="Dateofbirth" value="{{isset($edit_employee->personal["id"]) ? $dateFormat->retriveDateInVal($edit_employee->personal["dob"]) : ""}}"
                                                                                           name="dob" class="form-control datepickers" placeholder="mm/dd/yyyy" data-provide="datepicker" data-date-format="mm/dd/yyyy">
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text"><i class="ri-calendar-event-fill"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Languages Known</label>
                                                                                <div class="row">
                                                                                    <input type="hidden" name="langauge_hidden" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["langauge"] : ""}}">
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="bangali" name="langauge[]" value="bangali">
                                                                                        <label for="bangali"> Bengali</label>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="english" name="langauge[]" value="english">
                                                                                        <label for="english">English </label>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="gujarati" name="langauge[]" value="gujarati">
                                                                                        <label for="gujarati">Gujarati </label>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="hindi" name="langauge[]" value="hindi">
                                                                                        <label for="hindi">Hindi </label>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="spanish" name="langauge[]" value="spanish">
                                                                                        <label for="spanish">Spanish </label>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input type="checkbox" id="urdu" name="langauge[]" value="urdu">
                                                                                        <label for="urdu">Urdu </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">ID Proof 1 </label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <select name="pf1" class="form-control" id="pf1">
                                                                                            <option value="">Select</option>
                                                                                            <option value="State ID" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1"]=="State ID" ? "Selected" : "" : ""}}>State ID</option>
                                                                                            <option value="Voter Id" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1"]=="Voter Id" ? "Selected" : "" : ""}}>Voter ID</option>
                                                                                            <option value="Driving Licence" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1"]=="Driving Licence" ? "Selected" : "" : ""}}>Driving Licence</option>
                                                                                            <option value="Pan Card" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1"]=="Pan Card" ? "Selected" : "" : ""}}>Pan Card</option>
                                                                                            <option value="Pass Port" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1"]=="Pass Port" ? "Selected" : "" : ""}}>Pass Port</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="fileupload btn btn-success waves-effect waves-light">
                                                                                            <span><i class="mdi mdi-cloud-upload mr-1"></i> Browse for file...</span>
                                                                                            <input type="file" class="upload" name="pf1_document" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1_document"] : ""}}">
                                                                                            <input type="hidden" name="pf1_document_2" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf1_document"] : ""}}">
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">ID Proof 2 </label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <select name="pf2" class="form-control" id="pf2">
                                                                                            <option value="">Select</option>
                                                                                            <option value="State ID" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2"]=="State ID" ? "Selected" : "" : ""}}>State ID</option>
                                                                                            <option value="Voter Id" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2"]=="Voter Id" ? "Selected" : "" : ""}}>Voter ID</option>
                                                                                            <option value="Driving Licence" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2"]=="Driving Licence" ? "Selected" : "" : ""}}>Driving Licence</option>
                                                                                            <option value="Pan Card" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2"]=="Pan Card" ? "Selected" : "" : ""}}>Pan Card</option>
                                                                                            <option value="Pass Port" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2"]=="Pass Port" ? "Selected" : "" : ""}}>Pass Port</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="fileupload btn btn-success waves-effect waves-light">
                                                                                            <span><i class="mdi mdi-cloud-upload mr-1"></i> Browse for file...</span>
                                                                                            <input type="file" class="upload" name="pf2_document" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2_document"] : ""}}">
                                                                                            <input type="hidden" name="pf2_document_2" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["pf2_document"] : ""}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Resume</label><br>
                                                                                <div class="fileupload btn btn-success waves-effect waves-light ">
                                                                                    <span><i class="mdi mdi-cloud-upload mr-1"></i> Browse for file...</span>
                                                                                    <input type="file" class="upload" name="resume_document" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["resume_document"] : ""}}">
                                                                                    <input type="hidden" class="upload" name="resume_document_2" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["resume_document"] : ""}}">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Type of Agreement</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <select name="type_of_agreement" class="form-control" id="type_agreement">
                                                                                            <option value="">Select</option>
                                                                                            <option value="Hiring Letter" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["type_of_agreement"]=="Hiring Letter" ? "Selected" : "" : ""}}>Hiring Letter</option>
                                                                                            <option value="Employment Agreement" {{isset($edit_employee->personal["id"]) ? $edit_employee->personal["type_of_agreement"]=="Employment Agreement" ? "Selected" : "" : ""}}>Employment Agreement</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="fileupload btn btn-success waves-effect waves-light">
                                                                                            <span><i class="mdi mdi-cloud-upload mr-1"></i> Browse for file...</span>
                                                                                            <input type="file" class="upload" name="agreement_document" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["agreement_document"] : ""}}">
                                                                                            <input type="hidden" name="agreement_document_2" value="{{isset($edit_employee->personal["id"]) ? $edit_employee->personal["agreement_document"] : ""}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    @if(isset($edit_employee->emergency["id"]))
                                                                        <input type="hidden" name="emergency_id" value="{{$edit_employee->emergency["id"]}}">
                                                                    @endif
                                                                    <h4>
                                                                        Emergency Contact Info
                                                                    </h4>
                                                                </div>
                                                                <div class="card-body text-secondary">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Contact Person Name </label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_fname"] : ""}}" name="emergency_fname" placeholder="First"/>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_mname"] : ""}}" name="emergency_mname" placeholder="M"/>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_lname"] : ""}}" name="emergency_lname" placeholder="Last"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Address 1</label>
                                                                                <input class="form-control" name="emergency_address_1" id="Type" placeholder="Address 1" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_address_1"] : ""}}"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Address 2</label>
                                                                                <input class="form-control" name="emergency_address_2" placeholder="Address 2" id="Type" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_address_2"] : ""}}"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">City / State / Zip</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control" name="emergency_city" placeholder="City" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_city"] : ""}}"/>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        @if(isset($edit_employee->emergency["id"]))
                                                                                            <x-state data="emergency_state" message="{{$edit_employee->emergency['emergency_state']}}"/>
                                                                                        @else
                                                                                            <x-state data="emergency_state" message=""/>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control input-usa-zip-mask" name="emergency_zip" placeholder="Zip" value="{{isset($edit_employee->emergency["id"]) ?$edit_employee->emergency["emergency_zip"]: ""}}"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Telephone 1</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control input-tel-mask" name="emergency_contact_1" placeholder="(999) 999-9999" value="{{isset($edit_employee->emergency["id"]) ?$edit_employee->emergency["emergency_contact_1"]: ""}}"/>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <select name="emergency_type_1" id="emergency_type_1" class="form-control">
                                                                                            <option value="Home" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Home" ? "Selected" : "" : ""}}>Home</option>
                                                                                            <option value="Mobile" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Mobile" ? "Selected" : "" : ""}}>Mobile</option>
                                                                                            <option value="Office" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Office" ? "Selected" : "" : ""}}>Office</option>
                                                                                            <option value="Other" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Other" ? "Selected" : "" : ""}}>Other</option>
                                                                                            <option value="Work" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Work" ? "Selected" : "" : ""}}>Work</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control input-extension-mask" name="emergency_ext_1" placeholder="Ext" value="{{isset($edit_employee->emergency["id"]) ?$edit_employee->emergency["emergency_ext_1"]: ""}}"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Telephone 2</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control input-tel-mask" name="emergency_contact_2" id="FSC_Telephone" placeholder="(999) 999-9999" value="{{isset($edit_employee->emergency["id"]) ?$edit_employee->emergency["emergency_contact_2"]: ""}}"/>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <select name="emergency_type_2" id="emergency_type_2" class="form-control fsc-input">
                                                                                            <option value="Home" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Home" ? "Selected" : "" : ""}}>Home</option>
                                                                                            <option value="Mobile" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Mobile" ? "Selected" : "" : ""}}>Mobile</option>
                                                                                            <option value="Office" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Office" ? "Selected" : "" : ""}}>Office</option>
                                                                                            <option value="Other" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Other" ? "Selected" : "" : ""}}>Other</option>
                                                                                            <option value="Work" {{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_type_1"]=="Work" ? "Selected" : "" : ""}}>Work</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control input-extension-mask" name="emergency_ext_2" placeholder="Ext" value="{{isset($edit_employee->emergency["id"]) ?$edit_employee->emergency["emergency_ext_2"]: ""}}"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">

                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label for="Type">Fax</label>
                                                                                <input class="form-control" name="emergency_fax" id="fax" placeholder="(999) 999-9999" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_fax"] : ""}}"/>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Email</label>
                                                                                <input class="form-control" name="emergency_email" placeholder="Email" readonly/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Relationship </label>
                                                                                <input class="form-control" name="emergency_relation" placeholder="Relationship" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_relation"] : ""}}"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Note For Emergency </label>
                                                                                <input class="form-control" name="emergency_note" placeholder="Note For Emergency" value="{{isset($edit_employee->emergency["id"]) ? $edit_employee->emergency["emergency_note"] : ""}}"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Security" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Security-tab">
                                                            <div class="">
                                                                @if(isset($edit_employee->security["id"]))
                                                                    <input type="hidden" name="security_id" value="{{$edit_employee->security["id"]}}">
                                                                @endif
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <h4>Security Information</h4>
                                                                </div>
                                                                <div class="card-body text-secondary">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="Type">User Name </label>
                                                                                <input type="text" name="username" value="{{$edit_employee->email}}" class="form-control" placeholder="User Name" readonly/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="Type">Password</label>
                                                                                <div class="input-group">
                                                                                    <input type="password" name="password" id="Pass" class="form-control" placeholder="Password" readonly>
                                                                                    <div class="input-group-append">
                                                                                        <a class="input-group-text"><i class="fa fa-eye"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Employee">Reset Days </label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <select name="reset_days" id="reset_days" class="form-control">
                                                                                            <option value="30" selected="">30</option>
                                                                                            <option value="60">60</option>
                                                                                            <option value="90">90</option>
                                                                                            <option value="120">120</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <input class="form-control" id="reset_date" name="reset_date" readonly="">
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Question 1</label>
                                                                                <select name="que_1" id="que_1" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="1" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_1"]==1 ? "selected" :"" :""}}>What was your favorite place to visit as a child?</option>
                                                                                    <option value="2" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_1"]==2 ? "selected" :"" :""}}>Who is your favorite actor, musician, or artist?</option>
                                                                                    <option value="3" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_1"]==3 ? "selected" :"" :""}}>What is the name of your favorite pet?</option>
                                                                                    <option value="4" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_1"]==4 ? "selected" :"" :""}}>In what city were you born?</option>
                                                                                    <option value="5" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_1"]==5 ? "selected" :"" :""}}>What is the name of your first school?</option>
                                                                                </select>
                                                                                <input type="hidden" id="question1" name="question1" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["question1"]:""}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Answer 1 </label>
                                                                                <input name="answer1" id="answer1" class="form-control" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["answer1"]:""}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Question 2</label>
                                                                                <select name="que_2" id="que_2" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="1" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_2"]==1 ? "selected" :"" :""}}>What was your favorite place to visit as a child?</option>
                                                                                    <option value="2" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_2"]==2 ? "selected" :"" :""}}>Who is your favorite actor, musician, or artist?</option>
                                                                                    <option value="3" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_2"]==3 ? "selected" :"" :""}}>What is the name of your favorite pet?</option>
                                                                                    <option value="4" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_2"]==4 ? "selected" :"" :""}}>In what city were you born?</option>
                                                                                    <option value="5" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_2"]==5 ? "selected" :"" :""}}>What is the name of your first school?</option>
                                                                                </select>
                                                                                <input type="hidden" id="question2" name="question2" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["question2"]:""}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Answer 2 </label>
                                                                                <input name="answer2" id="answer2" class="form-control" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["answer2"]:""}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Question 3</label>
                                                                                <select name="que_3" id="que_3" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="1" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_3"]==1 ? "selected" :"" :""}}>What was your favorite place to visit as a child?</option>
                                                                                    <option value="2" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_3"]==2 ? "selected" :"" :""}}>Who is your favorite actor, musician, or artist?</option>
                                                                                    <option value="3" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_3"]==3 ? "selected" :"" :""}}>What is the name of your favorite pet?</option>
                                                                                    <option value="4" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_3"]==4 ? "selected" :"" :""}}>In what city were you born?</option>
                                                                                    <option value="5" {{isset($edit_employee->security["id"]) ? $edit_employee->security["que_3"]==5 ? "selected" :"" :""}}>What is the name of your first school?</option>
                                                                                </select>
                                                                                <input type="hidden" id="question3" name="question3" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["question3"]:""}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Answer 3 </label>
                                                                                <input name="answer3" id="answer3" class="form-control" value="{{isset($edit_employee->security["id"]) ? $edit_employee->security["answer3"]:""}}">
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Rights" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Rights-tab">
                                                            <div class="card-body">

                                                                <ul class="nav nav-pills navtab-bg nav-justified">
                                                                    <li class="nav-item ">
                                                                        <a href="#ClientManagement" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                            <span class="d-none d-sm-inline-block">Client Management</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a href="#WorkManagement" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                            <span class="d-none d-sm-inline-block">Work Management</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a href="#EmployeeManagement" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                                                            <span class="d-none d-sm-inline-block">Employee Management</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a href="#Report" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-cog"></i></span>
                                                                            <span class="d-none d-sm-inline-block" style="padding:11px 0px;">Report</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a href="#TechnicalSupport" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-cog"></i></span>
                                                                            <span class="d-none d-sm-inline-block">Technical Support</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a href="#EmailAccess" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-cog"></i></span>
                                                                            <span class="d-none d-sm-inline-block">Email Access</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="ClientManagement">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="col-sm-3" style="text-align:right;">
                                                                                <label>Client Edit / View</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Add</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Edit</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> View</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Delete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane " id="WorkManagement">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="col-sm-3 align-right" style="text-align:right;">
                                                                                <label>Work New</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Add</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Edit</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> View</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Delete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="EmployeeManagement">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="col-sm-3 align-right" style="text-align:right;">
                                                                                <label>FSC EE Schedule</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Add</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Edit</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> View</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Delete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="Report">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="col-sm-3 align-right" style="text-align:right;">
                                                                                <label>Client Report</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Add</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Edit</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> View</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Delete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="TechnicalSupport">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="col-sm-3 align-right" style="text-align:right;">
                                                                                <label>Client Support</label>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Add</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Edit</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> View</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="checkbox">
                                                                                        <label> Delete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="EmailAccess">
                                                                        <div class="col-sm-12">
                                                                            <table id="demo-foo-addrow" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:30% !important;">Email</th>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:60% !important;">Access</th>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Action</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr style="" class="footable-even">
                                                                                    <td style="text-align: center;" colspan="6" class="footable-visible footable-first-column">
                                                                                        No records found
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Rules" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Rules-tab">
                                                            <div class="">

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <b>
                                                                        Employee Rules
                                                                    </b>
                                                                </div>
                                                                <div class="card-body text-secondary">

                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12 row">
                                                                            <div class="form-group">
                                                                                <label for="Type"> 01. Account Services Rule :</label>
                                                                                <div class="col-sm-12">Testing</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <input type="checkbox" id="chk_Rule1">
                                                                                <label class="x_small">I read and acknowledge the rules of Financial Service Center and I will follow as per company's rule.</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Responsiblity" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Responsiblity-tab">
                                                            <div class="">

                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <b>
                                                                        EE Responsibility
                                                                    </b>
                                                                </div>
                                                                <div class="card-body text-secondary">


                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Service </label>
                                                                                <input name="Service" id="Service" class="form-control" placeholder="Service">

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Taxation Service </label>
                                                                                <input name="Taxation" id="Taxation" class="form-control" placeholder="Taxation Service">

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <b>
                                                                        Checking (Supervisor)
                                                                    </b>
                                                                </div>
                                                                <div class="card-body text-secondary">
                                                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Service </label>
                                                                                <input name="Service" id="Service" class="form-control" placeholder="Service">

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label for="Type">Taxation Service </label>
                                                                                <input name="Taxation" id="Taxation" class="form-control" placeholder="Taxation Service">

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <b>
                                                                        Checking (Supervisor)
                                                                    </b>
                                                                </div>
                                                                <div class="card-body text-secondary">
                                                                    <div class="col-sm-12">
                                                                        <div class="col-sm-12">
                                                                            <table id="demo-foo-addrow" class="table table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">No.</th>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:60% !important;">Responsibility</th>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:20% !important;">Checked Date</th>
                                                                                    <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Checked</th>

                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr style="" class="footable-even">
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                </tbody>

                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Agreement" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Agreement-tab">
                                                            <div class="">
                                                                <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                    <b>
                                                                        Agreement
                                                                    </b>
                                                                </div>
                                                                <div class="card-body text-secondary">
                                                                    <div class="col-sm-12">

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-Other" role="tabpanel" style="display:none;" aria-labelledby="v-pills-Other-tab">
                                                            <div class="">
                                                                <div class="card-body">
                                                                    <ul class="nav nav-pills navtab-bg nav-justified">
                                                                        <li class="nav-item">
                                                                            <a href="#Conversation" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                                <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                                <span class="d-none d-sm-inline-block">Conversation Sheet</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a href="#Appointment" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                                <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                                <span class="d-none d-sm-inline-block">Appointment</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a href="#Notes" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                                                                <span class="d-none d-sm-inline-block">Notes</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane active" id="Conversation">
                                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                                <b>
                                                                                    Conversation Sheet
                                                                                </b>
                                                                            </div>
                                                                            <div class="card-body text-secondary">
                                                                                <div class="">
                                                                                    <table id="demo-foo-addrow" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">No.</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:22% !important;">Date Day Time</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Related To</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:18% !important;">Description</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:45% !important;">Note</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Action</th>

                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr style="" class="footable-even">
                                                                                            <td style="text-align: center;" colspan="6" class="footable-visible footable-first-column">
                                                                                                No records found
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane show" id="Appointment">
                                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                                <b>
                                                                                    Appointment
                                                                                </b>
                                                                            </div>
                                                                            <div class="card-body text-secondary">
                                                                                <div class="">
                                                                                    <table id="demo-foo-addrow" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">No.</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Priority</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Appt. Date</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Appt. Time</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Type</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Client ID</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Regards</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Appt. With</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Team Rep.</th>
                                                                                            <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:10% !important;">Action</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr style="" class="footable-even">
                                                                                            <td style="text-align: center;" colspan="10" class="footable-visible footable-first-column">
                                                                                                No records found
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id="Notes">
                                                                            <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                                <b>
                                                                                    Notes
                                                                                </b>
                                                                            </div>
                                                                            <div class="card-body text-secondary">
                                                                                <div class="col-sm-12 row">
                                                                                    <div class="col-sm-3" style="text-align:right;">
                                                                                        <label>Note 1</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="form-control">
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <button type="button" class="btn btn-secondary">Add</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <ul class="nav nav-pills navtab-bg nav-justified">
                                                                                    <li class="nav-item ">
                                                                                        <a href="#Temporary" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                                                                            <span class="d-none d-sm-inline-block">Temporary Note</span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="nav-item">
                                                                                        <a href="#Permenant" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                                            <span class="d-inline-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                                            <span class="d-none d-sm-inline-block">Permenant Note</span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                                <div class="tab-content">
                                                                                    <div class="tab-pane active" id="Temporary">
                                                                                        <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                                            <b>
                                                                                                Temporary Note
                                                                                            </b>
                                                                                        </div>
                                                                                        <div class="card-body text-secondary">
                                                                                            <div class="">
                                                                                                <table id="demo-foo-addrow" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                                    <thead>
                                                                                                    <tr>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">No.</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Date Day Time</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:20% !important;">Related To</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Description</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Notes</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Action</th>
                                                                                                    </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                    <tr style="" class="footable-even">
                                                                                                        <td style="text-align: center;" colspan="6" class="footable-visible footable-first-column">
                                                                                                            No records found
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="tab-pane show" id="Permenant">
                                                                                        <div class="alert alert-Fsc" role="alert" style="text-align:center;">
                                                                                            <b>
                                                                                                Permenant Note
                                                                                            </b>
                                                                                        </div>
                                                                                        <div class="card-body text-secondary">
                                                                                            <div class="">
                                                                                                <table id="demo-foo-addrow" class="table table_small table-centered table-striped table-bordered mb-0 toggle-circle tablet breakpoint footable-loaded footable no-paging" data-page-size="7">
                                                                                                    <thead>
                                                                                                    <tr>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">No.</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Date Day Time</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:20% !important;">Related To</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Description</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Notes</th>
                                                                                                        <th data-sort-ignore="true" class="min-width footable-visible footable-first-column" style="width:15% !important;">Action</th>
                                                                                                    </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                    <tr style="" class="footable-even">
                                                                                                        <td style="text-align: center;" colspan="6" class="footable-visible footable-first-column">
                                                                                                            No records found
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-body text-secondary">
                                                            <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                                        <button class="col-sm-2 btn btn-danger text-center" type="button">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div> <!-- end col-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')
    <div class="modal fade" id="modal_NickName" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Nickname</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Nickname">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary waves-effect waves-light" type="button">Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <table id="new-application-table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="new-application-table_info">
                                        <thead>
                                        <tr role="row">
                                        </tr>
                                        <tr role="row">
                                            <th class="sorting_disabled sorting_asc" rowspan="1" colspan="1" aria-label="No" style="width:10%;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Type" style="width:80%;">Nickname</th>
                                            <th class="sorting align-center" tabindex="0" rowspan="1" colspan="1" style="width:10%;">Action</th>
                                        </tr>

                                        </thead>

                                        <tbody>

                                        <tr class="odd">
                                            <td class="sorting_1">1</td>
                                            <td>Employee</td>
                                            <td style="text-align:center;">
                                                <a href="" id="" class=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group row mb-3" style="float:right;">
                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-danger text-center">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal_Add_Pay" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Add Pay Information</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" name="pay_form" id="pay_form">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="Type">Pay Method</label>
                                    <div class="col-md-3">
                                        <select class="form-control form-control" name="pay_method">
                                            <option value="">Select</option>
                                            <option value="Salary">Salary</option>
                                            <option value="Hourly">Hourly</option>
                                        </select>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="name">Pay Duration</label>
                                    <div class="col-md-3">
                                        <select class="form-control form-control" name="pay_frequency">
                                            <option value="">Select</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Bi-Weekly">Bi-Weekly</option>
                                            <option value="Semi-Monthly">Semi-Monthly</option>
                                            <option value="Monthly">Monthly</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="Type">Pay Rate</label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="pay_rate" class="form-control only-numbers" id="pay_rate" required="">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="name">Effective Date</label>
                                    <div class="col-md-3">
                                        <input type="text" name="effective_date" placeholder="mm/dd/yyyy" class="form-control datepickers">
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="Type">Note</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="notes" id="notes" placeholder="Note">
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <a href="javascript:void(0);" data-src="{{route('ajax.storePay')}}" id="pay_form_submit" class="col-md-2 btn btn-primary text-center">Submit</a>
                                            <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_Review_Pay" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Add Review Information</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" id="review_form">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user_id}}">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="Type">Review Date</label>
                                    <div class="col-md-3">
                                        <input type="text" name="review_date" placeholder="mm/dd/yyyy" class="form-control datepickers">
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="Type">Next Review</label>
                                    <div class="col-md-3">
                                        <input type="text" name="next_review" id="next_review" placeholder="Reviews" class="form-control only-numbers" onblur="">
                                    </div>

                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="name">Next Review Date</label>
                                    <div class="col-md-3">
                                        <input type="text" name="next_review_date" id="next_review_date" class="form-control" readonly>
                                    </div>
                                    <label class="col-md-3 col-form-label text-right" for="name">Next Pay Rate</label>
                                    <div class="col-md-3 input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar-sign"></i></span>
                                        </div>
                                        <input type="text" name="next_pay_rate" id="next_pay_rate" class="form-control only-numbers">
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="Type">Comment</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="comments" placeholder="Write a comments ..." class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row mb-3 text-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <a href="javascript:void(0);" data-src="{{route('ajax.storeReview')}}" id="review_submit" class="col-md-2 btn btn-primary text-center">Submit</a>
                                            <a class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_Tax" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Tax Withholding Information</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Type">Year</label>
                                                <select class="form-control" name="year_wh[]">
                                                    <option value="">Select</option>
                                                    <option value="2021">2021</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="Employee">Filling Status</label>
                                                <select name="fillingstatus[]" id="fillingstatus" class="form-control tttt">
                                                    <option value="Single" selected="">Single</option>
                                                    <option value="Married File Jointly">Married File Jointly</option>
                                                    <option value="Married File Separately">Married File Separately</option>
                                                    <option value="Head of Household">Head of Household</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Status">Fed. Claim</label>
                                                <input type="text" name="effective_date[]" placeholder="Fed. Claim" class="form-control form-control">

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Status">Fed. Add. Wh.</label>
                                                <input class="form-control" id="Employee_ID" placeholder="Fed. Add. Wh.">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Type">St. Claim</label>
                                                <input type="text" placeholder="St. Claim" class="form-control form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Employee">St. Add. Wh.</label>
                                                <input class="form-control" id="Employee_ID" placeholder="St. Add. Wh.">

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Status">Local Claim</label>
                                                <input type="text" placeholder="Local Claim" class="form-control form-control">

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Status">Local Add. Wh.</label>
                                                <input class="form-control" id="Employee_ID" placeholder="Local Add. Wh">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-3" style="text-align:center;">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                            <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection
@section('scripts')
    <script src="{{asset('Backend/js/custom pages/employee/edit-employee.js')}}"></script>
@endsection
