@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Admin Profile</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin/Profile</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="header-title text-center mb-4">New Employee / User</h3>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <div><i class="icon-warning-sign"></i> {{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col col-md-10">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="{{route('employee.store')}}" method="POST" id="create-employee" name="create_employee">
                                            @csrf
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right">Role</label>
                                                <div class="custom-control custom-radio col-md-3 mt-1 col-span-4">
                                                    <input type="radio" id="employee" name="user_type" value="1" class="custom-control-input mt-1" checked="">
                                                    <label class="custom-control-label" for="employee">Employee</label>
                                                </div>
                                                <div class="custom-control custom-radio mt-1 col-md-3">
                                                    <input type="radio" id="User" name="user_type" value="2" class="custom-control-input mt-1">
                                                    <label class="custom-control-label" for="User">User</label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="employee_id">Employee ID / Represent <span class="text-danger">*</span></label>
                                                <div class="col-md-3">
                                                    <input type="text" name="user_code" placeholder="ABC-999-9999" class="form-control " id="employee_id">
                                                </div>
                                                <div class="col-md-5">
                                                    <select name="represent" id="represent" class="form-control js-select-2">
                                                        <option value=""> Select Represent</option>
                                                        @if(isset($representList))
                                                            @foreach($representList as $key=>$represents)
                                                                <option value="{{$represents->id}}">{{$represents->represent_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <a class="text-warning" href="javascript:void(0);" data-toggle="modal" data-target="#modal-represent-name"><i class="ri ri-2x ri-add-box-line btn-warning"></i> </a>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_city">Name <span class="text-danger">*</span></label>
                                                <div class="col-md-2">
                                                    <select name="prefix_name" id="prefix_name" class="form-control">
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Miss">Miss.</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="name" name="fname">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="name" name="mname">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" id="reg_city" name="lname">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address1 <span class="text-danger">*</span></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_1">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="reg_address">Address2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="reg_address" name="address_2">
                                                </div>
                                            </div>

                                            <div class="form-group row mb-3">
                                                @php $name=[
                                                                'city_name'=>'city',
                                                                'zip_name'=>'zip',
                                                                'state_name'=>'state'
                                                                ];
                                                @endphp
                                                <x-CityStateZip data="data" message="GA" :name="$name"/>
                                            </div>
                                            <div class="form-group row mb-3">
                                                @php $name=[
                                                                'label_name'=>'Telephone #.',
                                                                'type_name'=>'contact_no_type_1',
                                                                'contact_name'=>'contact_no_1',
                                                                'extnsion_name'=>'contact_no_extension_1',
                                                                ];
                                                @endphp
                                                <x-telephone :name="$name" val="values"/>
                                            </div>

                                            <div class="form-group row mb-3">
                                                @php $name=[
                                                                'label_name'=>'Telephone 2 #.',
                                                                'type_name'=>'contact_no_type_2',
                                                                'contact_name'=>'contact_no_2',
                                                                'extnsion_name'=>'contact_no_extension_2',
                                                                ];
                                                @endphp
                                                <x-telephone :name="$name" val="values"/>
                                            </div>

                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="email" name="email">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label class="col-md-3 col-form-label text-right" for="profile">Profile</label>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" id="profile" name="profile">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3 text-center">
                                                <div class="col-md-6">
                                                    <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{route('employee.employees')}}" type="reset" class=" col-md-5 btn btn-danger text-center">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')
    <!--  Modal content for the Large example -->
    <div class="modal fade" id="modal-represent-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title text-center" id="myLargeModalLabel">Represent Name</h4>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="represent_name">Represent name</label>
                                    <div class="col-md-9">
                                        <input type="text" name="represent_name" class="form-control" id="represent_name">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-md-12 text-right">
                                        <a class="btn btn-primary" href="javascript:void(0);" id="create_represent_name" data-src="{{route('admin.storeRepresent')}}" data-token="{{csrf_token()}}">Submit</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xl-12 col-sm-12">
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-bordered table-striped" id="represent_name_table">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Represent Name</td>
                                        <td>Action</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($representList))
                                        @foreach($representList as $key=>$represents)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$represents->represent_name}}</td>
                                                <td>
                                                    <x-actionlink class="" id="" route="" buttons="delete"/>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/component.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script src="{{asset('Backend/js/custom pages/employee.js')}}"></script>
@endsection
