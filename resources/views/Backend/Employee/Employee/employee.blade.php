@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee / User</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Employee</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Employees</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="{{route('employee.create')}}" class="btn btn-warning waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>New EE / User
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-application-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Type</th>
                                                <th>EE / User ID</th>
                                                <th>Employee Name</th>
                                                <th>Email ID</th>
                                                <th>Telephone</th>
                                                <th>Status</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($employeeList))
                                                @foreach($employeeList as $key=>$employee)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$employee->user_type==1 ? "Employee" : "User"}}</td>
                                                        <td>{{$employee->user_code}}</td>
                                                        <td>{{$employee->fname ." " .$employee->mname ." ".$employee->lname}}</td>
                                                        <td>{{$employee->email}}</td>
                                                        <td>{{$employee->contact_no_1}}</td>
                                                        <td><x-status type="{{$employee->status}}"/></td>
                                                        <td>
                                                            <x-action-link-w-o-model
                                                                buttons="edit"
                                                                route="{{route('employee.edit',$employee->id)}}"
                                                            />
                                                            <x-action-link-w-o-model
                                                                buttons="delete"
                                                                route=""
                                                            />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/employee/employee.js')}}"></script>
@endsection
