@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Employee / User</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Employee / Supervisor Code.</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Supervisor</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-new-supervisor">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Supervisor Code
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-supervisor-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                            <tr>
                                                <th>No.</th>
                                                <th>Supervisor Name</th>
                                                <th>Supervisor Code</th>
                                                <th>Action</th>
                                            </tr>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Ankit Patel</td>
                                                <td>JKSOFT@2</td>
                                                <td>
                                                    <a href=""><i class="ri f-1-4x ri-edit-box-line text-primary"></i></a>
                                                    <a href=""><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <!-- Upload New Docs  -->
    <!-- start -->
    <div class="modal fade" id="modal-new-supervisor" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">New Hiring</h4>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-form-label col-md-3 text-right">Supervisor Name</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="supervisor_name" id="supervisor_name">
                                            <option value="">Select</option>
                                            <option value="FSC-EMP-001">Ankit Patel</option>
                                            <option value="CEO-001">Vijay Bombaywala</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right" for="supervisor_code">Supervisor Code</label>
                                    <div class="col-md-9">
                                        <input type="text" name="supervisor_code" class="form-control" id="supervisor_code">
                                    </div>
                                </div>
                                <div class="form-group row mb-3 text-center">
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End -->

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/employee.js')}}"></script>
@endsection
