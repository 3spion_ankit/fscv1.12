@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup / Federal Tax Authority</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List of Federal Tax Authorities</h4>
                        <div class="row">
                            <div class="col-12">
                                @if (session()->has('FederalSuccess') )<br><br>
                                <div class="alert alert-success alert-dismissable systemGenMessage col-md-12 text-center ">
                                    <h4>{{ session()->get('FederalSuccess') }} </h4>
                                </div>
                                @endif
                                @if (session()->has('FederalErrors'))<br><br>
                                <div class="alert alert-danger systemGenMessage col-md-12 text-center">
                                    <h4>{{ session()->get('FederalErrors') }}</h4>
                                </div>
                                @endif
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="{{route('setup.create-tax-form-federal')}}" class="btn btn-warning waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add New
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-application-table" class="table table-bordered">
                                            <thead>
                                            <tr class="text-center">
                                                <th>#</th>
                                                <th>Type of Entity</th>
                                                <th>Forms Name</th>
                                                <th>State Name</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($federalTaxAuthorityList))
                                                @foreach($federalTaxAuthorityList as $key=>$federalTaxAuthority)
                                                    <tr class="text-center">
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$federalTaxAuthority->typeOfEntity['type_of_entity']}}</td>
                                                        <td>{{$federalTaxAuthority->type_of_form}}</td>
                                                        <td>{{$federalTaxAuthority->form_name}}</td>
                                                        <td>{{$federalTaxAuthority->due_date}}</td>
                                                        <td>{{$federalTaxAuthority->extension_due_date}}</td>
                                                        <td>
                                                            <x-action-link-w-o-model class="" route="{{route('setup.federalEdit',$federalTaxAuthority->id)}}" id="" buttons="edit" />
                                                            <x-action-link-w-o-model class="" route="" id="" buttons="delete" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>


                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/employee/employee.js')}}"></script>
@endsection
