@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup/Type Of Authority</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12 padding_off">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                            <h4>County --- Taxation / License</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active show mb-1" id="v-pills-County1" data-toggle="pill" href="#v-pills-County" role="tab" aria-controls="v-pills-County" aria-selected="false">
                                        County Information</a>
                                    <a class="nav-link mb-1" id="v-pills-Sales1" data-toggle="pill" href="#v-pills-Sales" role="tab" aria-controls="v-pills-Sales" aria-selected="false">
                                        Sales Tax Information</a>
                                    <a class="nav-link mb-1" id="v-pills-Personal1" data-toggle="pill" href="#v-pills-Personal" role="tab" aria-controls="v-pills-Personal" aria-selected="false">
                                        Personal Property Tax Information</a>
                                    <a class="nav-link mb-1" id="v-pills-Property1" data-toggle="pill" href="#v-pills-Property" role="tab" aria-controls="v-pills-Property" aria-selected="true">
                                        Property Tax Information</a>
                                    <a class="nav-link mb-1" id="v-pills-Business1" data-toggle="pill" href="#v-pills-Business" role="tab" aria-controls="v-pills-Business" aria-selected="true">
                                        Business Licence Information</a>
                                </div>
                            </div> <!-- end col-->
                            <div class="col-sm-10">
                                <form action="{{route('setup.storeCounty')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="county_id" value="{{$taxCountyList->id}}">
                                    <div class="tab-content pt-0">
                                        <div class="tab-pane fade active show" id="v-pills-County" role="tabpanel" aria-labelledby="v-pills-County">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                                                <h4> County Information </h4>
                                                            </div>

                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label text-right" for="userName3">State</label>
                                                                <div class="col-md-4">
                                                                    <x-state data="state_county_first" message="{{$taxCountyList->county_state}}"/>
                                                                    {{-- @else --}}
                                                                    {{--  <x-state data="state_county_first" message="GA"/> --}}
                                                                    {{-- @endif--}}
                                                                </div>

                                                                <label class="col-md-2 col-form-label text-right">County Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="county_name" id="county" class="form-control fsc-input">
                                                                        <option value="">--Select--</option>
                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label text-right">County Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="county_code" id="countycode" value="{{$taxCountyList->county_code}}" class="form-control" readonly="">
                                                                </div>
                                                                <label class="col-md-2 col-form-label text-right">Authority Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="county_authority_name"  id="county_authority_name" class="form-control"  value="{{$taxCountyList->county_authority_name}}" placeholder="County Authority Name">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">

                                                                <label class="col-md-2 col-form-label text-right" for="userName3">Website</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="County Website" id="county_website" name="county_website" value="{{$taxCountyList->county_website}}">
                                                                </div>
                                                                <label class="col-md-2 col-form-label text-right" for="userName3">Telephone</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="County Telephone" id="county_telephone" name="county_telephone" value="{{$taxCountyList->county_telephone}}">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label text-right">Address</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" placeholder="County Address" id="county_address" name="county_address"  value="{{$taxCountyList->county_address}}">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label text-right">City</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="City" id="county_city" name="county_city"  value="{{$taxCountyList->county_city}}">
                                                                </div>
                                                                <label class="col-md-2 col-form-label text-right">State</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="State" id="county_state" name="county_state" value="{{$taxCountyList->county_state}}" readonly="">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label text-right">Zip Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="ZIP Code" id="county_zip" name="county_zip" value="{{$taxCountyList->county_zip}}">
                                                                </div>
                                                                <label class="col-md-2 col-form-label text-right" >FAX</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Fax" id="county_fax" name="county_fax" value="{{$taxCountyList->county_fax}}">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-Sales" role="tabpanel" aria-labelledby="v-pills-Sales">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                                                <h4>Sales Tax Information</h4>
                                                            </div>
                                                            <table id="corporation-renewal-table" class="table table-responsive table-bordered table-hover table-centered">
                                                                <thead>
                                                                <tr class="text-center">
                                                                    <th>Month</th>
                                                                    <th>Year</th>
                                                                    <th>State Sales Tax Rate <span style="font-size: 14px; padding:0px;visibility:hidden;"> + </span> County Sales Tax Rate</th>
                                                                    <th>County Sales Rate Type</th>
                                                                    <th><a class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></th>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <select name="county_tax_month[]" id="county_tax_month_0" class="form-control-sm fsc-input" style="width:80px;margin:auto">
                                                                            <option value="">Select month</option>
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <select name="county_tax_year[]" id="county_tax_year_0" class="form-control-sm fsc-input" style="width:80px;margin:auto;">
                                                                            <option value="">Select year</option>
                                                                            <option value="2020">2020</option>
                                                                            <option value="2021">2021</option>
                                                                            <option value="2022">2022</option>
                                                                            <option value="2023">2023</option>
                                                                            <option value="2024">2024</option>
                                                                            <option value="2025">2025</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <div class="" style="width:100%;display: inline-flex;">
                                                                            <input type="text" name="county_tax_personal_rate[]" style="width:70px;" value="4.00%" id="county_tax_personal_rate_0" class="form-control-sm txtinput_1" placeholder="Rate">
                                                                            <h3 style="margin:0px;"> + </h3>
                                                                            <input type="text" name="" value="" id="" style="width:70px;" class="col-sm- form-control-sm txtinput_1" placeholder="county">
                                                                        </div>
                                                                    </td>
                                                                    <td style="padding-top:0px!important; padding-right:0px!important;">
                                                                        <div class="salestaxckbox">
                                                                            <table style="margin:auto;" class="tbl_Country">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span>M</span>
                                                                                        <input type="checkbox" class="county_marta_0" name="county_marta[]" id="county_marta_0" value="1">
                                                                                        <label for="county_marta_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="MARTA"></label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span>L</span>
                                                                                        <input type="checkbox" class="county_lost_0" name="county_lost[0]" id="county_lost_0" value="1" checked="">
                                                                                        <label for="county_lost_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lost"> </label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span>E</span>
                                                                                        <input type="checkbox" class="county_educational_0" name="county_educational[0]" id="county_educational_0" value="1" checked="">
                                                                                        <label for="county_educational_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Educational"></label>
                                                                                    </td>

                                                                                    <td>
                                                                                        <span>H</span>
                                                                                        <input type="checkbox" class="county_host_0" name="county_host[]" id="county_host_0" value="1">
                                                                                        <label for="county_host_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="HOST or EHOST"></label>
                                                                                    </td>

                                                                                    <td>
                                                                                        <span>S</span>
                                                                                        <input type="checkbox" class="county_splost_0" name="county_splost[0]" id="county_splost_0" value="1">
                                                                                        <label for="county_splost_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="SPLOST"></label>
                                                                                    </td>

                                                                                    <td>
                                                                                        <span>O</span>
                                                                                        <input type="checkbox" class="county_other_0" name="county_other[]" id="county_other_0" value="1">
                                                                                        <label for="county_other_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Other"></label>
                                                                                    </td>

                                                                                    <td>
                                                                                        <span>T</span>
                                                                                        <input type="checkbox" class="county_tsplost_0" name="county_tsplost[]" id="county_tsplost_0" value="1">
                                                                                        <label for="county_tsplost_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="TSPLOST 1"></label>
                                                                                    </td>


                                                                                    <td>
                                                                                        <span>T2</span>
                                                                                        <input type="checkbox" class="county_tsplost20" name="county_tsplost2[]" id="county_tsplost2_0" value="1">
                                                                                        <label for="county_tsplost2_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="TSPLOST 2">&nbsp;</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span>m</span>

                                                                                        <input type="checkbox" class="county_m" name="county_m" id="county_m" value="1" checked="">
                                                                                        <label for="county_m" data-toggle="tooltip" data-placement="top" title="" data-original-title="Local MARTA (Atlanta MARTA)">&nbsp;</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span>Ta</span>
                                                                                        <input type="checkbox" class="county_atlantata_0" name="county_atlantata[]" id="county_atlantata_0" value="1">
                                                                                        <label for="county_atlantata_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Atlanta TSPLOST">&nbsp;</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span>Tf</span>
                                                                                        <input type="checkbox" class="county_filtontf_0" name="county_filtontf[]" id="county_filtontf_0" value="1">
                                                                                        <label for="county_filtontf_0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fulton TSPLOST">&nbsp;</label>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td style="text-align:center;">
                                                                        <a class="btn btn-danger btn-sm"><i class="fa fa-minus"></i></a>
                                                                    </td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-Personal" role="tabpanel" aria-labelledby="v-pills-Personal">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                                                <h4> Personal Property Tax Information </h4>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label" for="userName3">Form No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form No." id="property_form_number" name="property_form_number" value="">
                                                                </div>
                                                                <label class="col-md-2 col-form-label align-right"for="userName3">Form Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form Name" id="property_form_name" name="property_form_name" value="" minlength="2" maxlength="50">
                                                                </div>

                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label" for="userName3">Tax Year</label>
                                                                <div class="col-md-4">
                                                                    <select name="personal_tax_year" id="personal_tax_year" class="form-control fsc-input">
                                                                        <option value="">Select year</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2021">2021</option>
                                                                        <option value="2022">2022</option>
                                                                        <option value="2023">2023</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2025">2025</option>
                                                                    </select>
                                                                </div>

                                                                <label class="col-md-3 col-form-label" for="userName3">Filling Frequency</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" name="property_filling_frequency" id="property_filling_frequency">
                                                                        <option value="">Select</option>
                                                                        <option value="Annually">Annually</option>
                                                                        <option value="Quarterly">Quarterly</option>
                                                                        <option value="Monthly">Monthly</option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Due Date</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control datepickers" id="property_due_date" name="property_due_date" value="" placeholder="mm-dd-yyyy" readonly="">
                                                                </div>

                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Telephone</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Telephone" id="personal_telephone" name="personal_telephone" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Person Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Person Name" id="personal_contact_name" name="personal_contact_name" value="" minlength="2" maxlength="50">
                                                                </div>

                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Contact No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Telephone" id="personal_secondary_telephone" name="personal_secondary_telephone" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Email</label>
                                                                <div class="col-md-10">
                                                                    <input type="email" class="form-control" placeholder="Email" name="personal_email" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Website</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">File Online</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control" name="personal_file">
                                                                        <option value="">---Select---</option>
                                                                        <option value="Yes" selected="">Yes</option>
                                                                        <option value="No">No</option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Payment Online</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" name="personal_payment_online">
                                                                        <option value="">---Select---</option>
                                                                        <option value="Yes" selected="">Yes</option>
                                                                        <option value="No">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Note</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" name="personal_tax_note" id="personal_tax_note" value="" class="form-control" placeholder="Note">
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade " id="v-pills-Property" role="tabpanel" aria-labelledby="v-pills-Property">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                                                <h4> Property Tax Information </h4>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label" for="userName3">Form No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form No." id="property_form_number" name="property_form_number" value="">
                                                                </div>
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Form Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form Name" id="property_form_name" name="property_form_name" value="" minlength="2" maxlength="50">
                                                                </div>

                                                            </div>
                                                            <div class="form-group row mb-3">

                                                                <label class="col-md-2 col-form-label" for="userName3">Filling Frequency</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control" name="property_filling_frequency" id="property_filling_frequency">
                                                                        <option value="">Select</option>
                                                                        <option value="Annually">Annually</option>
                                                                        <option value="Quarterly">Quarterly</option>
                                                                        <option value="Monthly">Monthly</option>

                                                                    </select></div>
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Due Date</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control datepickers" id="property_due_date" name="property_due_date" value="Jan-01-2021" placeholder="mm-dd-yyyy" readonly="">

                                                                </div>

                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Telephone</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Telephone" id="property_telephone" name="property_telephone" value="">
                                                                </div>
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Person Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Person Name" id="property_contact_name" name="property_contact_name" value="" minlength="2" maxlength="50">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Contact No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Telephone" id="property_secondary_telephone" name="property_secondary_telephone" value="">
                                                                </div>

                                                                <label class="col-md-2 col-form-label align-right" for="userName3">File Online</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control" name="property_file">
                                                                        <option value="">---Select---</option>
                                                                        <option value="Yes">Yes</option>
                                                                        <option value="No">No</option>
                                                                    </select>
                                                                </div>

                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Email</label>
                                                                <div class="col-md-10">
                                                                    <input type="email" class="form-control" placeholder="Email" name="property_email" value="">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Website</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" value="">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Note</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" name="personal_tax_note" id="personal_tax_note" value="" class="form-control" placeholder="Note">
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="v-pills-Business" role="tabpanel" aria-labelledby="v-pills-Business">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                                                <h4> Business Licence Information </h4>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label" for="userName3">Form No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form No." id="property_form_number" name="property_form_number" value="">
                                                                </div>
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Form Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Form Name" id="property_form_name" name="property_form_name" value="" minlength="2" maxlength="50">
                                                                </div>

                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label" for="userName3">Tax Year</label>
                                                                <div class="col-md-4">
                                                                    <select name="personal_tax_year" id="personal_tax_year" class="form-control fsc-input">
                                                                        <option value="">Select year</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2021">2021</option>
                                                                        <option value="2022">2022</option>
                                                                        <option value="2023">2023</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2025">2025</option>
                                                                    </select>
                                                                </div>

                                                                <label class="col-md-3 col-form-label" for="userName3">Filling Frequency</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" name="property_filling_frequency" id="property_filling_frequency">
                                                                        <option value="">Select</option>
                                                                        <option value="Annually">Annually</option>
                                                                        <option value="Quarterly">Quarterly</option>
                                                                        <option value="Monthly">Monthly</option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Due Date</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control datepickers" id="property_due_date" name="property_due_date" value="" placeholder="mm-dd-yyyy" readonly="">
                                                                </div>

                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Telephone</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Telephone" id="personal_telephone" name="personal_telephone" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Person Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Person Name" id="personal_contact_name" name="personal_contact_name" value="" minlength="2" maxlength="50">
                                                                </div>

                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Contact No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" placeholder="Contact Telephone" id="personal_secondary_telephone" name="personal_secondary_telephone" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right"  for="userName3">Email</label>
                                                                <div class="col-md-10">
                                                                    <input type="email" class="form-control" placeholder="Email" name="personal_email" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Website</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" value="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right"  for="userName3">File Online</label>
                                                                <div class="col-md-4">
                                                                    <select class="form-control" name="personal_file">
                                                                        <option value="">---Select---</option>
                                                                        <option value="Yes" selected="">Yes</option>
                                                                        <option value="No">No</option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-3 col-form-label align-right" for="userName3">Payment Online</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" name="personal_payment_online">
                                                                        <option value="">---Select---</option>
                                                                        <option value="Yes" selected="">Yes</option>
                                                                        <option value="No">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">


                                                                <label class="col-md-2 col-form-label align-right" for="userName3">Note</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" name="personal_tax_note" id="personal_tax_note" value="" class="form-control" placeholder="Note">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-3" style="text-align:center;">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                                    <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div> <!-- end col-->
                        </div>

                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection

@section('customModels')
    <div class="modal fade" id="modal_Add_Note" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Add Note</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="Type">Note</label>
                                                <input class="form-control" id="Note_ID" placeholder="Note">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-3" style="text-align:center;">
                                    <div class="col-sm-12">
                                        <div class="">
                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                            <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal_Taxation_Service" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Taxation Service</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-12 row" style="margin-left:0px !Important;">
                                        <div class="col-sm-12 row ">
                                            <div class="col-sm-5">
                                                <h6 for="Type">Taxation Service </h6>
                                                <select class="form-control-sm col-sm-12" name="taxation_service[]">
                                                    <option value="">Select</option>
                                                    <option value="4">941 Reporting
                                                        Service
                                                    </option>
                                                    <option value="6">Business Income
                                                        Tax Return
                                                    </option>
                                                    <option value="3">COAM Reporting
                                                        Service
                                                    </option>
                                                    <option value="8">Estimate Advance
                                                        Income Tax
                                                    </option>
                                                    <option value="7">Individual Income
                                                        Tax Return
                                                    </option>
                                                    <option value="9">Personal Property
                                                        Tax
                                                    </option>
                                                    <option value="1">Sales Tax
                                                        Reporting Service
                                                    </option>
                                                    <option value="2">Tobacco (Excise)
                                                        Tax Rep.
                                                    </option>
                                                    <option value="5">W-2 Reporting
                                                        Service
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <h6 for="Type">Period</h6>
                                                <select class="form-control-sm col-sm-12 tax_period_1" name="taxation_service_period1">
                                                    <option value="">Select</option>
                                                    <option value="Monthly">Monthly
                                                    </option>
                                                    <option value="Quarterly">
                                                        Quarterly
                                                    </option>
                                                    <option value="Annually">Annually
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <h6 for="Type">Month</h6>
                                                <select class="form-control-sm col-sm-12" name="tax_service_month">
                                                    <option value=""> Select</option>
                                                    <option class="qtr1" value="Jan">
                                                        Jan
                                                    </option>
                                                    <option class="qtr1" value="Feb">
                                                        Feb
                                                    </option>
                                                    <option value="Mar">Mar</option>
                                                    <option class="qtr1" value="Apr">
                                                        Apr
                                                    </option>
                                                    <option class="qtr1" value="May">
                                                        May
                                                    </option>
                                                    <option value="Jun">Jun</option>
                                                    <option class="qtr1" value="Jul">
                                                        Jul
                                                    </option>
                                                    <option class="qtr1" value="Aug">
                                                        Aug
                                                    </option>
                                                    <option value="Sep">Sep</option>
                                                    <option class="qtr1" value="Oct">
                                                        Oct
                                                    </option>
                                                    <option class="qtr1" value="Nov">
                                                        Nov
                                                    </option>
                                                    <option value="Dec">Dec</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <h6 for="Type">Year</h6>
                                                <select class="form-control-sm col-sm-12" name="tax_service_year">
                                                    <option value="">Select</option>
                                                    <option value="2019">2019</option>
                                                    <option value="2020">2020</option>
                                                    <option value="2021">2021</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-3" style="text-align:center;">
                                    <div class="col-sm-12">
                                        <div class="">
                                            <button type="submit" class="col-md-2 btn btn-primary text-center">Submit</button>
                                            <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            const mainLibs = {
                ajaxCalling: function (url, data, method, callback) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: url,
                        method: method,
                        data: data,
                        success: callback,
                        error: function (error) {
                            //returnval = error;
                        }
                    });
                }
            };


            $('#state_county_first').change(function () {
                const val = $(this).val();
                const _token = $('meta[name="csrf-token"]').attr('content');
                const url = "{{route('ajax.getCountyByState')}}";
                const data = {
                    "_token": _token,
                    "state_code": val
                };
                mainLibs.ajaxCalling(url, data, "POST", function (response) {
                    //console.log(response);
                    var jsonData = JSON.parse(response);
                    $('#county').empty();
                    var output = "<option value=''> Select </option>";
                    for (var i = 0; i < jsonData.length; i++) {
                        output += "<option value='" + jsonData[i]['county_name'] + "'>" + jsonData[i]['county_name'] + " </option>";
                    }
                    $('#county').append(output);
                    $('#county').select2();
                });
            });

            $('#county').change(function () {
                const val = $(this).val();
                const _token = $('meta[name="csrf-token"]').attr('content');
                const url = "{{route('ajax.getCountyCode')}}";
                const data = {
                    "_token": _token,
                    "county_name": val
                };
                mainLibs.ajaxCalling(url, data, "POST", function (response) {
                    //console.log(response);
                    $('#countycode').val(response);
                });
            });


        });
    </script>
    <script src="{{asset('Backend/js/custom pages/admin_profile.js')}}"></script>
@endsection
