@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup/Type Of Authority</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12 padding_off">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                            <h4>Federal Tax Authorities / Form</h4>
                        </div>
                        <form action="{{route('setup.storeFederalTadAuthority')}}" id="federal_form" method="POST">
                            @csrf
                            <input type="hidden" name="federal_authority_id" value="{{$federalTaxAuthority->id}}">
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="typeofform">Type of Tax :</label>
                                <div class="col-md-5">
                                    <select type="text" class="form-control fsc-input" name="type_of_form" id="type_of_form">
                                        <option value="">---Select---</option>
                                        <option value="Income Tax" {{$federalTaxAuthority->type_of_form == "Income Tax"?"selected":""}}>Income Tax</option>
                                        <option value="Payroll Tax" {{$federalTaxAuthority->type_of_form == "Payroll Tax"?"selected":""}}>Payroll Tax</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="authority_name">Type of Entity :</label>
                                <div class="col-md-5">
                                    <select class="form-control fsc-input" name="type_of_entity_id" id="type_of_entity_id" placeholder="Enter Your Company Name">
                                        <option value="">---Select---</option>
                                        @if(isset($typeOfEntityList))
                                            @foreach($typeOfEntityList as $key=>$typeOfEntity)
                                                <option value="{{$typeOfEntity->id}}"  {{$federalTaxAuthority->type_of_entity_id == $typeOfEntity->id?"selected":""}}>{{$typeOfEntity->type_of_entity}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a class="btn btn-primary" data-toggle="modal" data-target="#modal_TypeofEntity"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="formname">Form Name :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->form_name}}" name="form_name" id="form_name" class="form-control" placeholder="Form Name">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="formname">State :</label>
                                <div class="col-md-1">
                                    <x-state data="authority_state" message="{{$federalTaxAuthority->authority_state}}"/>
                                </div>
                                <label class="col-md-2 col-form-label text-right" for="formname">State Form Name :</label>
                                <div class="col-md-2">
                                    <input type="text" value="{{$federalTaxAuthority->state_form_name}}" name="state_form_name" id="state_form_name" class="form-control" placeholder="Form Name">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="filling_frequency">Filing Frequency :</label>
                                <div class="col-md-5">
                                    <select type="text" class="form-control fsc-input" name="filling_frequency" id="filling_frequency">
                                        <option value="">---Select---</option>
                                        <option value="Annually" {{$federalTaxAuthority->filling_frequency == "Annually"?"selected":""}}>Annually</option>
                                        <option value="Monthly" {{$federalTaxAuthority->filling_frequency == "Monthly"?"selected":""}}>Monthly</option>
                                        <option value="Quaterly" {{$federalTaxAuthority->filling_frequency == "Quaterly"?"selected":""}}>Quaterly</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="duedate">Due Date :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->due_date}}" name="due_date" id="due_date" class="form-control" placeholder="Due Date">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="extesion_due_date">Extension Due Date :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->extension_due_date}}" name="extension_due_date" id="extension_due_date" class="form-control" placeholder="Extension Due Date">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="authority_short_name">Authority Short Name :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->authority_short_name}}" name="authority_short_name" id="authority_short_name" class="form-control" placeholder="Authority Level">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="license_note">Authority Full Name :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->authority_full_name}}" name="authority_full_name" id="authority_full_name" class="form-control" placeholder="Authority Name">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="website">Website :</label>
                                <div class="col-md-5">
                                    <input type="url" value="{{$federalTaxAuthority->website}}" name="website" id="website" class="form-control" placeholder="Website" required>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="telephone">Telephone # :</label>
                                <div class="col-md-5">
                                    <input type="text" value="{{$federalTaxAuthority->telephone}}" name="telephone" id="telephone" class="form-control valid input-tel-mask" placeholder="Telephone" aria-invalid="false">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label text-right" for="license_note"></label>
                                <div class="col-md-9">
                                    <div class="">
                                        <button type="submit" class="col-md-2 btn btn-primary text-center">Update</button>
                                        <button class="col-sm-2 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection




@section('customModels')

    <div class="modal fade show" id="modal_TypeofEntity" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-modal="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Add Type of Entity</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post" id="typeOfEntityForm">
                                @csrf
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label text-right">Type Of Entity (SOS) :</label>
                                    <div class="col-sm-9" style="display:flex;padding:7px;">
                                        <div class="custom-control custom-radio col-md-3 col-span-4">
                                            <input type="radio" id="coporation" name="entity" value="coporation" class="custom-control-input" checked>
                                            <label class="custom-control-label" for="coporation">Corporation</label>
                                        </div>
                                        <div class="custom-control custom-radio  col-md-3">
                                            <input type="radio" id="llc" name="entity" value="llc" class="custom-control-input">
                                            <label class="custom-control-label" for="llc">LLC</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="input-group">
                                        <input type="text" id="newTypeOfEntity" name="type_of_entity" class="form-control" placeholder="Type of Entity">
                                        <div class="input-group-append">
                                            <button id="addNewTypeOfEntity" data-src="{{route('ajax.storeTypeOfEntity')}}" class="btn btn-primary waves-effect waves-light" type="button">Add Type of Entity</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <table id="typeOfEntity_table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="new-application-table_info">
                                        <thead>
                                        <tr role="row">
                                        </tr>
                                        <tr role="row">
                                            <th class="sorting_disabled sorting_asc" rowspan="1" colspan="1" aria-label="No" style="width:10%;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Type" style="width:40%;">Type of Entity</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Type" style="width:40%;">Type of Entity(SOS)</th>
                                            <th class="sorting align-center" tabindex="0" rowspan="1" colspan="1" style="width:10%;">Action</th>
                                        </tr>

                                        </thead>

                                        <tbody>
                                        @if(isset($typeOfEntityList))
                                            @foreach($typeOfEntityList as $key=>$typeOfEntity)
                                                <tr class="odd">
                                                    <td class="sorting_1">{{$key+1}}</td>
                                                    <td>{{$typeOfEntity->type_of_entity}}</td>
                                                    <td>{{$typeOfEntity->entity}}</td>
                                                    <td style="text-align:center;">
                                                        <a id="{{$typeOfEntity->id}}" onclick="delTypeOfEntity({{$typeOfEntity->id}});" class="deleteProfession1"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group row mb-3" style="float:right;">
                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-danger text-center">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            const libs = {
                ajaxCalling: function (url, data, method, callback) {
                    $.ajax({
                        url: url,
                        method: method,
                        data: data,
                        success: callback,
                        error: function (error) {
                            //returnval = error;
                        }
                    });
                }
            };

            $("#due_date").datepicker({
                autoclose: true,
                format: "M-dd",
            });
            $("#extension_due_date").datepicker({
                autoclose: true,
                format: "M-dd",
            });

            $("#federal_form").validate({
                rules: {
                    type_of_form: {required: true},
                    type_of_entity_id: {required: true},
                    form_name: {required: true},
                    due_date: {required: true},
                    extesion_due_date: {required: true},
                    website: {url:true},
                },
                messages: {
                    type_of_form: {required: "Select type of form."},
                    type_of_entity_id: {required: "Select type of Entity."},
                    form_name: {required: "form name must be required."},
                    due_date: {required: "select due date required."},
                    extesion_due_date: {required: "Select extension Date required."},
                    website:{url:"website format not correct."}
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');
                    $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
                    $(e).remove();
                },
                submitHandler: function (form) {
                    form.submit();

                }
            });

            $('#addNewTypeOfEntity').click(function () {
                const formsData = $('#typeOfEntityForm').serialize();
                const url = $(this).attr('data-src');
                const method = "POST";
                const data = formsData;
                libs.ajaxCalling(url, data, method, function (response) {
                    //console.log(response);
                    var jsonData = JSON.parse(response);
                    $('#typeOfEntity_table tbody').empty();
                    $("#type_of_entity_id").empty();
                    //$("#profession").select2();
                    var typeOfEntityOption = "<option value=''>Select Profession</option>";
                    for (var i = 0; i < jsonData.length; i++) {
                        var no = i + 1;
                        let typeOfEntityData = "<tr>";
                        typeOfEntityData += "<td>" + no + "</td>";
                        typeOfEntityData += "<td>" + jsonData[i]['type_of_entity'] + "</td>";
                        typeOfEntityData += "<td>" + jsonData[i]['entity'] + "</td>";
                        typeOfEntityData += '<td>' +
                            '<a href="javascript:void(0);" id="' + jsonData[i]['id'] + '" onclick="delProfession(' + jsonData[i]['id'] + ');" class="delProfession"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                            '</td>';
                        typeOfEntityOption += "<option value='" + jsonData[i]['id'] + "'>" + jsonData[i]['type_of_entity'] + " </option>";

                        $('#typeOfEntity_table tbody').append(typeOfEntityData);
                    }
                    $("#type_of_entity_id").append(typeOfEntityOption);
                    $('#modal_TypeofEntity').modal('hide');
                });
            });
        });

        function delTypeOfEntity(id) {
            const url = '/ajax/deleteTypeOfEntity';
            const method = "GET";
            const data = {
                'id': id
            };

            $.ajax({
                url: url,
                method: method,
                data: data,
                success: function (response) {
                    console.log(response);
                    var jsonData = JSON.parse(response);
                    $('#typeOfEntity_table tbody').empty();
                    $("#type_of_entity_id").empty();
                    //$("#profession").select2();
                    var typeOfEntityOption = "<option value=''>Select Profession</option>";
                    for (var i = 0; i < jsonData.length; i++) {
                        var no = i + 1;
                        let typeOfEntityData = "<tr>";
                        typeOfEntityData += "<td>" + no + "</td>";
                        typeOfEntityData += "<td>" + jsonData[i]['type_of_entity'] + "</td>";
                        typeOfEntityData += "<td>" + jsonData[i]['entity'] + "</td>";
                        typeOfEntityData += '<td>' +
                            '<a href="javascript:void(0);" id="' + jsonData[i]['id'] + '" onclick="delProfession(' + jsonData[i]['id'] + ');" class="delProfession"><i class="ri f-1-4x ri-delete-bin-2-line text-danger"></i></a>' +
                            '</td>';
                        typeOfEntityOption += "<option value='" + jsonData[i]['id'] + "'>" + jsonData[i]['type_of_entity'] + " </option>";

                        $('#typeOfEntity_table tbody').append(typeOfEntityData);
                    }
                    $("#type_of_entity_id").append(typeOfEntityOption);
                    $('#modal_TypeofEntity').modal('hide');
                },
                error: function (error) {

                }
            });
        }
    </script>
@endsection
