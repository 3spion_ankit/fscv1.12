@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup / Bank & Vendor / List Of Bank</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Bank</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="{{route('setup.bank-add')}}" class="btn btn-warning waves-effect waves-light">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Bank
                                                </a>
                                            </div>
                                        </div>

                                        <table id="upload-table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Bank Name</th>
                                                <th>Address</th>
                                                <th>Routing Number</th>
                                                <th>Telephone</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($bankList))
                                                @foreach($bankList as $key=>$bank)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$bank->bank_name}}</td>
                                                        <td>{{$bank->bank_address}}</td>
                                                        <td>{{$bank->routing_number}}</td>
                                                        <td>{{$bank->bank_contact_no}}</td>
                                                        <td>
                                                            <x-actionlink class="" route="" id="" buttons="edit" />
                                                            <x-actionlink class="" route="" id="" buttons="delete" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>

                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/uploads.js')}}"></script>
@endsection
