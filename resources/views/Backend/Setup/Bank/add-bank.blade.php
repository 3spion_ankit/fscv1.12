@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup / Bank & Vendor / List Of Bank</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-4">List Of Bank</h4>
                        <ul class="nav nav-pills navtab-bg nav-justified">
                            <li class="nav-item">
                                <a href="#bank_info" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                                    <span class="d-none d-sm-inline-block">Bank Info</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#other" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <span class="d-inline-block d-sm-none"><i class="mdi mdi-email-variant"></i></span>
                                    <span class="d-none d-sm-inline-block">Other</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="bank_info">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="{{route('setup.storeBank')}}" method="POST" id="bankdata">
                                                    @csrf
                                                    <input type="hidden" name="bank_id" value="0">
                                                    <h4> Bank Information</h4>
                                                    <hr/>
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Bank Name <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="bank_name" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Address <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="bank_address" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Routing Number <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="routing_number" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            @php $name=[
                                                                'label_name'=>'Telephone #.',
                                                                'type_name'=>'bank_contact_type',
                                                                'contact_name'=>'bank_contact_no',
                                                                'extnsion_name'=>'bank_contact_no_extension',
                                                                ];
                                                            @endphp
                                                            <x-telephone :name="$name" val="values"/>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="website">Website</label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="website" class="form-control" id="website">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"></div>
                                                    <h4> Branch Information</h4>
                                                    <hr/>
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Branch Name<span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="branch_name" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Address <span class="text-danger">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" name="branch_address" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            @php $name=[
                                                                'city_name'=>'city',
                                                                'zip_name'=>'zip',
                                                                'state_name'=>'state'
                                                                ];
                                                            @endphp
                                                            <x-CityStateZip data="data" message="GA" :name="$name"/>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="work_status_docs">Contact Person / Position<span class="text-danger">*</span></label>
                                                            <div class="col-md-5">
                                                                <input type="text" name="contact_person" class="form-control" id="certificate">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text" name="position" class="form-control" id="certificate">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            @php $name=[
                                                                'label_name'=>'Telephone #.',
                                                                'type_name'=>'contact_type',
                                                                'contact_name'=>'contact_no',
                                                                'extnsion_name'=>'contact_no_extension',
                                                                ];
                                                            @endphp
                                                            <x-telephone :name="$name" val="values"/>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            @php $name=[
                                                                'label_name'=>'Telephone 2 #.',
                                                                'type_name'=>'contact_type_2',
                                                                'contact_name'=>'contact_no_2',
                                                                'extnsion_name'=>'contact_no_extension_2',
                                                                ];
                                                            @endphp
                                                            <x-telephone :name="$name" val="values"/>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <x-email name="email" val="valu"/>
                                                        </div>
                                                        <div class="form-group row mb-3">
                                                            <label class="col-md-3 col-form-label text-right" for="notes">Notes</label>
                                                            <div class="col-md-9">
                                                                <textarea name="notes" id="notes" class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row mb-3 text-center">
                                                            <div class="col-md-6">
                                                                <button type="submit" class="col-md-5 btn btn-primary text-center">Submit</button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <button type="reset" class="col-md-5 btn btn-danger text-center">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"></div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/component.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/bank.js')}}"></script>
@endsection
