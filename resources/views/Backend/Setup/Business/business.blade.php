@extends('Backend.layouts.app')
@section('title','Profile')
@section('backend_content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Setup</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FSC</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Setup / Business / Create Business</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                            <h4>Setup Business</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active show mb-1" id="business-tab" data-toggle="pill" href="#business" role="tab" aria-controls="v-pills-home"
                                       aria-selected="true">
                                        Business</a>
                                    <a class="nav-link mb-1" id="business-category-tab" data-toggle="pill" href="#business-category" role="tab" aria-controls="v-pills-profile"
                                       aria-selected="false">
                                        Business Category</a>
                                </div>
                            </div>

                            <div class="col-sm-10">
                                <div class="tab-content pt-0">
                                    <div class="tab-pane fade active show" id="business" role="tabpanel" aria-labelledby="v-pills-company-tab">

                                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                            <h4>Create Business</h4>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Add_Business">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Business
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-application-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Business Name</th>
                                                <th>Business Image</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($businessList))
                                                @foreach($businessList as $key=>$business)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$business->name}}</td>
                                                        <td>{{$business->image}}</td>
                                                        <td>
                                                            <x-action-link-w-o-model
                                                                buttons="edit"
                                                                route="{{route('employee.edit',$employee->id)}}"
                                                            />

                                                            <x-action-link-w-o-model
                                                                buttons="delete"
                                                                route=""
                                                            />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="tab-pane fade" id="business-category" role="tabpanel" aria-labelledby="v-pills-company-tab">
                                        <div class="alert alert-Fsc Header" role="alert" style="text-align:center;">
                                            <h4>Create Business Category</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-sm-12 mb-1 text-right">
                                                <a href="javascript:void(0);" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal_Add_Business_Category">
                                                    <span class="btn-label"><i class="mdi mdi-plus"></i></span>Add Business Category
                                                </a>
                                            </div>
                                        </div>

                                        <table id="new-application-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Business Name</th>
                                                <th>Business Category</th>
                                                <th>NAICS</th>
                                                <th>SIC</th>
                                                <th>Business Image</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($businessCategoryList))
                                                @foreach($businessCategoryList as $key=>$businessCategory)
                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{$businessCategory->name}}</td>
                                                        <td>{{$businessCategory->category['name']}}</td>
                                                        <td>{{$businessCategory->category['naics']}}</td>
                                                        <td>{{$businessCategory->category['sic']}}</td>
                                                        <td>{{$businessCategory->category['image']}}</td>
                                                        <td>
                                                            <x-action-link-w-o-model
                                                                buttons="edit"
                                                                route="{{route('employee.edit',$employee->id)}}"
                                                            />

                                                            <x-action-link-w-o-model
                                                                buttons="delete"
                                                                route=""
                                                            />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- end tab-content -->
                            </div><!-- end col-9 -->

                        </div> <!-- end row -->
                    </div>
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

    </div> <!-- container -->
@endsection
@section('customModels')

    <div class="modal fade" id="modal_Add_Business_Category" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Add New Business Category</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" method="post">
                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3">Business Name</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="bussiness_name" name="bussiness_name">
                                            <option value="">Please select Business</option>

                                            <option value="1">Business</option>

                                            <option value="2">Non-Profit Organization</option>

                                            <option value="3">Service Industry</option>

                                            <option value="4">Profession</option>

                                            <option value="5">Investor</option>

                                            <option value="6">Personal</option>

                                            <option value="8">Other Business</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3">Business Category</label>
                                    <div class="col-md-6">
                                        <input name="business_cat_name" type="text" Placeholder="Business Category" id="business_cat_name" class="form-control" value="">
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3">Business Category Image</label>
                                    <div class="col-md-6">
                                        <div class="fileupload btn btn-success waves-effect waves-light">
                                            <span><i class="mdi mdi-cloud-upload mr-1"></i> Upload Files</span>
                                            <input type="file" class="upload">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3">NAICS / SIC Code</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="" name="" placeholder="NAICS" value="" aria-invalid="false">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control fsc-input sic" placeholder="SIC" id="" name="" value="">
                                    </div>
                                    <div class="col-md-2">
                                        <a href="javascript: void(0);" class="btn btn-primary">Go to Link</a>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>


                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3">Url</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Url" name="Url" id="Url" value="">
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3"></label>
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-4 btn btn-primary text-center">Submit</button>
                                        <button class="col-sm-4 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_Add_Business" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Create Business</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form class="control-form" action="{{route('setup.storeBusiness')}}" method="post" id="business_form" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label align-right" for="userName3">Business Name</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Business Name" name="business_Name" id="business_Name" value="">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label align-right" for="userName3">Business Image</label>
                                    <div class="col-md-3">
                                        <div class="fileupload btn btn-success waves-effect waves-light">
                                            <span><i class="mdi mdi-cloud-upload mr-1"></i> Upload Files</span>
                                            <input type="file" class="upload" name="business_image" onchange="loadFile(event)">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <img class="img img-preview" src="" id="previewImag" width="200" height="200"/>
                                    </div>

                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label align-right" for="userName3">Url</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Url" name="url" id="url" value="">
                                    </div>

                                </div>

                                <div class="form-group row mb-3">
                                    <label class="col-md-4 col-form-label align-right" for="userName3"></label>
                                    <div class="col-md-6">
                                        <button type="submit" class="col-md-4 btn btn-primary text-center">Submit</button>
                                        <button class="col-sm-4 btn btn-danger text-center" data-dismiss="modal" type="button">Cancel</button>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/component.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('Backend/js/custom pages/setup.js')}}"></script>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('previewImag');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>

@endsection
