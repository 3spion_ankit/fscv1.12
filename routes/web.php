<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\WorkStatusController;
use App\Http\Controllers\Admin\UploadsController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\Ajax\CountryStateController;
use App\Http\Controllers\employee\HiringController;
use App\Http\Controllers\employee\ApplicationController;
use App\Http\Controllers\Employee\CandidateController;
use App\Http\Controllers\Setup\BankController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FSCClient\ClientController;
use App\Http\Controllers\Setup\BusinessController;
use App\Http\Controllers\Setup\CountyController;
use App\Http\Controllers\Ajax\StateCountyController;
use App\Http\Controllers\Setup\FederalController;
use App\Http\Controllers\Master\MasterController;
use App\Http\Controllers\Setup\StateAuthoritiesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/migratecode',function(){
   try{
       \Illuminate\Support\Facades\Artisan::call('migrate');
       return redirect(\route('admin.login'));
   }catch(Exception $ex){
       dd($ex->getMessage());
   }catch(HttpException $ex){
       dd($ex->getMessage());
   }catch(ErrorException $ex){
       dd($ex->getMessage());
   }

});

Route::get('/dbseeds',function(){
    try{
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }catch(Exception $ex){
        dd($ex->getMessage());
    }catch(HttpException $ex){
        dd($ex->getMessage());
    }catch(ErrorException $ex){
        dd($ex->getMessage());
    }

});

//Route::get('/makeController/{test}',function (){
//   \Illuminate\Support\Facades\Artisan::command('make:controller',function($test){
//
//   });
//});

/*
 * Common Menu routes
 */

Route::post('admin/accounts',[ProfileController::class,'changePassword'])->name('admin.changepassword');

Route::get('/admin/models/share-account', function () {
    return view('Backend.Admin.Models.share-account');
})->name('models.share-holder');



//Admin Calling View With Data
Route::get('/admin/profile',[ProfileController::class,'index'])->name('admin.profile');
Route::get('/admin/work-status',[WorkStatusController::class,'index'])->name('admin.work-status');
Route::get('/admin/moveLicenseHistory',[WorkStatusController::class,'moveLicenseHistory'])->name('admin.licenseHistroy');
Route::get('/admin/uploads',[UploadsController::class,'index'])->name('admin.uploads');
Route::get('/admin/editEmployee',[EmployeeController::class,'edit'])->name('admin.editEmployee');

//Admin Actions
Route::post('/admin/storeProfile',[ProfileController::class,'storeCompanyInfo'])->name('admin.storeProfile');
Route::post('/admin/storeContact',[ProfileController::class,'storeContact'])->name('admin.storeContact');
Route::post('/admin/storeSecurity',[ProfileController::class,'storeSecurity'])->name('admin.storeSecurity');
Route::post('/admin/storeFormation',[ProfileController::class,'storeFormation'])->name('admin.storeFormation');
Route::post('/admin/storeAdminBusinessLicense',[ProfileController::class,'storeBusinessLicense'])->name('admin.storeAdminBusinessLicense');
Route::post('/admin/storeProfessionalLicense',[ProfileController::class,'storeProfessionalLicense'])->name('admin.storeProfessionalLicense');
Route::get('/admin/getProfessionalLicense',[ProfileController::class,'getProfessionalLicense'])->name('admin.getProfessionalLicense');
Route::post('/admin/storeTaxation',[ProfileController::class,'storeTaxation'])->name('admin.storeTaxation');
Route::post('/admin/storeProfession',[ProfileController::class,'storeProffesion'])->name('admin.storeProfession');
Route::get('/admin/deleteProfession',[ProfileController::class,'deleteProfession'])->name('admin.deleteProfession');
Route::post('/admin/storeShareHolder',[ProfileController::class,'storeShareHolder'])->name('admin.storeShareHolder');
Route::get('/admin/getShareHolder/{id}',[ProfileController::class,'getSharedHolderData'])->name('admin.getShareHolder');
Route::get('/admin/deleteShareHolder/{id}',[ProfileController::class,'deleteSharedHolderData'])->name('admin.deleteShareHolder');
Route::post('/admin/storeBankAccount',[ProfileController::class,'storeBankAccount'])->name('admin.storeBankAccount');

/*
 * Admin Work status Routes
 * start
 */
Route::post('/admin/storeCorporationLicense',[WorkStatusController::class,'storeCorporationLicense'])->name('admin.storeCorporationLicense');
Route::post('/admin/storeShareLedger',[WorkStatusController::class,'storeShareLedger'])->name('admin.storeShareLedger');
Route::post('/admin/storeBusinessLicense',[WorkStatusController::class,'storeBusinessLicense'])->name('admin.storeBusinessLicense');


/*
 * Ajax Calling Routes
 * start
 */

Route::post('/admin/removeCertificate',[ProfileController::class,'removeCertificate'])->name('admin.removeCertificate');
Route::get('/admin/viewDocument/{type}',[ProfileController::class,'viewCertificate'])->name('admin.viewDocument');
Route::post('/admin/storeRepresent',[EmployeeController::class,'setRepresent'])->name('admin.storeRepresent');
Route::post('/ajax/storeTypeOfEntity',[MasterController::class,'storeTypeOfEntity'])->name('ajax.storeTypeOfEntity');
Route::get('/ajax/deleteTypeOfEntity',[MasterController::class,'deleteTypeOfEntity'])->name('ajax.deleteTypeOfEntity');
Route::post('/ajax/setState',[CountryStateController::class,'setStateByCountry'])->name('ajax.setState');
Route::post('/ajax/storePay',[EmployeeController::class,'storePay'])->name('ajax.storePay');
Route::post('/ajax/storeReview',[EmployeeController::class,'storeReview'])->name('ajax.storeReview');
Route::post('/ajax/getCounty',[StateCountyController::class,'setCountyByState'])->name('ajax.getCountyByState');
Route::post('/ajax/getCountyCode',[StateCountyController::class,'getCountyCode'])->name('ajax.getCountyCode');
Route::get('/ajax/getFormsByEntity/{id}',[ProfileController::class,'getFormsByTypeOfEntity'])->name('ajax.getFormsByEntity');

/*
 * For Coporation
 */
Route::post('/admin/editCorporation',[WorkStatusController::class,'editCorporation'])->name('admin.editCorporation');

/*
 * End
 */

Route::get('/Employee/hiring',[HiringController::class,'index'])->name('employee.hiring');
Route::post('/Employee/storeHiring',[HiringController::class,'storeHiring'])->name('employee.hiring.storeHiring');
Route::get('/Employee/application',[ApplicationController::class,'index'])->name('employee.application');
Route::get('/Employee/candidate',[CandidateController::class,'index'])->name('employee.candidate');
Route::get('/Employee/supervisor',[EmployeeController::class,'supervisorList'])->name('employee.supervisor');
Route::get('/Employee/employees',[EmployeeController::class,'index'])->name('employee.employees');
Route::get('/Employee/create',[EmployeeController::class,'create'])->name('employee.create');
Route::get('/Employee/edit/{id}',[EmployeeController::class,'edit'])->name('employee.edit');
Route::post('/Employee/update',[EmployeeController::class,'createAndUpdate'])->name('employee.updateInfo');
Route::post('/Employee/store',[EmployeeController::class,'store'])->name('employee.store');

/*
 * Fsc Client
 */
Route::get('/FSC_Client/list',[ClientController::class,'index'])->name('fsc_client');
Route::get('/FSC_Client/create',[ClientController::class,'create'])->name('fsc_client.client');

/*
 * Setup menu Routes
 */

Route::get('/setup/bank-list',[BankController::class,'index'])->name('setup.bank-list');
Route::get('/setup/bank-add',[BankController::class,'create'])->name('setup.bank-add');
Route::post('/setup/storeBank',[BankController::class,'store'])->name('setup.storeBank');

Route::post('/setup/storeBusiness',[BusinessController::class,'storeBusiness'])->name('setup.storeBusiness');
Route::post('/setup/storeFederalTaxAuthority',[FederalController::class,'storeFederalTaxAuthority'])->name('setup.storeFederalTadAuthority');
Route::get('/setup/editFederalTaxAuthority/{id}',[FederalController::class,'edit'])->name('setup.federalEdit');
Route::get('/setup/business',[BusinessController::class,'business'])->name('setup.business');
Route::get('/setup/business-brand',[BusinessController::class,'business_brand'])->name('setup.business-brand');
Route::get('/setup/create-tax-form-federal',[FederalController::class,'create'])->name('setup.create-tax-form-federal');
Route::get('/setup/create-tax-form-list',[FederalController::class,'index'])->name('setup.federal');

Route::get('/setup/create-state-forms',[StateAuthoritiesController::class,'create'])->name('setup.stateCreate');
Route::get('/setup/list-state-forms',[StateAuthoritiesController::class,'index'])->name('setup.stateList');
Route::post('/setup/storeStateAuthorities',[StateAuthoritiesController::class,'storeStateAuthorities'])->name('setup.storeStateAuthorities');
Route::get('/setup/getStateAuthorities',[StateAuthoritiesController::class,'edit'])->name('setup.stateEdit');

Route::get('/setup/create-county',[CountyController::class,'create'])->name('setup.create-county');
Route::get('/setup/county-list',[CountyController::class,'index'])->name('setup.county-list');
Route::post('/setup/storeCounty',[CountyController::class,'storeCounty'])->name('setup.storeCounty');
Route::get('/setup/editCounty/{id}',[CountyController::class,'edit'])->name('setup.editCounty');



Route::group(['prefix'=>'admin','middleware'=>'admin:admin'],function (){
    Route::get('/login',[AdminController::class,'loginForm']);
    Route::post('/login',[AdminController::class,'store'])->name('admin.login');
    /*
     * Setup Admin Profiles page Routes
     */
});


Route::group(['prefix'=>'/'],function (){
    Route::get('/login',[AdminController::class,'loginForm']);
    Route::post('/login',[AdminController::class,'store'])->name('admin.login');
    /*
     * Setup Admin Profiles page Routes
     */
});
Route::get('/login',[AdminController::class,'frontLogin']);
Route::middleware(['auth:sanctum,admin', 'verified'])->get('/admin/dashboard', function () {
    return view('Backend.dashboard');
})->name('admin.dashboard');

Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
