<?php


namespace App\Utility;


use Carbon\Carbon;

class DateUtility
{

    public function formats($date)
    {
        return date('Y-m-d',strtotime($date));
    }

    public function retriveDate($date)
    {
        return date('m/d/Y',strtotime($date));
    }
    public function retriveDateInVal($date)
    {
        return date('m/d/Y',strtotime($date));
    }

    public function retriveDateSpefic($date)
    {
        return date('M-d-Y',strtotime($date));
    }

    public function futureDate($days)
    {
        return date('Y-m-d',strtotime('+'.$days.' days',strtotime(str_replace('/', '-', Carbon::now()))));
    }


}
