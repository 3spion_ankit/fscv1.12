<?php

namespace App\Models\Admin;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;
    protected $table = "admin_formation";
    protected $fillable = [
        'admin_id',
        'state_of_formation',
        'certificate_type',
        'date_of_incorporation',
        'legal_name',
        'control_number',
        'certificate',
        'articles',
        'agent_fname',
        'agent_mname',
        'agent_lname',
        'renewal_date'
    ];

    public function admins()
    {
        return $this->belongsTo(Admin::class);
    }
}
