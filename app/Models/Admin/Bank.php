<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $fillable = [
        'bank_name',
        'bank_address',
        'routing_number',
        'bank_contact_type',
        'bank_contact_no',
        'bank_contact_no_extension',

        //Branch Datatables

        'branch_name',
        'branch_address',
        'contact_person',
        'position',
        'city',
        'state',
        'zip',
        'contact_type',
        'contact_type_2',
        'contact_no',
        'contact_no_2',
        'contact_no_extension',
        'contact_no_extension_2',
        'notes',
    ];
}
