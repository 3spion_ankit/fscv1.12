<?php

namespace App\Models\Admin;

use App\Models\Admin\Master\TypeOfEntity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FederalAuthorities extends Model
{
    use HasFactory;

    protected $table = "federal_authorities";
    protected $fillable = [
        'type_of_form',
        'type_of_entity',
        'type_of_entity_id',
        'form_name',
        'authority_state',
        'state_form_name',
        'filling_frequency',
        'due_date',
        'extension_due_date',
        'authority_short_name',
        'authority_full_name',
        'website',
        'telephone',
    ];

    public function typeOfEntity()
    {
        return $this->belongsTo(TypeOfEntity::class, 'type_of_entity_id', 'id');
    }
}
