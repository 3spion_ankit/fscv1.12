<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    public $table = "admin_corporation_license";
    use HasFactory;

    protected $fillable = [
        'admin_id',
        'renewal_for',
        'renewal_year',
        'renewal_amount',
        'paid_amount',
        'method_of_payment',
        'corporation_status',
        'annually_receipt',
        'officer',
        'website',
    ];
}
