<?php

namespace App\Models\Admin\Employee;

use App\Models\Admin\Employee\EmployeeProfile\EmergencyInfo;
use App\Models\Admin\Employee\EmployeeProfile\HiringInfo;
use App\Models\Admin\Employee\EmployeeProfile\Pay;
use App\Models\Admin\Employee\EmployeeProfile\PersonalInfo;
use App\Models\Admin\Employee\EmployeeProfile\Review;
use App\Models\Admin\Employee\EmployeeProfile\SecurityInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UEmployee extends Model
{
    protected $table = "uemployees";
    use HasFactory;
    protected $fillable=[
        'user_type',
        'user_code',
        'represent',
        'prefix_name',
        'fname',
        'mname',
        'lname',
        'address_1',
        'address_2',
        'country',
        'city',
        'state',
        'zip',
        'contact_no_1',
        'contact_no_type_1',
        'contact_no_extension_1',
        'contact_no_2',
        'contact_no_type_2',
        'contact_no_extension_2',
        'email',
        'profile_pic'
    ];

    public function Hiring()
    {
        return $this->hasOne(HiringInfo::class,'user_id','id');
    }

    public function Emergency()
    {
        return $this->hasOne(EmergencyInfo::class,'user_id','id');
    }

    public function Security()
    {
        return $this->hasOne(SecurityInfo::class,'user_id','id');
    }
    public function Personal()
    {
        return $this->hasOne(PersonalInfo::class,'user_id','id');
    }
    public function Pay()
    {
        return $this->hasMany(Pay::class,'user_id','id');
    }
    public function Review()
    {
        return $this->hasMany(Review::class,'user_id','id');
    }
}
