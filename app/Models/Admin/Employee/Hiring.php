<?php

namespace App\Models\Admin\Employee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hiring extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_date',
        'job_type',
        'position',
        'country',
        'state',
        'city',
        'name',
        'contact_no',
        'description',
        'status'
    ];
}
