<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use App\Models\Admin\Employee\UEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HiringInfo extends Model
{
    use HasFactory;
protected $table="uemployees_hiring";
    protected $fillable = [
        'user_id',
        'hire_date',
        'termination_date',
        't_note',
        'rehire_date',
        'retermination_date',
        't_note2',
        'branch_city',
        'branch_name',
        'hiring_position',
        'is_supervisor',
        'branch_note',
    ];

    public function Uemployees()
    {
        return $this->belongsTo(UEmployee::class, 'user_id', 'id');
    }
}
