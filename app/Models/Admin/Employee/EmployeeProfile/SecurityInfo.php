<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use App\Models\Admin\Employee\UEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SecurityInfo extends Model
{
    use HasFactory;
    protected $table="uemployees_security";
    protected $fillable=[
        'user_id',
        'question1',
        'que_1',
        'question2',
        'que_2',
        'question3',
        'que_3',
        'answer1',
        'answer2',
        'answer3',
    ];

    public function Uemployees()
    {
        return $this->belongsTo(UEmployee::class,'user_id','id');
    }
}
