<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use App\Models\Admin\Employee\UEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    use HasFactory;
    protected $table="uemployees_pay";
    protected $fillable=[
      'user_id',
      'pay_method',
      'pay_frequency',
      'pay_rate',
      'effective_date',
      'notes',
    ];

    public function Uemployees()
    {
        return $this->belongsTo(UEmployee::class, 'user_id', 'id');
    }
}
