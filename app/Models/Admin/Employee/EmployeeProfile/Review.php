<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $table = "uemployees_reviews";

    protected $fillable = [
        'user_id',
        'review_date',
        'next_review',
        'next_review_date',
        'next_pay_rate',
        'comments',
    ];
}
