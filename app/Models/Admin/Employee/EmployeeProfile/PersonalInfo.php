<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use App\Models\Admin\Employee\UEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalInfo extends Model
{
    use HasFactory;
protected $table="uemployees_personal";
    protected $fillable = [
        'user_id',
        'gender',
        'marital_staus',
        'dob',
        'langauge',
        'pf1',
        'pf1_document',
        'pf2',
        'pf2_document',
        'resume_document',
        'type_of_agreement',
        'agreement_document',
    ];

    public function Uemployees()
    {
        return $this->belongsTo(UEmployee::class, 'user_id', 'id');
    }
}
