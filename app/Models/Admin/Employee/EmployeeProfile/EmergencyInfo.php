<?php

namespace App\Models\Admin\Employee\EmployeeProfile;

use App\Models\Admin\Employee\UEmployee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmergencyInfo extends Model
{
    use HasFactory;
protected $table="uemployees_emergency";
    protected $fillable = [
        'user_id',
        'emergency_fname',
        'emergency_mname',
        'emergency_lname',
        'emergency_address_1',
        'emergency_address_2',
        'emergency_city',
        'emergency_state',
        'emergency_zip',
        'emergency_contact_1',
        'emergency_type_1',
        'emergency_ext_1',
        'emergency_contact_2',
        'emergency_type_2',
        'emergency_ext_2',
        'emergency_email',
        'emergency_relation',
        'emergency_note',
    ];

    public function Uemployees()
    {
        return $this->belongsTo(UEmployee::class, 'user_id', 'id');
    }
}
