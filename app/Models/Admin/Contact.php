<?php

namespace App\Models\Admin;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $table = "admin_contact_info";

    protected $fillable = [
        'admin_id',
        'prefix_name',
        'fname',
        'mname',
        'lname',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'contact_no_1',
        'contact_no_type_1',
        'contact_no_extension_1',
        'contact_no_2',
        'contact_no_type_2',
        'contact_no_extension_2',
        'same_as_company_fax_no',
        'fax_no',
        'email'
    ];

    public function admins()
    {
        return $this->belongsTo(Admin::class);
    }
}
