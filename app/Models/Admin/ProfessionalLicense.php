<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfessionalLicense extends Model
{
    use HasFactory;

    protected $table = "admin_professional_license";
    protected $fillable = [
        'admin_id',
        'profession',
        'professional_license_state',
        'professional_effective_date',
        'type_of_license',
        'professional_license_period',
        'professional_license_no',
        'professional_expire_date',
        'professional_license_note',
        'is_ce_status'
    ];
}
