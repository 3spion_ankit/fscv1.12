<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxCounty extends Model
{
    use HasFactory;

    protected $fillable = [
        'county_name',
        'county_code',
        'county_authority_name',
        'county_website',
        'county_telephone',
        'county_address',
        'county_city',
        'county_state',
        'county_zip',
        'county_fax',
    ];
}
