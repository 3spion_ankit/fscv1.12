<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminBank extends Model
{
    use HasFactory;
    protected $fillable=[
        'bank_id',
        'bank_name',
        'nick_name',
        'last_four_digit',
        'check_stubs',
        'op_date',
        'statement',
        'ac_status',
    ];
}
