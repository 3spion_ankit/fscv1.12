<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminTaxation extends Model
{
    use HasFactory;

    protected $table = "admin_taxations";
    protected $fillable = [
        "admin_id",
        "type_of_entity",
        "typeOfForm",
        "typeofcorp_effect",
        "income_tax_federal_id",
        "income_tax_federal_file",
        "income_tax_federal_due_date",
        "income_tax_federal_extension_due_date",
        "income_tax_state_id",
        "income_tax_state_file",
        "income_tax_state_due_date",
        "income_tax_extension_state_due_date",

        "fica_name",
        "fica_dept_authority",
        "fica_form_number",
        "fica_quarter_period",
        "fica_quarter_period_date",
        "fica_quarter_period",
        "fica_due_date",
        "fica_eptpspin",
        "fica_pay_pw",
        "fica_federal_payment_frequency",
        "fica_payment_frequency_date",
        "fica_note",

        "futa_name",
        "futa_dept_authority",
        "futa_frequency",
        "futa_form_no",
        "futa_frequency_due_date",
        "futa_payment_frequency",
        "futa_payment_frequency_date",
        "futa_note",

        "state_holding_quarter_type",
        "state_holding_quarter_date",
        "state_holding_due_date",
        "state_holding_no",
        "state_holding_eptpspin",
        "state_holding_Username",
        "state_holding_pay_pw",
        "state_holding_payment_frequency",
        "state_holding_payment_due_date",

        "suta_quarter_period",
        "suta_peroid_end_date",
        "suta_peroid_due_date",
        "suta_unemployment_no",
        "suta_username",
        "suta_payment_frequency",
        "suta_payment_frequency_date"
    ];
}
