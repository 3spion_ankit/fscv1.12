<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shareholder extends Model
{
    use HasFactory;
    protected $fillable=[
        'formation_id',
        'fname',
        'mname',
        'lname',
        'position',
        'percentage',
        'effective_date',
        'status'
    ];
}
