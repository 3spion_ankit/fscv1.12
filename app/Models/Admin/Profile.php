<?php

namespace App\Models\Admin;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table = "admin_profiles";
    protected $fillable = [
        'admin_id',
        'company_name',
        'business_name',
        'address_1',
        'address_2',
        'city',
        'comp_state',
        'state_code',
        'zip',
        'contact_no',
        'contact_no_type',
        'extension',
        'fax_no',
        'website',
        'email',
        'same_as_company_info',
        'pa_address_1',
        'pa_address_2',
        'pa_city',
        'pa_state',
        'pa_state_code',
        'pa_zip',
        'county',
        'county_code'
    ];

    public function admins()
    {
        return $this->belongsTo(Admin::class);
    }
}
