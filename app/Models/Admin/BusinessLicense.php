<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessLicense extends Model
{
    use HasFactory;

    protected $table = "admin_business_license";
    protected $fillable = [
        'admin_id',
        'jurisdiction',
        'license_city',
        'license_county',
        'license_county_code',
        'license_no',
        'expire_date',
        'license_note',
    ];
}
