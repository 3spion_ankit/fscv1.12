<?php

namespace App\Models\Admin\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Represent extends Model
{
    use HasFactory;
    protected $fillable=[
        'represent_name'
    ];
}
