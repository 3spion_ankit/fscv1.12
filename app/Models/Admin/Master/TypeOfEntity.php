<?php

namespace App\Models\Admin\Master;

use App\Models\Admin\FederalAuthorities;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfEntity extends Model
{
    use HasFactory;
    protected $table="type_of_entities";
    protected $fillable=[
        'type_of_entity',
        'entity'
    ];

    public function federalTaxAuthority()
    {
        return $this->hasOne(FederalAuthorities::class,'type_of_entity_id','id');
    }
}
