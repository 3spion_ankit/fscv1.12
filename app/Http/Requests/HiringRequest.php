<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HiringRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_date'=>['required'],
            'job_type'=>['required'],
            'position'=>['required'],
            'country'=>['required'],
            'state'=>['required'],
            'city'=>['required'],
            'name'=>['required'],
            'contact_no'=>['required']
        ];
    }
}
