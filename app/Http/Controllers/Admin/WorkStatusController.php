<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Corporation;
use App\Models\Admin\License;
use App\Models\Admin\Master\Profession;
use App\Models\Admin\ProfessionalLicense;
use App\Models\Admin\ShareLedger;
use App\Utility\DateUtility;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Laravel\Jetstream\Jetstream;

class WorkStatusController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $id = Auth::user()->id;
        $corporationLicense = Corporation::Where('admin_id', $id)->get();
        $businessLicense = License::where('admin_id',$id)->get();
        $shareLedgerList = ShareLedger::where('admin_id',$id)->get();

        $professionalLicenseList = ProfessionalLicense::where('admin_id',$id)->get();
        $professionList = Profession::all();
        return view('Backend.Admin.work-status', compact([
            'businessLicense',
            'corporationLicense',
            'shareLedgerList',
            'professionalLicenseList',
            'professionList'
        ]));
    }


    public function removeFile($filename)
    {
        if (File::isFile(public_path($filename))) {
            unlink(public_path($filename));
        }
    }

    public function moveLicenseHistory()
    {
        return redirect(route('admin.work-status'))->with(['worktabno' => 2,'insideNo'=>2]);
    }

    public function editCorporation(Request $request)
    {
        $id=$request->corporation_id;
        $corporationList = Corporation::where('id',$id)->get();
        echo json_encode($corporationList);
    }

    public function storeCorporationLicense(Request $request)
    {
        try {
            //return $request->all();
            $upload_annual_receipt = '';
            $upload_officer = '';
            if ($request->hasFile('annually_receipt')) {
                $fileName = str_replace(' ', '_', $_FILES['annually_receipt']['name']);
                $upload_annual_receipt = date('mdYHis') . uniqid() . $fileName;
                $request->annually_receipt->move('company_document/corporation', $upload_annual_receipt);
            }else{
                $upload_annual_receipt = '';
            }
            if ($request->hasFile('officer')) {
                $fileName = str_replace(' ', '_', $_FILES['officer']['name']);
                $upload_officer = date('mdYHis') . uniqid() . $fileName;
                $request->officer->move('company_document/corporation', $upload_officer);
            }else{
                $upload_officer = '';
            }
            if($request->corporation_id > 0){
                $id = $request->corporation_id;
                $input = $request->except(['corporation_id']);
                $input['annually_receipt'] = $upload_annual_receipt;
                $input['officer'] = $upload_officer;
                Corporation::update($id,$input);
            }else{
                $input = $request->all();
                $input['annually_receipt'] = $upload_annual_receipt;
                $input['officer'] = $upload_officer;
                Corporation::create($input);
            }
            return redirect(route('admin.work-status'));
        } catch (Exception $ex) {
            $this->removeFile('company_document/corporation/' . $upload_annual_receipt);
            $this->removeFile('company_document/corporation/' . $upload_officer);
            abort(500);
        } catch (QueryException $ex) {
            $this->removeFile('company_document/corporation/' . $upload_annual_receipt);
            $this->removeFile('company_document/corporation/' . $upload_officer);
            abort(504);
        } catch (\ErrorException $ex) {
            $this->removeFile('company_document/corporation/' . $upload_annual_receipt);
            $this->removeFile('company_document/corporation/' . $upload_officer);
            abort(500);
        }
    }

    public function storeShareLedger(Request $request)
    {
        try {
            $dateUtility = new DateUtility();
            if($request->has('shareledger_id'))
            {
                $id = $request->shareledger_id;
                $input = $request->except(['_token','shareledger_id']);
                $input['transfer_date'] = $dateUtility->formats($request->transfer_date);
                ShareLedger::where('id',$id)->update($input);
            }else{

                $input = $request->all();
                $input['transfer_date'] = $dateUtility->formats($request->transfer_date);
                ShareLedger::create($input);
            }
            return redirect(route('admin.work-status'));
        } catch (Exception $ex) {
            abort(404);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function storeBusinessLicense(Request $request)
    {
        try {
            $upload_license = '';
            if ($request->hasFile('license_copy')) {
                $fileName = str_replace(' ', '_', $_FILES['license_copy']['name']);
                $upload_license = date('mdYHis') . uniqid() . $fileName;
                $request->license_copy->move('company_document/license', $upload_license);
            }
            $input = $request->all();
            $dateformate = new DateUtility;
            if ($request->blid != 0) {

            } else {
                $input['license_copy']=$upload_license;
                $input['license_issue_date'] = $dateformate->formats($request->license_issue_date);
                License::create($input);
            }
            return redirect(route('admin.work-status'));
        } catch (Exception $ex) {
            $this->removeFile('company_document/license/' . $upload_license);
            dd($ex->getMessage());
            //abort(403);
        } catch (QueryException $ex) {
            $this->removeFile('company_document/license/' . $upload_license);
            //abort(403);
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            $this->removeFile('company_document/license/' . $upload_license);
            //abort(403);
            dd($ex->getMessage());
        }
    }
}
