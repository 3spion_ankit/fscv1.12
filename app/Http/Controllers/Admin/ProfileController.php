<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Admin\AdminBank;
use App\Models\Admin\AdminTaxation;
use App\Models\Admin\Bank;
use App\Models\Admin\BusinessLicense;
use App\Models\Admin\Contact;
use App\Models\Admin\Corporation;
use App\Models\Admin\FederalAuthorities;
use App\Models\Admin\Formation;
use App\Models\Admin\License;
use App\Models\Admin\Master\Profession;
use App\Models\Admin\Master\TypeOfEntity;
use App\Models\Admin\ProfessionalLicense;
use App\Models\Admin\Profile;
use App\Models\Admin\Security;
use App\Models\Admin\Shareholder;
use App\Models\Admin\TaxCounty;
use App\Models\PositionMaster;
use App\Utility\DateUtility;
use File;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $isProfileExsist = Profile::all()->count();
        $isContactExist = Contact::all()->count();
        $isSecurityExist = Security::all()->count();
        $isFormationExist = Formation::all()->count();
        $isCorporationLicenseExist = Corporation::all()->count();
        $isBusinessLicenseExist = BusinessLicense::all()->count();
        $isProfessionalLicenseExist = ProfessionalLicense::all()->count();
        $isTaxationListExist = AdminTaxation::all()->count();
        //exit();
        $companyInfo = Profile::where('admin_id', $id)->first();
        $contactInfo = Contact::where('admin_id', $id)->first();
        $securityInfo = Security::where('admin_id', $id)->first();
        $formationInfo = Formation::where('admin_id', $id)->first();
        $businessLicenseInfo = BusinessLicense::where('admin_id', $id)->first();
        $professionalLicenseList = ProfessionalLicense::where('status', 1)->get();
        $licenseData = License::where('admin_id', $id)->latest()->first();
        $taxationList = AdminTaxation::where('admin_id', $id)->latest()->first();

        $shareHolders = Shareholder::where('formation_id', '1')->get();
        $totalPercentageOfShareholder = Shareholder::where('formation_id', 1)->sum('percentage');

        $bankList = Bank::all();
        $adminBankList = AdminBank::all();
        $taxCountyList = TaxCounty::all();
        $professionList = Profession::all();
        $positionsList = PositionMaster::all();
        $typeOfEntityList = TypeOfEntity::all();

        $corporationLicense = Corporation::where('admin_id', Auth::user()->id)->latest()->first();
        $thisYear = date('Y');
        $licenseRenewalFor = $corporationLicense->renewal_for + $thisYear;
        $renewal_date = "Apr-01-" . $licenseRenewalFor;
        $licenseExpireDate = 1 + $thisYear;
        $license_expire_date = "Mar-31-" . $licenseExpireDate;

        $submittedPosition = [];
        foreach ($shareHolders as $key=>$shareHolder):
            $submittedPosition[$key+1]=$shareHolder->position;
        endforeach;

        $passData = [
            'shareHolders',
            'positionsList',
            'typeOfEntityList',
            'submittedPosition',
            'isFormationExist',
            'formationInfo',
            'isSecurityExist',
            'securityInfo',
            'isProfileExsist',
            'companyInfo',
            'isContactExist',
            'contactInfo',
            'bankList',
            'totalPercentageOfShareholder',
            'adminBankList',
            'isCorporationLicenseExist',
            'corporationLicense',
            'taxCountyList',
            'renewal_date',
            'licenseData',
            'isBusinessLicenseExist',
            'businessLicenseInfo',
            'license_expire_date',
            'professionList',
            'professionalLicenseList',
            'isProfessionalLicenseExist',
            'isTaxationListExist',
            'taxationList'
        ];
        return view('Backend.Admin.profile', compact($passData));
    }

    public function getFormsByTypeOfEntity($id)
    {
        try{
            echo json_encode(FederalAuthorities::where('type_of_entity_id',$id)->first());
        }catch (QueryException $ex){
            dd($ex->getMessage());
        }catch (ErrorException $ex){
            dd($ex->getMessage());
        }

    }

    public function removeCertificate(Request $request)
    {
        try {
            $id = $request->id;
            $certificate = $request->certificate;
            if ($certificate == 'certificate') {
                Formation::where('id', $id)->update([
                    'certificate' => ''
                ]);
            } else if ($certificate == 'article') {
                Formation::where('id', $id)->update([
                    'articles' => ''
                ]);
            }
            $msg = 'Deleted your ' . $certificate;
            return redirect()->back()->with(['ProfileSuccess' => $msg, 'tab_no' => 4]);
        } catch (\Exception $ex) {
            abort(500);
            echo 'Error';
        }

    }

    public function viewCertificate(Request $request, $type)
    {
        $fomationDocs = Formation::where('admin_id', Auth::user()->id)->first();
        $license = License::where('admin_id', Auth::user()->id)->latest()->first();
        $documentLink = "";
        if ($type == 1) {
            $documentLink = 'http://' . $request->getHttpHost() . '/company_document/certificate/' . $fomationDocs->certificate;
        } else if ($type == 2) {
            $documentLink = 'http://' . $request->getHttpHost() . '/company_document/article/' . $fomationDocs->articles;
        } else if ($type == 3) {
            $documentLink = 'http://' . $request->getHttpHost() . '/company_document/license/' . $license->license_copy;
        }
        return view('Backend.Admin.document-viewer', compact(['documentLink']));
    }

    public function storeCompanyInfo(Request $request)
    {
        try {
            Validator::make($request->all(), [
                'company_name' => ['required'],
                'business_name' => ['required'],
                'address_1' => ['required'],
                'city' => ['required'],
                'comp_state' => ['required'],
                'zip' => ['required'],
                'pa_city' => ['required'],
                'pa_state' => ['required'],
                'pa_zip' => ['required'],
                'contact_no' => ['required'],
                'email' => ['email:rfc,dns']
            ])->validate();

            if ($request->has('profile_id')) {
                $id = $request->profile_id;
                $input = $request->except(['_token', 'profile_id', 'pa_state__']);

                $input['state_code'] = 'None';
                $input['pa_state_code'] = 'None';
                Profile::where('id', $id)->update($input);
            } else {
                $input = $request->except(['']);
                $input['state_code'] = 'GA';
                $input['pa_state_code'] = 'GA';
                Profile::create($input);
            }
            //return redirect(route('admin.profile'));
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 1, 'tab_no1' => 2]);
        } catch (Exception $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 1]);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 1]);
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 1]);
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 1]);
        }
    }

    public function storeContact(Request $request)
    {
        try {
            Validator::make($request->all(), [
                'fname' => ['required'],
                'lname' => ['required'],
                'city' => ['required'],
                'state' => ['required'],
                'zip' => ['required'],
                'contact_no_1' => ['required'],
                'contact_no_type_1' => ['required'],
                'email' => ['email:rfc,dns']
            ])->validate();

            Admin::where('id', Auth::user()->id)->update(['name' => $request->fname . " " . $request->lname]);

            if ($request->has('contact_id')) {
                $id = $request->contact_id;
                $input = $request->except(['_token', 'contact_id']);
                if ($request->has('same_as_company_fax_no')) {
                    if ($request->same_as_company_fax_no == "on") {
                        $input['same_as_company_fax_no'] = 1;
                    } else {
                        $input['same_as_company_fax_no'] = 0;
                    }
                }
                //$input['state_code']='None';
                //$input['pa_state_code']='None';
                Contact::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                if ($request->has('same_as_company_fax_no')) {
                    if ($request->same_as_company_fax_no == "on") {
                        $input['same_as_company_fax_no'] = 1;
                    } else {
                        $input['same_as_company_fax_no'] = 0;
                    }
                }
                Contact::create($input);
            }


            //return redirect(route('admin.profile'));
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 2]);
        } catch (Exception $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 2]);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 2]);
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 2]);
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 2]);
        }

    }

    public function storeSecurity(Request $request)
    {
        try {


            if ($request->has('security_id')) {
                $id = $request->security_id;
                $input = $request->except(['_token', 'security_id']);
                Security::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                Security::create($input);
            }
            //return redirect(route('admin.profile'));
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 3]);
        } catch (Exception $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 3]);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 3]);
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 3]);
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 3]);
        }
    }

    public function storeFormation(Request $request)
    {
        try {
            $upload_certificate = '';
            $upload_article = '';
            if ($request->hasFile('certificate')) {
                $fileName = str_replace(' ', '_', $_FILES['certificate']['name']);
                $upload_certificate = date('mdYHis') . uniqid() . $fileName;
                $request->certificate->move('company_document/certificate', $upload_certificate);
            } else {
                $upload_certificate = $request->certificate;
            }
            if ($request->hasFile('articles')) {
                $fileName = str_replace(' ', '_', $_FILES['articles']['name']);
                $upload_article = date('mdYHis') . uniqid() . $fileName;
                $request->articles->move('company_document/article', $upload_article);
            } else {
                $upload_article = $request->articles;
            }

            if ($request->has('formation_id')) {
                $id = $request->formation_id;
                //$input=$request->all();
                $input = $request->except(['_token', 'formation_id']);
                $input['certificate'] = $upload_certificate;
                $input['articles'] = $upload_article;
                $input['date_of_incorporation'] = date('Y-m-d', strtotime($request->date_of_incorporation));
                $input['renewal_date'] = date('Y-m-d', strtotime($request->renewal_date));
                Formation::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                $input['certificate'] = $upload_certificate;
                $input['articles'] = $upload_article;
                $input['date_of_incorporation'] = date('Y-m-d', strtotime($request->date_of_incorporation));
                $input['renewal_date'] = date('Y-m-d', strtotime($request->renewal_date));
                Formation::create($input);
            }

        } catch (Exception $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
            dd($ex->getMessage());
        } catch (QueryException $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
        } catch (\ErrorException $ex) {
            unlink(public_path('company_document/certificate/' . $upload_certificate));
            unlink(public_path('company_document/article/' . $upload_article));
        }

        //return redirect(route('admin.profile'));
        return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 4]);
    }

    public function storeShareHolder(Request $request)
    {
        try {
            if ($request->shareholder_id > 0) {
                $id = $request->shareholder_id;
                $input = $request->except(['_token', 'shareholder_id']);
                $input['effective_date'] = date('Y-m-d', strtotime($request->effective_date));
                $isStore = Shareholder::where('id', $id)->update($input);
            } else {
                $input = $request->except(['_token']);
                $input['effective_date'] = date('Y-m-d', strtotime($request->effective_date));
                $isStore = Shareholder::create($input);
            }
//            if ($isStore) {
//                $shareholder = Shareholder::Where('formation_id', '1')->get();
//                echo json_encode($shareholder);
//            } else {
//                echo '2';
//            }
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 4]);
        } catch (QueryException $ex) {
            abort(405);
        } catch (Exception $ex) {
            abort(405);
        }
    }

    public function storeBusinessLicense(Request $request)
    {
        try {

            if ($request->has('businessLicense_id')) {
                $id = $request->businessLicense_id;
                $input = $request->except(['_token', 'businessLicense_id']);
                $input['expire_date'] = date('Y-m-d', strtotime($request->expire_date));
                BusinessLicense::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                $input['expire_date'] = date('Y-m-d', strtotime($request->expire_date));
                BusinessLicense::create($input);
            }
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 5]);
        } catch (QueryException $ex) {
            abort(405);
        } catch (Exception $ex) {
            abort(405);
        }
    }

    public function storeProfessionalLicense(Request $request)
    {
        try {
            $dateFormat = new DateUtility();
            $input = $request->all();
            Validator::make($input, [
                'admin_id' => ['required'],
                'profession' => ['required'],
                'professional_license_state' => ['required'],
                'professional_effective_date' => ['required'],
                'professional_license_no' => ['required'],
                'professional_expire_date' => ['required'],
                'professional_license_note' => ['required']
            ])->validate();

            if ($request->professional_id > 0) {
                $id = $request->professional_id;
                $input = $request->except(['_token', 'professional_id']);
                $input['professional_effective_date'] = $dateFormat->formats($request->professional_effective_date);
                $input['professional_expire_date'] = $dateFormat->formats($request->professional_expire_date);
                ProfessionalLicense::where('id', $id)->update($input);
            } else {
                $input = $request->except(['professional_id']);
                $input['professional_effective_date'] = $dateFormat->formats($request->professional_effective_date);
                $input['professional_expire_date'] = $dateFormat->formats($request->professional_expire_date);
                ProfessionalLicense::create($input);
            }
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 5]);

        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getProfessionalLicense(Request $request)
    {
        try {
            $id = $request->id;
            echo json_encode(ProfessionalLicense::Where('id', $id)->first());
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }

    }

    public function getSharedHolderData($id)
    {
        try {
            $shareholder = Shareholder::Where('id', $id)->first();
            echo json_encode($shareholder);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function deleteSharedHolderData($id)
    {
        try {
            Shareholder::where('id', $id)->delete();
            return redirect()->back()->with(['ProfileSuccess' => 'Record Deleted !', 'tab_no' => 4]);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function storeTaxation(Request $request)
    {
        try{

            return $request->all();

            $dateForamt = new DateUtility();
            if($request->taxation_id > 0){
                $id=$request->taxation_id;
                $input = $request->except(['_token','taxation_id']);
                $input['typeofcorp_effect']= $dateForamt->formats($request->has('typeofcorp_effect')==null?$request->typeofcorp_effect:"");
                $input['income_tax_federal_due_date']= $dateForamt->formats($request->has('income_tax_federal_due_date')==null?$request->income_tax_federal_due_date:Carbon::now());
                $input['income_tax_federal_extension_due_date']= $dateForamt->formats($request->has('income_tax_federal_extension_due_date')==null?$request->income_tax_federal_extension_due_date:Carbon::now());
                $input['income_tax_state_due_date']= $dateForamt->formats($request->has('income_tax_state_due_date')==null?$request->income_tax_state_due_date:Carbon::now());
                $input['income_tax_extension_state_due_date']= $dateForamt->formats($request->has('income_tax_extension_state_due_date')==null?$request->income_tax_extension_state_due_date:Carbon::now());
                $input['fica_quarter_period_date']= $dateForamt->formats($request->has('fica_quarter_period_date')==null?$request->fica_quarter_period_date:Carbon::now());
                $input['fica_due_date']= $dateForamt->formats($request->has('fica_due_date')==null?$request->fica_due_date:Carbon::now());
                $input['fica_payment_frequency_date']= $dateForamt->formats($request->has('fica_payment_frequency_date')==null?$request->fica_payment_frequency_date:Carbon::now());
                $input['futa_frequency_due_date']= $dateForamt->formats($request->has('futa_frequency_due_date')==null?$request->futa_frequency_due_date:Carbon::now());
                $input['futa_payment_frequency_date']= $dateForamt->formats($request->has('futa_payment_frequency_date')==null?$request->futa_payment_frequency_date:Carbon::now());
                AdminTaxation::where('id',$id)->update($input);
            }else{
                $input = $request->all();
                $input['typeofcorp_effect']= $dateForamt->formats($request->has('typeofcorp_effect')==null?$request->typeofcorp_effect:Carbon::now());
                $input['income_tax_federal_due_date']= $dateForamt->formats($request->has('income_tax_federal_due_date')==null?$request->income_tax_federal_due_date:Carbon::now());
                $input['income_tax_federal_extension_due_date']= $dateForamt->formats($request->has('income_tax_federal_extension_due_date')?$request->income_tax_federal_extension_due_date:Carbon::now());
                $input['income_tax_state_due_date']= $dateForamt->formats($request->has('income_tax_state_due_date')?$request->income_tax_state_due_date:Carbon::now());
                $input['income_tax_extension_state_due_date']= $dateForamt->formats($request->has('income_tax_extension_state_due_date')?$request->income_tax_extension_state_due_date:Carbon::now());
                $input['fica_due_date']= $dateForamt->formats($request->has('fica_due_date')?$request->fica_due_date:Carbon::now());
                $input['fica_quarter_period_date']= $dateForamt->formats($request->has('fica_quarter_period_date')?$request->fica_quarter_period_date:Carbon::now());
                $input['fica_payment_frequency_date']= $dateForamt->formats($request->has('fica_payment_frequency_date')?$request->fica_payment_frequency_date:Carbon::now());
                $input['futa_frequency_due_date']= $dateForamt->formats($request->has('futa_frequency_due_date')?$request->futa_frequency_due_date:Carbon::now());
                $input['futa_payment_frequency_date']= $dateForamt->formats($request->has('futa_payment_frequency_date')?$request->futa_payment_frequency_date:Carbon::now());
                AdminTaxation::create($input);
            }
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 6]);
        }catch(QueryException $ex) {
            //return redirect()->back()->with(['ProfileError' => 'Record not Updated', 'tab_no' => 6]);
            dd($ex->getMessage());
        }
    }

    public function storeBankAccount(Request $request)
    {
        try {
            $dateFormate = new DateUtility();
            $input = $request->all();
//            Validator::make($input, [
//                'bank_id' => ['required','max:255'],
//                'bank_name' => ['required','max:255'],
//                'nick_name' => ['required','max:255'],
//                'check_stubs' => ['required','max:255'],
//                'op_date' => ['required', 'max:255'],
//                'statement' => ['required','max:255'],
//                'ac_status' => ['required', 'max:255']
//            ])->validate();

            if ($request->has('bank_ac_id')) {
                $id = $request->bank_ac_id;
                $input = $request->except(['_token', 'bank_ac_id']);
                $input['op_date'] = $dateFormate->formats($request->op_date);
                AdminBank::where('id', $id)->update($input);
            } else {
                $input['op_date'] = $dateFormate->formats($request->op_date);
                AdminBank::create($input);

            }
            //return redirect(route('admin.profile'));
            return redirect()->back()->with(['ProfileSuccess' => 'Record Updated', 'tab_no' => 7]);
        } catch (Exception $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 7]);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 7]);
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 7]);
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
            return redirect()->back()->with(['ProfileError' => $ex->getMessage(), 'tab_no' => 7]);
        }

    }

    public function changePassword(Request $request)
    {
        try {
            $dateFormate = new DateUtility();

            $update = [
                'password' => Hash::make($request->password),
                'reset_days' => $request->reset_days,
                'reset_date' => $dateFormate->futureDate($request->reset_days)
            ];
            $current_password = $request->old_password;
            if (Hash::check($current_password, Auth::user()->password)) {
                Admin::where('id', Auth::user()->id)->update($update);
                return redirect()->back()->with('Success', 'Reset Your password!');
            } else {
                return redirect()->back()->with('Error', 'Old Password does not match');
            }
        } catch (Exception $ex) {
            dd($ex->getMessage());
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function storeProffesion(Request $request)
    {
        try {
            $input = $request->all();
            Validator::make($input, [
                'profession_name' => ['required']
            ])->validate();

            $isStore = Profession::create($input);
            if ($isStore) {
                echo json_encode(Profession::all());
            } else {
                echo 2;
            }
        } catch (Exception $ex) {
            dd($ex->getMessage());
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    public function deleteProfession(Request $request)
    {
        try {
            $input = $request->all();
            Validator::make($input, [
                'id' => ['required']
            ])->validate();
            $id = $request->id;
            $isStore = Profession::where('id', $id)->delete();
            if ($isStore) {
                echo json_encode(Profession::all());
            } else {
                echo 2;
            }
        } catch (Exception $ex) {
            dd($ex->getMessage());
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\HttpException $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }


}
