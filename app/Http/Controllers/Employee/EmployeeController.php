<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Admin\Employee\EmployeeProfile\EmergencyInfo;
use App\Models\Admin\Employee\EmployeeProfile\HiringInfo;
use App\Models\Admin\Employee\EmployeeProfile\Pay;
use App\Models\Admin\Employee\EmployeeProfile\PersonalInfo;
use App\Models\Admin\Employee\EmployeeProfile\Review;
use App\Models\Admin\Employee\EmployeeProfile\SecurityInfo;
use App\Models\Admin\Employee\UEmployee;
use App\Models\Admin\Master\Represent;
use App\Utility\DateUtility;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeList = UEmployee::where('status', 1)->get();
        $passData = [
            'employeeList'
        ];
        return view('Backend.Employee.Employee.employee', compact($passData));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $representList = Represent::all();
        return view('Backend.Employee.Employee.add-employee', compact([
            'representList'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            Validator::make($input, [
                'user_type' => ['required'],
                'user_code' => ['required'],
                'represent' => ['required'],
                'fname' => ['required'],
                'lname' => ['required'],
                'address_1' => ['required'],
                'city' => ['required'],
                'state' => ['required'],
                'zip' => ['required'],
                'contact_no_1' => ['required'],
                'email' => ['email:rfc,dns']
            ]);
            if ($request->employee_id != "") {
                $input['user_code'] = $request->employee_id;
            }
            if ($request->user_id != "") {
                $input['user_code'] = $request->user_id;
            }
            UEmployee::create($input);
            return redirect(route('employee.employees'));
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        } catch (\ErrorException $ex) {
            dd($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = $id;
        $edit_employee = UEmployee::with('Hiring')
            ->with('Emergency')
            ->with('Personal')
            ->with('Security')
            ->with('Pay')
            ->with('Review')
            ->where('id', $id)->first();

//        echo "<pre>";
//        print_r($edit_employee->toArray());
//        exit('000');

        return view('Backend.Employee.Employee.edit-employee', compact(['user_id', 'edit_employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function setRepresent(Request $request)
    {
        try {
            if ($request->has('rep_id')) {
                $id = $request->rep_id;
                $input = $request->except(['_token', 'rep_id']);
                Represent::where('id', $id)->update($input);
            } else {
                $input = $request->all();
                Represent::create($input);
            }
            $representList = Represent::all();
            echo json_encode($representList);
        } catch (QueryException $ex) {
            dd($ex->getMessage());
            abort(403);
        }
    }

    public function supervisorList()
    {
        return view('Backend.Employee.supervisor');
    }

    public function isHasData($id, $name)
    {
        $isData = $name::where('user_id', $id);
        if (count($isData) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
    Custom functions

    */

    public function createAndUpdate(Request $request)
    {
        try{
            $dateFormate = new DateUtility();
            $id = $request->emp_id;
            $emp_code = $request->user_code;
            $hiring = [
                'user_id' => $id,
                'hire_date' => $request->hire_date != "" ?  $dateFormate->formats($request->hire_date):"",
                'termination_date' => $request->termination_date != "" ?  $dateFormate->formats($request->termination_date):"",
                'rehire_date' => $request->rehire_date != "" ? $dateFormate->formats($request->rehire_date) : "",
                'retermination_date' => $request->retermination_date != "" ? $dateFormate->formats($request->retermination_date) : "",
                't_note2' => $request->t_note2,
                't_note' => $request->t_note,
                'branch_city' => $request->branch_city,
                'branch_name' => $request->branch_name,
                'hiring_position' => $request->hiring_position,
                'is_supervisor' => $request->is_supervisor,
                'branch_note' => $request->branch_note,
            ];
            if ($request->has('hiring_id')) {
                $hiring_id = $request->hiring_id;
                HiringInfo::where('id', $hiring_id)->update($hiring);
            } else {
                HiringInfo::create($hiring);
            }
            $employeeDocsPath = public_path("employee_document/".$emp_code);
            if(!is_dir($employeeDocsPath)){
                mkdir($employeeDocsPath);
            }
//            $upload_pf1_doc = '';
//            $upload_pf2_doc = '';
//            $upload_resume = '';
//            $upload_agreement = '';
            if ($request->hasFile('pf1_document')) {
                $fileName = str_replace(' ', '_', $_FILES['pf1_document']['name']);
                $upload_pf1_doc = date('mdYHis') . uniqid() . $fileName;
                $request->pf1_document->move($employeeDocsPath, $upload_pf1_doc);
            }else{
                $upload_pf1_doc = $request->pf1_document_2;
            }
            if ($request->hasFile('pf2_document')) {
                $fileName = str_replace(' ', '_', $_FILES['pf2_document']['name']);
                $upload_pf2_doc = date('mdYHis') . uniqid() . $fileName;
                $request->pf2_document->move($employeeDocsPath, $upload_pf2_doc);
            }else{
                $upload_pf2_doc=$request->pf2_document_2;
            }
            if ($request->hasFile('resume_document')) {
                $fileName = str_replace(' ', '_', $_FILES['resume_document']['name']);
                $upload_resume = date('mdYHis') . uniqid() . $fileName;
                $request->resume_document->move($employeeDocsPath, $upload_resume);
            }else{
                $upload_resume=$request->resume_document_2;
            }
            if ($request->hasFile('agreement_document')) {
                $fileName = str_replace(' ', '_', $_FILES['agreement_document']['name']);
                $upload_agreement = date('mdYHis') . uniqid() . $fileName;
                $request->agreement_document->move($employeeDocsPath, $upload_agreement);
            }else{
                $upload_agreement=$request->agreement_document_2;
            }


            $personal=[
                'user_id' => $id,
                'gender' => $request->gender,
                'marital_staus' => $request->marital_staus,
                'dob' => $request->dob != "" ?  $dateFormate->formats($request->dob):"",
                'langauge' => $request->has('langauge') ? count($request->langauge)>0 ? implode(",",$request->langauge) : "" : $request->langauge_hidden,
                'pf1' => $request->pf1,
                'pf1_document' => $upload_pf1_doc,
                'pf2' => $request->pf2,
                'pf2_document' => $upload_pf2_doc,
                'resume_document' => $upload_resume,
                'type_of_agreement' => $request->type_of_agreement,
                'agreement_document' => $upload_agreement,
            ];

            if ($request->has('personal_id')) {
                $personal_id = $request->personal_id;
                PersonalInfo::where('id', $personal_id)->update($personal);
            } else {
                PersonalInfo::create($personal);
            }

            $emergency=[
                'user_id' => $id,
                'emergency_fname'=> $request->emergency_fname,
                'emergency_mname'=> $request->emergency_mname,
                'emergency_lname'=> $request->emergency_lname,
                'emergency_address_1'=> $request->emergency_address_1,
                'emergency_address_2'=> $request->emergency_address_2,
                'emergency_city'=> $request->emergency_city,
                'emergency_state'=> $request->emergency_state,
                'emergency_zip'=> $request->emergency_zip,
                'emergency_contact_1'=> $request->emergency_contact_1,
                'emergency_type_1'=> $request->emergency_type_1,
                'emergency_ext_1'=> $request->emergency_ext_1,
                'emergency_contact_2'=> $request->emergency_contact_2,
                'emergency_type_2'=> $request->emergency_type_2,
                'emergency_ext_2'=> $request->emergency_ext_2,
                'emergency_email'=> $request->emergency_email,
                'emergency_relation'=> $request->emergency_relation,
                'emergency_note'=> $request->emergency_note,
            ];
            if ($request->has('emergency_id')) {
                $emergency_id = $request->emergency_id;
                EmergencyInfo::where('id', $emergency_id)->update($emergency);
            } else {
                EmergencyInfo::create($emergency);
            }

            $security=[
                'user_id'=>$id,
                'question1'=>$request->question1,
                'que_1'=>$request->que_1,
                'question2'=>$request->question2,
                'que_2'=>$request->que_2,
                'question3'=>$request->question3,
                'que_3'=>$request->que_3,
                'answer1'=>$request->answer1,
                'answer2'=>$request->answer2,
                'answer3'=>$request->answer3,
            ];
            if ($request->has('security_id')) {
                $security_id = $request->security_id;
                SecurityInfo::where('id', $security_id)->update($security);
            } else {
                SecurityInfo::create($security);
            }

            return redirect(route('employee.edit',$id));
        }catch(Exception $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }catch(\ErrorException $ex){
            dd($ex->getMessage());
        }

    }

    public function storePay(Request $request)
    {
        $dateFormat = new DateUtility();
        try{
            $id = $request->user_id;
            if($request->has('pay_id')){
                $pay_id = $request->pay_id;
                $input = $request->except(['pay_id','_token']);
                $input['effective_date'] = $dateFormat->formats($request->effective_date);
                Pay::Where('id',$pay_id)->update($input);
            }else{
                $input = $request->all();
                $input['effective_date'] = $dateFormat->formats($request->effective_date);
                Pay::create($input);
            }
            $payList = Pay::where('user_id',$id)->get();
            echo json_encode($payList);
        }catch(Exception $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }catch(\ErrorException $ex){
            dd($ex->getMessage());
        }
    }

    public function storeReview(Request $request)
    {
        $dateFormat = new DateUtility();
        try{
            $id = $request->user_id;
            if($request->has('review_id')){
                $pay_id = $request->review_id;
                $input = $request->except(['review_id','_token']);
                $input['review_date'] = $dateFormat->formats($request->review_date);
                $input['next_review_date'] = $dateFormat->formats($request->next_review_date);
                Review::Where('id',$pay_id)->update($input);
            }else{
                $input = $request->all();
                $input['review_date'] = $dateFormat->formats($request->review_date);
                $input['next_review_date'] = $dateFormat->formats($request->next_review_date);
                Review::create($input);
            }
            $payList = Pay::where('user_id',$id)->get();
            echo json_encode($payList);
        }catch(Exception $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }catch(\ErrorException $ex){
            dd($ex->getMessage());
        }
    }
}
