<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\HiringRequest;
use App\Models\Admin\Employee\Hiring;
use App\Utility\DateUtility;
use Illuminate\Database\QueryException;

class HiringController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $hiringList = Hiring::where('status', 0)->get();
        $passData = [
            'hiringList'
        ];
        return view('Backend.Employee.hiring', compact($passData));
    }

    public function storeHiring(HiringRequest $request)
    {
        try {

            $dateFormate = new DateUtility();
            if($request->has('hiring_id')){
                $id=$request->hiring_id;
                $input = $request->except(['_token','hiring_id']);
                $input['post_date']= $dateFormate->formats($request->post_date);
                Hiring::where('id',$id)->update($input);
            }else{
                $input=$request->all();
                $input['post_date']= $dateFormate->formats($request->post_date);
                Hiring::create($input);
            }
            return redirect(route('employee.hiring'));

        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }catch(\ErrorException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }
    }
}
