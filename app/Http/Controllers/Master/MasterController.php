<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\TypeOfEntity;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class MasterController extends Controller
{
   public function storeTypeOfEntity(Request $request)
   {
       try{
           $input = $request->all();
           $isStore = TypeOfEntity::create($input);
           if($isStore){
               echo json_encode(TypeOfEntity::all());
           }else{
               echo 2;
           }

       }catch(Exception $ex){
           abort(403);
       }catch(QueryException $ex){
           abort(403);
       }catch(\ErrorException $ex){
           abort(403);
       }catch(\HttpException $ex){
           abort(403);
       }
   }

   public function deleteTypeOfEntity(Request $request)
   {
       try{
           $id = $request->id;
           $isStore = TypeOfEntity::where('id',$id)->delete();
           if($isStore){
               echo json_encode(TypeOfEntity::all());
           }else{
               echo 2;
           }

       }catch(Exception $ex){
           abort(403);
       }catch(QueryException $ex){
           abort(403);
       }catch(\ErrorException $ex){
           abort(403);
       }catch(\HttpException $ex){
           abort(403);
       }
   }
}
