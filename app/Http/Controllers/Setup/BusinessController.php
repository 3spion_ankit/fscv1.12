<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function index(){

    }

    public function business(){
        return view('Backend.Setup.Business.business');
    }

    public function business_brand(){
        return view('Backend.Setup.Business.business-brand');
    }

    public function storeBusiness(Request $request)
    {
        try{
            if($request->has('business_id')){

            }else{
                $input=$request->all();
                if ($request->hasFile('business_image')) {
                    $fileName = str_replace(' ', '_', $_FILES['business_image']['name']);
                    $upload_business_image = date('mdYHis') . uniqid() . $fileName;
                    $request->certificate->move('business', $upload_business_image);
                }else{
                    $upload_business_image="";
                }
            }
        }catch(Exception $ex){
            unlink(public_path('business/'.$upload_business_image));
            dd($ex->getMessage());
        }catch(ErrorException $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }
    }
}
