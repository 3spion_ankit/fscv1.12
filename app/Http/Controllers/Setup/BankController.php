<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Admin\Bank;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Jetstream\Jetstream;

class BankController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bankList = Bank::where('status',0)->get();
        $passData = [
          'bankList'
        ];
        return view('Backend.Setup.Bank.bank-list',compact($passData));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.Setup.Bank.add-bank');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $input = $request->except(['bank_id']);
            Validator::make($input, [
                'bank_name' => ['required','max:255'],
                'bank_address' => ['required','max:255'],
                'routing_number' => ['required','max:255'],
                'bank_contact_no' => ['required','max:255'],
                'bank_contact_type' => ['required', 'max:255'],
                'branch_name' => ['required','max:255'],
                'branch_address' => ['required', 'max:255'],
                'city' => ['required', 'max:255'],
                'state' => ['required', 'max:255'],
                'zip' => ['required', 'max:255'],
                'contact_person' => ['required', 'max:255'],
                'contact_no' => ['required', 'max:255'],
                'contact_type' => ['required',  'max:255'],
            ])->validate();

            Bank::create($request->all());
            return redirect(route('setup.bank-list'));
        }catch(Exception $ex){
            abort(403);
        }catch(QueryException $ex){
            abort(403);
        }catch(\ErrorException $ex){
            abort(403);
        }catch(\HttpException $ex){
            abort(403);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
