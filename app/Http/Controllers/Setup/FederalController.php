<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Admin\FederalAuthorities;
use App\Models\Admin\Master\TypeOfEntity;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class FederalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $federalTaxAuthorityList = FederalAuthorities::with('typeOfEntity')->get();
        $passData = [
            'federalTaxAuthorityList'
        ];
        return view('Backend.Setup.TypeOfAuthority.Federal.tax-form-list',compact($passData));
    }
    public function create()
    {
        $typeOfEntityList = TypeOfEntity::all();
        $passData=[
          'typeOfEntityList'
        ];

        return view('Backend.Setup.TypeOfAuthority.Federal.tax-form-create',compact($passData));
    }

    public function edit($id)
    {
        $federalTaxAuthority = FederalAuthorities::where('id',$id)->with('typeOfEntity')->first();
        $typeOfEntityList = TypeOfEntity::all();
        $passData = [
            'federalTaxAuthority',
            'typeOfEntityList'
        ];
        return view('Backend.Setup.TypeOfAuthority.Federal.edit-tax-form',compact($passData));
    }

    public function storeFederalTaxAuthority(Request $request)
    {
        try{
            if($request->federal_authority_id > 0){
                $id = $request->federal_authority_id;
                $input = $request->except(['_token','federal_authority_id']);
                FederalAuthorities::where('id',$id)->update($input);
            }else{
                FederalAuthorities::create($request->all());
            }
            return redirect(route('setup.federal'))->with('FederalSuccess','Record Inserted');
        }catch(Exception $ex){
            return redirect(route('setup.federal'))->with('FederalErrors',$ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
            //return redirect(route('setup.federal'))->with('FederalErrors','Query Error');
        }catch(\ErrorException $ex){
            return redirect(route('setup.federal'))->with('FederalErrors',$ex->getMessage());
        }catch(\HttpException $ex){
            return redirect(route('setup.federal'))->with('FederalErrors',$ex->getMessage());
        }
    }


}
