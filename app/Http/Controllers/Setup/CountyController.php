<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Admin\State;
use App\Models\Admin\TaxCounty;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CountyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $taxCountyList= TaxCounty::all();
        $passData=[
            'taxCountyList'
        ];
        return view('Backend.Setup.TypeOfAuthority.County.county-list',compact($passData));
    }

    public function create()
    {
        //$stateList = State::All();
        return view('Backend.Setup.TypeOfAuthority.County.create-county');
    }

    public function edit($id){
        $taxCountyList= TaxCounty::Where('id',$id)->first();
        $passData=[
            'taxCountyList'
        ];
        return view('Backend.Setup.TypeOfAuthority.County.county-edit',compact($passData));
    }

    public function storeCounty(Request $request)
    {
        try{
            $input= $request->all();
            if($request->has('county_id')){
                $id = $request->county_id;
                $input=$request->except(['county_id','_token']);
                TaxCounty::Where('id',$id)->update($input);
            }else{
                TaxCounty::create($input);
            }
            return redirect(route('setup.county-list'));

        }catch(Exception $ex){
            dd($ex->getMessage());
        }catch(ErrorException $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }catch(\HttpException $ex){
            dd($ex->getMessage());
        }
    }
}
