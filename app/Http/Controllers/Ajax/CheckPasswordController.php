<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CheckPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function checkPassword(Request $req)
    {
        try{
            if(Hash::check($req->input_password, Auth::user()->password))
            {
               echo 1;
            }
        }catch(Exception $ex){
            dd($ex->getMessage());
        }catch(QueryException $ex){
            dd($ex->getMessage());
        }

    }
}
