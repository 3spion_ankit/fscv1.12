<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests\CountyStateRequest;
use App\Models\Admin\County;
use Illuminate\Http\Request;

class StateCountyController extends Controller
{
    public function setCountyByState(CountyStateRequest $request)
    {
        echo json_encode(County::where('state_code',$request->state_code)->get());
    }

    public function getCountyCode(Request $request)
    {
        $code = County::Where('county_name',$request->county_name)->first();
        echo $code->county_code;
    }
}
