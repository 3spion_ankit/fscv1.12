<?php

namespace App\Http\Controllers\Ajax;

use App\Actions\Fortify\ResetUserPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\CountryStateRequest;
use App\Models\Admin\State;
use Illuminate\Http\Request;

class CountryStateController extends Controller
{

    public function setStateByCountry(CountryStateRequest $request)
    {
        echo json_encode(State::where('countrycode',$request->contry_code)->get());
    }
}
