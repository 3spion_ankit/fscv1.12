<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Actionlink extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $route,$class,$id;
    public $buttons; // Here define because how many no of button user wants.
    public function __construct($route,$id,$class,$buttons)
    {
        $this->route = $route;
        $this->class = $class;
        $this->buttons = $buttons;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.actionlink');
    }
}
