<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Email extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $email_name;
    public function __construct($name,$val)
    {
        $this->email_name=$name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.email');
    }
}
