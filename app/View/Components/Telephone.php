<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Telephone extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $label_name,
        $type_name,
        $contact_name,
    $extnsion_name;

    public $type_val,$contact_val,$extension_val;

    public function __construct($name,$val)
    {
        $this->label_name = $name["label_name"];
        $this->type_name = $name["type_name"];
        $this->contact_name = $name["contact_name"];
        $this->extnsion_name = $name["extnsion_name"];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.telephone');
    }
}
