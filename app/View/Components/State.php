<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Admin\State as states;

class State extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $getUSAStates,$getAllStates,$ui_id,$selectionData;

    public function __construct($data,$message)
    {
        $this->getAllStates = states::all();
        $this->getUSAStates = states::where('countrycode','USA')->get();
        $this->ui_id = $data;
        $this->selectionData = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $getUSAStates = $this->getUSAStates;
        return view('components.state',compact(['getUSAStates']));
    }
}
