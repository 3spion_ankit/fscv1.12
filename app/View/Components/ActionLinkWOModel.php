<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ActionLinkWOModel extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $route,$buttons;
    public function __construct($route,$buttons)
    {
        $this->route = $route;
        $this->buttons = $buttons;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.action-link-w-o-model');
    }
}
