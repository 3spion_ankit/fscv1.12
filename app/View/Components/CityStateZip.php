<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Admin\State;

class CityStateZip extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $getUSAStates,
        $getAllStates,
        $ui_id,
        $city_name,
        $state_name,
        $zip_name,
        $selectionData;
    public function __construct($data,$message,$name)
    {
        $this->getAllStates = State::all();
        $this->getUSAStates = State::where('countrycode','USA')->get();
        $this->ui_id = $data;
        $this->selectionData = $message;
        $this->city_name = $name['city_name'];
        $this->state_name = $name['state_name'];
        $this->zip_name = $name['zip_name'];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $getUSAStates = $this->getUSAStates;
        return view('components.city-state-zip',compact(['getUSAStates']));
    }
}
