<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('position_masters')->delete();
        $postions = array(
            array('position_name' => 'CEO / President'),
            array('position_name' => 'CFO / Vise President'),
            array('position_name' => 'Secretary'),
            array('position_name' => 'Agent'),
            array('position_name' => 'CEO / CFO / Sec.')
        );
        DB::table('position_masters')->insert($postions);
    }
}
