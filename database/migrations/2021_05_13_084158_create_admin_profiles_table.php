<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('company_name');
            $table->string('business_name');
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->string('city');
            $table->string('comp_state');
            $table->string('state_code');
            $table->integer('zip');
            $table->string('contact_no');
            $table->enum('contact_no_type',['Mobile','Office']);
            $table->integer('extension');
            $table->string('fax_no')->nullable();
            $table->text('website')->nullable();
            $table->string('email')->unique()->nullable();
            $table->tinyInteger('same_as_company_info');
            $table->text('pa_address_1')->comment('PA for Physical Address');
            $table->text('pa_address_2')->comment('PA for Physical Address');
            $table->string('pa_city')->comment('PA for Physical Address');
            $table->string('pa_state')->comment('PA for Physical Address');
            $table->string('pa_state_code')->comment('PA for Physical Address');
            $table->integer('pa_zip')->comment('PA for Physical Address');
            $table->string('county');
            $table->string('county_code');
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
