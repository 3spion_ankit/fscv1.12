<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHiringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hirings', function (Blueprint $table) {
            $table->id();
            $table->date('post_date');
            $table->string('job_type');
            $table->string('position');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('name');
            $table->string('contact_no');
            $table->text('description')->nullable();
            $table->tinyInteger('staus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hirings');
    }
}
