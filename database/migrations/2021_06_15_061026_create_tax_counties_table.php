<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_counties', function (Blueprint $table) {
            $table->id();
            $table->string('county_name')->nullable();
            $table->string('county_code')->nullable();
            $table->string('county_authority_name')->nullable();
            $table->string('county_website')->nullable();
            $table->string('county_telephone')->nullable();
            $table->text('county_address')->nullable();
            $table->string('county_city')->nullable();
            $table->string('county_state')->nullable();
            $table->integer('county_zip')->nullable();
            $table->string('county_fax')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_counties');
    }
}
