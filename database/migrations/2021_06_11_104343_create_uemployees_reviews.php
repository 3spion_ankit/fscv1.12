<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_reviews', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->date('review_date');
            $table->integer('next_review');
            $table->date('next_review_date');
            $table->double('next_pay_rate');
            $table->text('comments');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_reviews');
    }
}
