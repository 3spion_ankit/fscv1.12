<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFederalAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('federal_authorities', function (Blueprint $table) {
            $table->id();
            $table->string('type_of_form');
            $table->bigInteger('type_of_entity_id')->unsigned();
            $table->string('form_name');
            $table->string('authority_state')->nullable();
            $table->string('state_form_name')->nullable();
            $table->string('filling_frequency');
            $table->string('due_date');
            $table->string('extension_due_date');
            $table->string('authority_short_name')->nullable();
            $table->string('authority_full_name')->nullable();
            $table->string('website');
            $table->string('telephone');
            $table->timestamps();
            $table->foreign('type_of_entity_id')->references('id')->on('type_of_entities')->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('federal_authorities');
    }
}
