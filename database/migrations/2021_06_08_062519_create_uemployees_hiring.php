<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesHiring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_hiring', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->date('hire_date')->nullable();
            $table->date('termination_date')->nullable();
            $table->text('t_note')->nullable();
            $table->date('rehire_date')->nullable();
            $table->date('retermination_date')->nullable();
            $table->text('t_note2')->nullable();
            $table->string('branch_city')->nullable();
            $table->string('branch_name')->nullable();
            $table->string('hiring_position')->nullable();
            $table->tinyInteger('is_supervisor')->nullable();
            $table->text('branch_note')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_hiring');
    }
}
