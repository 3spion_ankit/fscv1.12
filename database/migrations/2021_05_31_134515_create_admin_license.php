<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_license', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->integer('year');
            $table->integer('gross_revenue');
            $table->double('license_fee');
            $table->string('license_status');
            $table->string('license_copy')->nullable();
            $table->date('license_issue_date');
            $table->string('certificate_no');
            $table->string('license_website')->nullable();
            $table->text('license_note')->nullable();
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_license');
    }
}
