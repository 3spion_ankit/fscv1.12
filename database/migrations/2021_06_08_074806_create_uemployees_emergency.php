<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesEmergency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_emergency', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('emergency_fname')->nullable();
            $table->string('emergency_mname')->nullable();
            $table->string('emergency_lname')->nullable();
            $table->text('emergency_address_1')->nullable();
            $table->text('emergency_address_2')->nullable();
            $table->string('emergency_country')->default("USA");
            $table->string('emergency_city')->nullable();
            $table->string('emergency_state')->nullable();
            $table->string('emergency_zip')->nullable();
            $table->string('emergency_contact_1')->nullable();
            $table->string('emergency_type_1')->nullable();
            $table->string('emergency_ext_1')->nullable();
            $table->string('emergency_contact_2')->nullable();
            $table->string('emergency_type_2')->nullable();
            $table->string('emergency_ext_2')->nullable();
            $table->string('emergency_email')->nullable();
            $table->string('emergency_relation')->nullable();
            $table->string('emergency_note')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_emergency');
    }
}
