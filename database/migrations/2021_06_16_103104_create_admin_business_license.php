<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminBusinessLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_business_license', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('jurisdiction');
            $table->string('license_city')->nullable();
            $table->string('license_county')->nullable();
            $table->string('license_county_code')->nullable();
            $table->string('license_no');
            $table->date('expire_date');
            $table->text('license_note')->nullable();
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_business_license');
    }
}
