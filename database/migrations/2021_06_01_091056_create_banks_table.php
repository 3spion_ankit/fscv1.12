<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id();
            $table->string('bank_name');
            $table->string('bank_address');
            $table->string('routing_number');
            $table->string('bank_contact_type');
            $table->string('bank_contact_no');
            $table->string('bank_contact_no_extension')->nullable();
            $table->string('website')->nullable();
            /*
             * Branch Datatables
             */
            $table->string('branch_name');
            $table->string('branch_address');
            $table->string('contact_person');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('position');
            $table->string('contact_type');
            $table->string('contact_type_2')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('contact_no_2')->nullable();
            $table->string('contact_no_extension')->nullable();
            $table->string('contact_no_extension_2')->nullable();
            $table->string('notes')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
