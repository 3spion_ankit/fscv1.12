<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTaxationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_taxations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->integer('type_of_entity');
            $table->string('typeOfForm');
            $table->date("typeofcorp_effect")->nullable();
            $table->string("income_tax_federal_id")->nullable();
            $table->string("income_tax_federal_file")->nullable();
            $table->date("income_tax_federal_due_date")->nullable();
            $table->date("income_tax_federal_extension_due_date")->nullable();
            $table->string("income_tax_state_id")->nullable();
            $table->string("income_tax_state_file")->nullable();
            $table->date("income_tax_state_due_date")->nullable();
            $table->date("income_tax_extension_state_due_date")->nullable();

            $table->string("fica_name")->nullable();
            $table->string("fica_dept_authority")->nullable();
            $table->string("fica_form_number")->nullable();
            $table->date("fica_quarter_period_date")->nullable();
            $table->string("fica_quarter_period")->nullable();
            $table->date("fica_due_date")->nullable();
            $table->string("fica_eptpspin")->nullable();
            $table->string("fica_pay_pw")->nullable();
            $table->string("fica_federal_payment_frequency")->nullable();
            $table->date("fica_payment_frequency_date")->nullable();
            $table->text("fica_note")->nullable();

            $table->string("futa_name")->nullable();
            $table->string("futa_dept_authority")->nullable();
            $table->string("futa_frequency")->nullable();
            $table->string("futa_form_no")->nullable();
            $table->date("futa_frequency_due_date")->nullable();
            $table->string("futa_payment_frequency")->nullable();
            $table->date("futa_payment_frequency_date")->nullable();
            $table->text("futa_note")->nullable();

            $table->string("state_holding_quarter_type")->nullable();
            $table->date("state_holding_quarter_date")->nullable();
            $table->date("state_holding_due_date")->nullable();
            $table->string("state_holding_no")->nullable();
            $table->date("state_holding_eptpspin")->nullable();
            $table->string("state_holding_Username")->nullable();
            $table->string("state_holding_pay_pw")->nullable();
            $table->string("state_holding_payment_frequency")->nullable();
            $table->date("state_holding_payment_due_date")->nullable();

            $table->text("suta_quarter_period")->nullable();
            $table->date("suta_peroid_end_date")->nullable();
            $table->date("suta_peroid_due_date")->nullable();
            $table->string("suta_unemployment_no")->nullable();
            $table->string("suta_username")->nullable();
            $table->string("suta_payment_frequency")->nullable();
            $table->date("suta_payment_frequency_date")->nullable();

            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_taxations');
    }
}
