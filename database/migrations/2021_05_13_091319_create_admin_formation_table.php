<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminFormationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_formation', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('state_of_formation');
            $table->integer('certificate_type');
            $table->date('date_of_incorporation');
            $table->string('legal_name');
            $table->string('control_number');
            $table->text('certificate');
            $table->text('articles');
            $table->string('agent_fname');
            $table->string('agent_mname')->nullable();
            $table->string('agent_lname');
            $table->date('renewal_date');
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_formation');
    }
}
