<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminContactInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_contact_info', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('prefix_name');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('state_code')->default('0');
            $table->string('zip');
            $table->string('contact_no_1');
            $table->string('contact_no_type_1');
            $table->string('contact_no_extension_1')->nullable();
            $table->string('contact_no_2')->nullable();
            $table->string('contact_no_type_2')->nullable();
            $table->string('contact_no_extension_2')->nullable();
            $table->string('same_as_company_fax_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_contact_info');
    }
}
