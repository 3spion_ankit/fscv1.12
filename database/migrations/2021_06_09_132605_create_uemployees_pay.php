<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_pay', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('pay_method');
            $table->string('pay_frequency');
            $table->string('pay_rate');
            $table->date('effective_date');
            $table->string('notes');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_pay');
    }
}
