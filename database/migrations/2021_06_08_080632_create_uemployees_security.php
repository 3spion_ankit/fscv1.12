<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesSecurity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_security', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('question1')->nullable();
            $table->integer('que_1')->nullable();
            $table->string('question2')->nullable();
            $table->integer('que_2')->nullable();
            $table->string('question3')->nullable();
            $table->integer('que_3')->nullable();
            $table->string('answer1')->nullable();
            $table->string('answer2')->nullable();
            $table->string('answer3')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_security');
    }
}
