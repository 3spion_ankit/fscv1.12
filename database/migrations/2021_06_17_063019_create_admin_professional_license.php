<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminProfessionalLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_professional_license', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('profession');
            $table->string('professional_license_state');
            $table->string('type_of_license');
            $table->integer('professional_license_period')->default(1);
            $table->date('professional_effective_date');
            $table->string('professional_license_no');
            $table->date('professional_expire_date');
            $table->string('professional_license_note');
            $table->integer('is_ce_status')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_professional_license');
    }
}
