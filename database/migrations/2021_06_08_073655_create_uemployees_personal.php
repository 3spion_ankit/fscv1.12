<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesPersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees_personal', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('gender')->nullable();
            $table->string('marital_staus')->nullable();
            $table->date('dob')->nullable();
            $table->text('langauge')->nullable();
            $table->string('pf1')->nullable();
            $table->string('pf1_document')->nullable();
            $table->string('pf2')->nullable();
            $table->string('pf2_document')->nullable();
            $table->string('resume_document')->nullable();
            $table->string('type_of_agreement')->nullable();
            $table->string('agreement_document')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('uemployees')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees_personal');
    }
}
