<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminCorporationLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_corporation_license', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('admin_id')->unsigned();
            $table->integer('renewal_for');
            $table->string('renewal_year');
            $table->integer('renewal_amount');
            $table->string('paid_amount');
            $table->string('method_of_payment');
            $table->string('corporation_status');
            $table->string('annually_receipt');
            $table->string('officer');
            $table->string('website')->nullable();
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_corporation_license');
    }
}
