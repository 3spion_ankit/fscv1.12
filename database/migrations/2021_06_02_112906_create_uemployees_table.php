<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUemployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uemployees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_type');
            $table->string('user_code');
            $table->integer('represent')->comment("it is represent as which team member");
            $table->string('prefix_name');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->string('country')->default("USA");
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('contact_no_1');
            $table->string('contact_no_type_1');
            $table->string('contact_no_extension_1');
            $table->string('contact_no_2')->nullable();
            $table->string('contact_no_type_2')->nullable();
            $table->string('contact_no_extension_2')->nullable();
            $table->string('email')->nullable();
            $table->text('profile_pic')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uemployees');
    }
}
